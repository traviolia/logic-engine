package com.gitlab.traviolia.semantics.rules.fluents;

import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.Init;
import com.gitlab.traviolia.semantics.rules.Version;
import com.gitlab.traviolia.semantics.rules.io.IO;
import com.gitlab.traviolia.semantics.rules.io.Parser;
import com.gitlab.traviolia.semantics.rules.runtime.ClauseCompiler;
import com.gitlab.traviolia.semantics.rules.runtime.ClauseRuntime;
import com.gitlab.traviolia.semantics.rules.runtime.LogicEngineImpl;
import com.gitlab.traviolia.semantics.rules.runtime.RuleEngine;
import com.gitlab.traviolia.semantics.rules.runtime.Structure;
import com.gitlab.traviolia.semantics.rules.runtime.Term;
import com.gitlab.traviolia.semantics.rules.terms.AtomAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ClauseAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ConsAstNode;
import com.gitlab.traviolia.semantics.rules.terms.StructureAstNode;
import com.gitlab.traviolia.semantics.rules.terms.TermAstNode;


/**
  Implements a Term and Clause  objects based blackboard (database).
*/
public class DataBase extends HashMap<String,LinkedList<Term>> {
  
	private static final long serialVersionUID = 1L;

	private static class DataBaseHolder {
        private static final DataBase INSTANCE= new DataBase();
	}
	
	public static DataBase getInstance() {
        return DataBaseHolder.INSTANCE;
	}
	
	Logger logger = LogManager.getLogger();

  public DataBase(){
    super();
  }
  
  private static AtomAstNode yes=AtomAstNode.aYes;
  
  private static AtomAstNode no=AtomAstNode.aNo;
  
//  /**
//    Removes a matching Term from the blackboards and
//    signals failure if no such term is found.
//  */
//  public Term cin(String k,Term pattern) {
//    Term found=take(k,pattern);
//    // if(found!=null) {
//    // found=found.matching_copy(pattern);
//    // }
//    if(found==null)
//      found=(Version.getInstance().getNumber() == 1) ? no : null;
//    else
//      found=new StructureAstNode("the",found.copy());
//    return found;
//  }
  
  /**
  Adds a Term to the blackboard
*/
//public TermAstNode out(String k,TermAstNode pattern,boolean copying) {
//  add(k,copying?pattern.copy():pattern);
//  return (Version.getInstance().getNumber() == 1) ? yes : null;
//}

/**
  Adds a copy of a Term to the blackboard
*/

// synchronized
public TermAstNode out(String key,Term pattern) {
  return out(key,pattern,true); // copies pattern
}

/**
Adds a Term to the blackboard
*/
public TermAstNode out(String k,Term pattern,boolean copying) {
add(k,copying?pattern.copy():pattern);
return (Version.getInstance().getNumber() == 1) ? yes : null;
}

/**
Adds a copy of a Term to the blackboard
*/

// synchronized
//public TermAstNode out(String key,TermAstNode pattern) {
//return out(key,pattern,true); // copies pattern
//}

  private void all0(int max,ArrayList<Term> To,String k,Term FXs) {
    if(0==max)
      max=-1;
    LinkedList<Term> Q = get(k);
    if(Q==null)
      return;
    // todo: use always the same "server's" trail
    for(Iterator<Term> e=Q.iterator();e.hasNext();) {
      Term t= e.next();
      if(null==t)
        break;
      t=t.matching_copy(FXs);
      if(t!=null&&0!=max--)
        To.add(t);
    }
  }
  
//  private TermAstNode all1(int max,Term FXs) {
//    ArrayList<Term> To=new ArrayList<>();
//    for(Iterator<String> e=keySet().iterator();e.hasNext();) {
//      all0(max,To,e.next(),FXs);
//    }
//    Structure R=new StructureAstNode("$",(Object[])To.toArray());
//    // IO.mes("RR"+R);
//    // To.copyInto(R.args);
//    return (TermAstNode) ((ConsAstNode)R.listify()).args[1];
//  }
//  
//  private TermAstNode all2(int max,String k,Term FXs) {
//    if(k==null) {
//      // IO.mes("expensive operation: all/2 with unknown key");
//      return all1(max,FXs);
//    }
//    ArrayList<Term> To=new ArrayList<>();
//    all0(max,To,k,FXs);
//    if(To.size()==0)
//      return AtomAstNode.aNil;
//    Structure R=new StructureAstNode("$",(Object[])To.toArray());
//    // To.copyInto(R.args);
//    TermAstNode T= (TermAstNode) ((ConsAstNode)R.listify()).args[1];
//    return T;
//  }
//  
//  /**
//     Returns a (possibly empty) list of matching Term objects
//  */
//  public Term all(String k,Term FX) {
//    FX=all2(0,k,FX);
//    return FX;
//  }
  
  /**
     Gives an Iterator view to the LinkedList
     of Term or Clause objects stored at key k
     @see LinkedList
     @see TermAstNode
     @see ClauseAstNode
  */
//  public Iterator toEnumerationFor(String k) {
//    Iterator E=super.toEnumerationFor(k);
//    return E;
//  }
  
  /**
     Returns a formatted String representation of this
     PrologBlackboard object
  */
  public String pprint() {
    StringBuffer s=new StringBuffer(name());
    Iterator<String> e=keySet().iterator();
    while(e.hasNext()) {
      s.append(pred_to_string((String)e.next()));
      s.append("\n");
    }
    return s.toString();
  }
  
  public String pred_to_string(String key) {
    LinkedList<Term> Q=get(key);
    if(null==Q)
      return null;
    Iterator<Term> e=Q.iterator();
    StringBuffer s=new StringBuffer("% "+key+"\n\n");
    while(e.hasNext()) {
      s.append(((TermAstNode)e.next()).pprint());
      s.append(".\n");
    }
    s.append("\n");
    return s.toString();
  }
  
  /**
    consults or reconsults a Prolog file by adding or
    overriding existing predicates
    to be extended to load from URLs transparently
  */
public boolean fromFile(String f, RuleEngine engine, boolean overwrite) {
    IO.trace("last consulted file was: "+lastFile);
    boolean ok=fileToProg(f, engine, overwrite);
    if(ok) {
      IO.trace("last consulted file set to: "+f);
      lastFile=f;
    } else
      IO.errmes("error in consulting file: "+f);
    return ok;
  }
  
  /**
    reconsults a file by overwritting similar predicates in memory
  */
public boolean fromFile(String f, RuleEngine engine) {
    return fromFile(f,engine,true);
  }
  
  private static String lastFile=Init.default_lib;
  
  /**
    reconsults the last reconsulted file
  */
public boolean fromFile(RuleEngine engine) {
    IO.println("begin('"+lastFile+"')");
    boolean ok=fromFile(lastFile, engine);
    if(ok)
      IO.println("end('"+lastFile+"')");
    return ok;
  }
  
private boolean fileToProg(String fname, RuleEngine engine, boolean overwrite) {
    Reader sname=IO.toFileReader(fname);
    if(null==sname)
      return false;
    return streamToProg(fname,sname,engine,overwrite);
  }
  
  /**
    Reads a set of clauses from a stream and adds them to the
    blackboard. Overwrites old predicates if asked to.
    Returns true if all went well.
  */
public boolean streamToProg(Reader sname,RuleEngine engine,boolean overwrite) {
    return streamToProg(sname.toString(),sname,engine, overwrite);
  }
  
private boolean streamToProg(String fname,Reader sname, RuleEngine engine, boolean overwrite) {
    @SuppressWarnings("unchecked")
	HashMap<String,LinkedList<Term>> ktable = overwrite ? (HashMap<String,LinkedList<Term>>) DataBase.getInstance().clone() : null;
    // Clause Err=new Clause(new Const("error"),new Var());
    try {
      Parser p=new Parser(sname, engine);
      apply_parser(p,fname,ktable);
    } catch(Exception e) { // already catched by readClause
      IO.errmes("unexpected error in streamToProg",e);
      return false;
    }
    return true;
  }
  
private void apply_parser(Parser p,String fname, HashMap<String,LinkedList<Term>> ktable) {
	  switch (Version.getInstance().getNumber()) {
//	  case 1: {
//			for(;;) {
//			    if(p.atEOF())
//			      return;
//			    int begins_at=p.lineno();
//			    ClauseAstNode C=p.readClause();
//			    if(null==C)
//			      return;
//			    if(Parser.isError(C))
//			      Parser.showError(C);
//			    else {
//			      // IO.mes("ADDING= "+C.pprint());
//			      processClause(C,ktable);
//			      C.setFile(fname,begins_at,p.lineno());
//			    }
//			  }
//	  }
	  case 2: {
			for(;;) {
			    if(p.atEOF())
			      return;
			    int begins_at=p.lineno();
			    ClauseAstNode C=p.readClause();
			    if(null==C)
			      return;
			    if(Parser.isError(C))
			      Parser.showError(C);
			    else {
			      // IO.mes("ADDING= "+C.pprint());
			    	ClauseCompiler compiler = new ClauseCompiler();
			    	C.inspect(compiler);
			      processClause((ClauseRuntime) compiler.getReturnValue(),ktable);
			      C.setFile(fname,begins_at,p.lineno());
			    }
			  }
	  }
	  default:
		  throw new RuntimeException();
	  }
  }
  
  /**
    adds a Clause to the joint Linda and Predicate table
  */
//  static public void addClause(ClauseAstNode C,HashMap<String,LinkedList<Term>> ktable) {
//	    String k=C.getKey();
//	    // overwrites previous definitions
//	    if(null!=ktable&&null!=ktable.get(k)) {
//	      ktable.remove(k);
//	      DataBase.getInstance().remove(k);
//	    }
//	    DataBase.getInstance().out(k,C,false);
//	  }
	  
  static public void addClause(ClauseRuntime C,HashMap<String,LinkedList<Term>> ktable) {
	    String k=C.getKey();
	    // overwrites previous definitions
	    if(null!=ktable&&null!=ktable.get(k)) {
	      ktable.remove(k);
	      DataBase.getInstance().remove(k);
	    }
	    DataBase.getInstance().out(k,C,false);
	  }
	  
  /**
    adds a Clause to the joint Linda and Predicate table
    @see ClauseAstNode
  */
//  static public void processClause(ClauseAstNode C, HashMap<String,LinkedList<Term>> ktable) {
//	    if(C.getHead().matches(new AtomAstNode("init"))) {
//	      IO.mes("init: "+C.getBody());
//	      LogicEngineImpl.firstSolution(C.getHead(),C.getBody());
//	    } else {
//	      IO.mes("ADDING= "+C.pprint());
//	      addClause(C,ktable);
//	    }
//	  }
	  
public void processClause(ClauseRuntime C, HashMap<String,LinkedList<Term>> ktable) {
//	    if(C.getHead().matches(new AtomRuntime("init"))) {
//	      IO.mes("init: "+C.getBody());
//	      RuleEngineRuntime.firstSolution(C.getHead(),C.getBody());
//	    } else {
	      logger.trace("ADDING= "+C.pprint());
	      addClause(C,ktable);
//	    }
	  }
	  
// blackboard methods
  public String name() {
      return getClass().getName()+hashCode();
    }
    
  /**
     Removes the first Term having key k
     or the first enumerated key if k is null
  */
  // synchronized
  private final Term pick(String k) {
    if(k==null) {
      Iterator<String> e=this.keySet().iterator();
      if(!e.hasNext())
        return null;
      k=e.next();
      // IO.trace("$$Got key:"+k+this);
    }
    LinkedList<Term> Q=get(k);
    if(Q==null)
      return null;
    Term T= Q.removeFirst();
    if(Q.isEmpty()) {
      remove(k);
      // IO.trace("$$Removed key:"+k+this);
    }
    return T;
  }
  
  private final void addBack(String k,ArrayList<Term> V) {
    for(Iterator<Term> e=V.iterator();e.hasNext();) {
      // cannot be here if k==null
      add(k,e.next());
    }
  }
  
  /**
     Removes the first matching Term or Clause from the
     blackboard, to be used by Linda in/1 operation in
     PrologBlackBoard

     @see PrologBlackBoard#in()
  */
  
  // synchronized
  protected final Term take(String k,Term pattern) {
    ArrayList<Term> V=new ArrayList<>();
    Term t;
    while(true) {
      t=pick(k);
      if(null==t)
        break;
      // IO.trace("$$After pick: t="+t+this);
      if(t.matches(pattern))
        break;
      else
        V.add(t);
    }
    addBack(k,V);
    return t;
  }
  
  /**
     Adds a Term or Clause
     to the the blackboard, to be used by Linda out/1 operation

     @see PrologBlackBoard

  */
  // synchronized
//  protected final void add(String k,TermAstNode value) {
//	    LinkedList<Term> Q = get(k);
//	    if(Q==null) {
//	      Q=new LinkedList<>();
//	      put(k,Q);
//	    }
//	    Q.addLast(value);
//	    // IO.trace("$$Added key/val:"+k+"/"+value+"=>"+this);
//	  }
	  
  protected final void add(String k,Term value) {
	    LinkedList<Term> Q=get(k);
	    if(Q==null) {
	      Q=new LinkedList<>();
	      put(k,Q);
	    }
	    Q.addLast(value);
	     // IO.errmes("LinkedList full, key:"+k);
	    // IO.trace("$$Added key/val:"+k+"/"+value+"=>"+this);
	  }
	  
  /** This gives an enumeration view for the sequence of
       objects kept under key k.
  */
  // synchronized
  public Iterator<Term> toEnumerationFor(String k) {
	    LinkedList<Term> Q= get(k);
	    return Q.iterator();
	  }
  public LinkedList<Term> getClauses(String k) {
	    LinkedList<Term> Q=get(k);
	    return Q;
	  }
  
}
