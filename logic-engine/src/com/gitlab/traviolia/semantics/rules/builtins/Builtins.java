package com.gitlab.traviolia.semantics.rules.builtins;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.fluents.DataBase;
import com.gitlab.traviolia.semantics.rules.io.IO;
import com.gitlab.traviolia.semantics.rules.runtime.AbstractNonVar;
import com.gitlab.traviolia.semantics.rules.runtime.AbstractTerm;
import com.gitlab.traviolia.semantics.rules.runtime.Atom;
import com.gitlab.traviolia.semantics.rules.runtime.AtomRuntime;
import com.gitlab.traviolia.semantics.rules.runtime.Cons;
import com.gitlab.traviolia.semantics.rules.runtime.ConstBuiltinRuntime;
import com.gitlab.traviolia.semantics.rules.runtime.FunBuiltinRuntime;
import com.gitlab.traviolia.semantics.rules.runtime.Nil;
import com.gitlab.traviolia.semantics.rules.runtime.RuleEngine;
import com.gitlab.traviolia.semantics.rules.runtime.StructureRuntime;
import com.gitlab.traviolia.semantics.rules.runtime.Term;
import com.gitlab.traviolia.semantics.rules.terms.AtomAstNode;
import com.gitlab.traviolia.semantics.rules.terms.StructureAstNode;

/**
 * This class contains a dictionary of all builtins i.e. Java based classes
 * callable from Prolog. They should provide a constructor and an exec method.
 * 
 * @author Paul Tarau
 */
public class Builtins extends HashMap {

	private static final long serialVersionUID = 1L;

	private static class BuiltinsHolder {
		private static final Builtins INSTANCE = new Builtins();
	}

	public static Builtins getInstance() {
		return BuiltinsHolder.INSTANCE;
	}

	Logger logger = LogManager.getLogger();

	/**
	 * This constructor registers builtins. Please put a header here if you add
	 * a builtin at the bottom of this file.
	 */
	private Builtins() {
		// add a line here for each new builtin

		// basics
		register(new is_builtin());
		register(AtomRuntime.aTrue);
		register(AtomRuntime.aFail);
		register(new halt());
		register(new compute());

		// I/O and trace related
		register(new set_max_answers());
		// register(new set_trace());
		register(new stack_dump());
		register(new consult());
		register(new reconsult());
		register(new reconsult_again());

		// database
		// register(new at_key());
		// register(new pred_to_string());
		// register(new db_to_string());
		//
		// register(new new_db());
		// register(new get_default_db());
		// register(new db_remove());
		// register(new db_add());
		// register(new db_collect());

		// data structure builders/converters
		register(new arg());
		// register(new new_fun());
		register(new get_arity());
		register(new name_to_chars());
		register(new chars_to_name());
		// register(new numbervars());

		// // fluent constructors
		// register(new unfolder_source());
		// register(new answer_source());
		//
		// register(new source_list());
		//
		// register(new term_source());
		// register(new source_term());
		//
		// // Fluent Modifiers
		//
		// register(new set_persistent());
		// register(new get_persistent());
		//
		// // Input Sources
		// // register(new file_char_reader());
		// // register(new char_file_writer());
		// //
		// // register(new file_clause_reader());
		// // register(new clause_file_writer());
		//
		// // writable Sinks
		// register(new string_char_reader());
		// register(new string_clause_reader());
		//
		// // fluent controllers
		// register(new get());
		// register(new put());
		// register(new stop());
		// register(new collect());
		//
		// // fluent combinators
		// // see compose_sources,append_sources,merge_sources in lib.pro
		//
		// // discharges a Source to a Sink
		// register(new discharge());

		// OS and process interface
		register(new system());
		register(new ctime());
	}

	/**
	 * registers a symbol as name of a builtin
	 */
	public void register(AbstractNonVar proto) {
		String key = proto.name() + "/" + proto.getArity();
		logger.trace("registering builtin: " + key);
		put(key, proto);
	}

	/**
	 * Creates a new builtin
	 */
	public StructureRuntime newBuiltin(StructureAstNode S) {
		String className = S.getSymbol();
		int arity = S.getArity();
		String key = className + "/" + arity;
		logger.trace(key);
		FunBuiltinRuntime b = (FunBuiltinRuntime) get(key);
		return b;
	}

	public AtomRuntime newBuiltin(AtomAstNode S) {
		String className = S.getName();
		int arity = S.getArity();
		String key = className + "/" + arity;
		AtomRuntime b = (AtomRuntime) get(key);
		return b;
	}

	// public static AtomAstNode toConstBuiltin(AtomAstNode c) {
	// if(c.name().equals(AtomRuntime.aNil.name()))
	// return new ConstBuiltinWrapper(AtomRuntime.aNil);
	// if(c.name().equals(AtomRuntime.aNo.name()))
	// return new ConstBuiltinWrapper(AtomRuntime.aNo);
	// if(c.name().equals(AtomRuntime.aYes.name()))
	// return new ConstBuiltinWrapper(AtomRuntime.aYes);
	//
	// AtomRuntime B = getInstance().newBuiltin(c);
	// if(null==B) {
	// // IO.mes("not a builtin:"+this);
	// return c;
	// }
	// return new ConstBuiltinWrapper(B);
	// }

	// public static Structure toFunBuiltin(Structure f) {
	// if(f.name().equals(":-")&&f.getArity()==2) {
	// return new ClauseAstNode((Term) f.getArg(1), (Term)f.getArg(1));
	// }
	// if(f.name().equals(",")&&f.getArity()==2) {
	// return new ConjAstNode((Term) f.getArg(0), (Term) f.getArg(1));
	// }
	// FunBuiltin B=(FunBuiltin)getInstance().newBuiltin(f);
	// if(null==B)
	// return f;
	// B=(FunBuiltin)B.funClone();
	// B.setArgs(f.getArgs());
	// return B;
	// }

} // end Builtins

// Code for actual kernel Builtins:
// add your own builtins in UserBuiltins.java, by cloning the closest example:-)

/**
 * checks if something is a builtin
 */
final class is_builtin extends FunBuiltinRuntime {
	is_builtin() {
		super("is_builtin", 1);
	}

	public int exec(RuleEngine p) {
		return ((AbstractTerm) getArg(0)).isBuiltin() ? 1 : 0;
	}
}

/**
 * does its best to halt the program:-)
 */
final class halt extends ConstBuiltinRuntime {
	halt() {
		super("halt");
	}

	public int exec(RuleEngine p) {
		Runtime.getRuntime().exit(0);
		return 1;
	}
}

/**
 * Calls an external program
 */
final class system extends FunBuiltinRuntime {
	system() {
		super("system", 1);
	}

	public int exec(RuleEngine p) {
		String cmd = ((AtomRuntime) getArg(0)).name();
		return IO.system(cmd);
	}
}

/**
 * gets an arity for any term: n>0 for f(A1,...,An) 0 for a constant like a -1
 * for a variable like X -2 for an integer like 13 -3 for real like 3.14 -4 for
 * a wrapped JavaObject;
 * 
 * @see AbstractTerm#getArity
 */
final class get_arity extends FunBuiltinRuntime {
	get_arity() {
		super("get_arity", 2);
	}

	public int exec(RuleEngine p) {
		return putArg(1, new Integer(((Term) getArg(0)).getArity()), p);
	}
}

/**
 * Dumps the current Java Stack
 */
final class stack_dump extends FunBuiltinRuntime {

	stack_dump() {
		super("stack_dump", 1);
	}

	public int exec(RuleEngine p) {
		String s = getArg(0).toString();
		IO.errmes("User requested dump", (new Exception(s)));
		return 1;
	}
}

/**
 * returns the real time spent up to now
 */
final class ctime extends FunBuiltinRuntime {

	ctime() {
		super("ctime", 1);
	}

	private final static long t0 = System.currentTimeMillis();

	public int exec(RuleEngine p) {
		return putArg(0, new Long(System.currentTimeMillis() - t0), p);
	}
}

/**
 * sets max answer counter for toplevel query if == 0, it will prompt the user
 * for more answers if > 0 it will not print more than IO.maxAnswers if < 0 it
 * will print them out all
 */
final class set_max_answers extends FunBuiltinRuntime {
	set_max_answers() {
		super("set_max_answers", 1);
	}

	public int exec(RuleEngine p) {
		IO.maxAnswers = getIntArg(0);
		return 1;
	}
}

/**
 * reconsults a file of clauses while overwriting old predicate definitions
 * 
 * @see consult
 */

final class reconsult extends FunBuiltinRuntime {
	Logger logger = LogManager.getLogger();

	reconsult() {
		super("reconsult", 1);
	}

	@Override
	public int exec(RuleEngine p) {
		String f = ((Atom) getArg(0)).name();
		if (!f.startsWith("file:"))
		  f = this.getClass().getResource(f).toString();
		System.err.println(f);
		return DataBase.getInstance().fromFile(f, p) ? 1 : 0;
	}
}

/**
 * consults a file of clauses while adding clauses to existing predicate
 * definitions
 * 
 * @see reconsult
 */
final class consult extends FunBuiltinRuntime {
	consult() {
		super("consult", 1);
	}

	public int exec(RuleEngine p) {
		String f = ((Atom) getArg(0)).name();
		IO.trace("consulting: " + f);
		return DataBase.getInstance().fromFile(f, p, false) ? 1 : 0;
	}
}

/**
 * shorthand for reconsulting the last file
 */
final class reconsult_again extends ConstBuiltinRuntime {
	reconsult_again() {
		super("reconsult_again");
	}

	public int exec(RuleEngine p) {
		return DataBase.getInstance().fromFile(p) ? 1 : 0;
	}
}

/**
 * gets default database
 */
// final class get_default_db extends FunBuiltinRuntime {
// get_default_db(){
// super("get_default_db",1);
// }
//
// public int exec(RuleEngine p) {
// return putArg(0,new JavaObject(DataBase.getInstance()),p);
// }
// }

// databse operations

/**
 * creates new database
 */
// final class new_db extends FunBuiltinRuntime {
// new_db(){
// super("new_db",1);
// }
//
// public int exec(RuleEngine p) {
// return putArg(0,new JavaObject(new DataBase()),p);
// }
// }

/**
 * Puts a term on the local blackboard
 */
// final class db_add extends FunBuiltinRuntime {
//
// db_add(){
// super("db_add",2);
// }
//
// public int exec(RuleEngine p) {
// switch (Version.getInstance().getNumber()) {
// case 1: {
// DataBase db=(DataBase)((JavaObject)getArg(0)).toObject();
// AbstractTerm X=(AbstractTerm) getArg(1);
// // IO.mes("X==>"+X);
// String key=X.getKey();
// // IO.mes("key==>"+key);
// if(null==key)
// return 0;
// db.out(key,X);
// // IO.mes("res==>"+R);
// return 1;
// }
// case 2: {
// DataBase db=(DataBase)((JavaObject)getArg(0)).toObject();
// Term X=(Term)getArg(1);
// // IO.mes("X==>"+X);
// String key=X.getKey();
// // IO.mes("key==>"+key);
// if(null==key)
// return 0;
// db.out(key, X);
// // IO.mes("res==>"+R);
// return 1;
// }
// default:
// throw new RuntimeException();
// }
// }
// }

/**
 * removes a matching term if available, fails otherwise
 */
// final class db_remove extends FunBuiltinRuntime {
//
// db_remove(){
// super("db_remove",3);
// }
//
// public int exec(RuleEngine p) {
// DataBase db=(DataBase)((JavaObject)getArg(0)).toObject();
// Term X=(Term)getArg(1);
// Term R=db.cin(X.getKey(),X);
// return putArg(2,R,p);
// }
// }

/**
 * collects all matching terms in a (possibly empty) list
 * 
 * @see out
 * @see in
 */
// final class db_collect extends FunBuiltinRuntime {
//
// db_collect(){
// super("db_collect",3);
// }
//
// public int exec(RuleEngine p) {
// DataBase db=(DataBase)((JavaObject)getArg(0)).toObject();
// AbstractTerm X=(AbstractTerm)getArg(1);
// Term R=db.all(X.getKey(),X);
// return putArg(2,R,p);
// }
// }

/**
 * collects all matching terms in a (possibly empty) list
 */
// final class at_key extends FunBuiltinRuntime {
//
// at_key(){
// super("at_key",2);
// }
//
// public int exec(RuleEngine p) {
// switch (Version.getInstance().getNumber()) {
// case 1: {
// Term R=DataBase.getInstance().all(((AbstractTerm)getArg(0)).getKey(),new
// VariableRuntime());
// return putArg(1,R,p);
// }
// case 2: {
// Term R=DataBase.getInstance().all(((Term)getArg(0)).getKey(),new
// VariableRuntime());
// return putArg(1,R,p);
// }
// default:
// throw new RuntimeException();
// }
// }
// }

/**
 * Returns a representation of predicate as a string constant
 */
final class pred_to_string extends FunBuiltinRuntime {

	pred_to_string() {
		super("pred_to_string", 2);
	}

	public int exec(RuleEngine p) {
		String key = ((AbstractTerm) getArg(0)).getKey();
		String listing = DataBase.getInstance().pred_to_string(key);
		if (null == listing)
			return 0;
		Atom R = new AtomRuntime(listing);
		return putArg(1, R, p);
	}
}

/**
 * lists all the local blackboard to a string (Linda terms + clauses)
 */
final class db_to_string extends FunBuiltinRuntime {
	db_to_string() {
		super("db_to_string", 1);
	}

	public int exec(RuleEngine p) {
		return putArg(0, new AtomRuntime(DataBase.getInstance().pprint()), p);
	}
}

/**
 * arg(I,Term,X) unifies X with the I-the argument of functor T
 */
final class arg extends FunBuiltinRuntime {
	arg() {
		super("arg", 3);
	}

	public int exec(RuleEngine p) {
		int i = getIntArg(0);
		StructureRuntime F = (StructureRuntime) getArg(1);
		Object A = (i == 0) ? new AtomRuntime(F.name())
				: ((i == -1) ? new Integer(F.getArity()) : (Term) F.args[i - 1]);
		return putArg(2, A, p);
	}
}

// /**
// new_fun(F,N,T) creates a term T based on functor F with arity
// N and new free varables as arguments
// */
// final class new_fun extends FunBuiltinRuntime {
// new_fun(){
// super("new_fun",3);
// }
//
// public int exec(RuleEngine p) {
// String s=((AtomRuntime)getArg(0)).name();
// int i=getIntArg(1);
// Term T;
// if(i==0)
// T=BuiltinsRuntime.toConstBuiltin(new AtomRuntime(s));
// else {
// StructureRuntime F=new StructureRuntime(s);
// F.init(i);
// for(int j=0;j<F.getArity();j++) {
// F.args[j]=new VariableRuntime();
// }
// T=BuiltinsRuntime.toFunBuiltin(F);
// }
// return putArg(2,T,p);
// }
// }

/**
 * converts a name to a list of chars
 */
final class name_to_chars extends FunBuiltinRuntime {
	name_to_chars() {
		super("name_to_chars", 2);
	}

	public int exec(RuleEngine p) {
		Term Cs = ((AbstractNonVar) getArg(0)).toChars();
		return putArg(1, Cs, p);
	}
}

/**
 * converts a name to a list of chars
 */
final class chars_to_name extends FunBuiltinRuntime {
	chars_to_name() {
		super("chars_to_name", 3);
	}

	public static String charsToString(Term Cs) {
		StringBuffer s = new StringBuffer("");

		while (!(Cs instanceof Nil)) {
			if (!(Cs instanceof Cons))
				return null;
			Object head = (Term) ((Cons) Cs).getArg(0);
			if (!(head instanceof Integer))
				return null;
			char c = (char) ((Integer) head).intValue();
			s.append(c);
			Cs = (Term) ((Cons) Cs).getArg(1);
		}

		return s.toString();
	}

	public int exec(RuleEngine p) {
		int convert = getIntArg(0);
		String s = charsToString((AbstractNonVar) getArg(1));
		Object T = new AtomRuntime(s);
		if (convert > 0) {
			try {
				double r = Double.valueOf(s).doubleValue();
				if (Math.floor(r) == r)
					T = new Long((long) r);
				else
					T = new Double(r);
			} catch (NumberFormatException e) {
			}
		}
		return putArg(2, T, p);
	}
}

/**
 * Performs simple arithmetic operations like compute('+',1,2,Result)
 */
final class compute extends FunBuiltinRuntime {
	compute() {
		super("compute", 4);
	}

	public int exec(RuleEngine p) {

		AbstractTerm o = (AbstractTerm) getArg(0);
		Object a = (AbstractTerm) getArg(1);
		Object b = (AbstractTerm) getArg(2);
		if (!(o instanceof AtomRuntime) || !(a instanceof Number)
				|| !(b instanceof Number))
			IO.errmes("bad arithmetic operation (" + o + "): " + a + "," + b
					+ "\nprog: " + p.toString());
		String opname = ((AtomRuntime) o).name();
		double x = ((Number) a).doubleValue();
		double y = ((Number) b).doubleValue();
		double r;
		char op = opname.charAt(0);
		switch (op) {
		case '+':
			r = x + y;
			break;
		case '-':
			r = x - y;
			break;
		case '*':
			r = x * y;
			break;
		case '/':
			r = x / y;
			break;
		case ':':
			r = (int) (x / y);
			break;
		case '%':
			r = x % y;
			break;
		case '?':
			r = (x < y) ? (-1) : (x == y ? 0 : 1);
			break; // compares!
		case 'p':
			r = Math.pow(x, y);
			break;
		case 'l':
			r = Math.log(y) / Math.log(x);
			break;
		case 'r':
			r = x * Math.random() + y;
			break;
		case '<':
			r = (long) x << (long) y;
			break;
		case '>':
			r = (long) x >> (long) y;
			break;

		default:
			IO.errmes("bad arithmetic operation <" + op + "> on " + x + " and "
					+ y);
			return 0;
		}
		Object R = ((Math.floor(r) == r)) ? new Long((long) r) : new Double(r);
		return putArg(3, R, p);
	}
}
