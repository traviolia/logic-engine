package com.gitlab.traviolia.semantics.rules;

import java.util.Map;

import com.gitlab.traviolia.semantics.rules.fluents.DataBase;
import com.gitlab.traviolia.semantics.rules.io.IO;
import com.gitlab.traviolia.semantics.rules.runtime.ClauseRuntime;
import com.gitlab.traviolia.semantics.rules.runtime.LogicEngineImpl;
import com.gitlab.traviolia.semantics.rules.runtime.RuleEngine;
import com.gitlab.traviolia.semantics.rules.runtime.Term;

/**
  Initializes Prolog. Sets up shared data areas.
  Ensures that lib.class, obtained from lib.pro->lib.java is loaded.
*/
public class Init {
  public static final int version=101;
  
  public static final String getInfo() {
    String s="Kernel Prolog "+version/100.0+"\n"
        +"Copyright (c) Paul Tarau 1999-2011\n";
    return s;
  }
  
//  public static String default_lib="src/com/gitlab/traviolia/semantics/rules/lib.pro";
  public static String default_lib="/lib.pro";
  
  public static ClauseRuntime getGoal(String line, RuleEngine engine) {
	    ClauseRuntime G=ClauseRuntime.goalFromString(line, engine);
	    IO.mes("getGoal: "+G+" DICT: "+G.dict); //OK
	    return G;
	  }
	  
  /**
  * reads a query from input strea
  */
  static ClauseRuntime getGoal(RuleEngine engine) {
    return getGoal(IO.promptln("?- "), engine);
  }
  
  /**
  * evalutes a query
  */
  public static void evalGoal(ClauseRuntime Goal, RuleEngine engine) {
	    ClauseRuntime NamedGoal=Goal.cnumbervars(false);
//	    Term Names=NamedGoal.getHead();
//	    if(!(Names instanceof StructureRuntime)) { // no vars in Goal
//	      Term Result=LogicEngineImpl.firstSolution(Goal.getHead(),Goal.getBody());
//	      if(!AtomRuntime.aNo.equals(Result))
//	        Result=AtomRuntime.aYes;
//	      IO.println(Result.toString());
//	      return;
//	    }
	    
	    LogicEngineImpl E=new LogicEngineImpl(Goal);
	    
	    for(int i=0;;i++) {
	      Map<String, Object> R = E.get();
	      // IO.mes("GOAL:"+Goal+"\nANSWER: "+R);
	      if(R==null) {
	        IO.println("no");
	        break;
	      }
	        for(Map.Entry<String,Object> entry : R.entrySet()) {	        	
	          IO.println(entry.getKey()+"="+entry.getValue().toString());
	        }
	        // IO.println(";");
	        if(!moreAnswers(i)) {
	          E.stop();
	          break;
	        }
	      
	    }
	  }
	  
  static boolean moreAnswers(int i) {
    if(IO.maxAnswers==0) { // under user control
      String more=IO.promptln("; for more, <enter> to stop: ");
      return more.equals(";");
    } else if(i<IO.maxAnswers||IO.maxAnswers<0) {
      IO.println(";"); // print all remaining
      return true;
    } else { // i >= ...}
      IO.println(";");
      IO.println("No more answers computed, max reached! ("+IO.maxAnswers+")");
      return false;
    }
  }
  
  /**
  *  evaluates and times a Goal querying program P
  */
  
  public static void timeGoal(ClauseRuntime Goal, RuleEngine engine) {
	    long t1=System.currentTimeMillis();
	    try {
	      evalGoal(Goal, engine);
	    } catch(RuntimeException e) {
	      IO.errmes("Execution error in goal:\n  "+Goal.pprint()+".\n",e);
	    }
	    long t2=System.currentTimeMillis();
	    IO.println("Time: "+(t2-t1)/1000.0+" sec");
	  }
  
  /**
  *  (almost) standard Prolog-like toplevel in Java
  *  (will) print out variables and values
  */
	  public static void standardTop(RuleEngine engine) {
		    standardTop("?- ", engine);
		  }
		  
		  public static void standardTop(String prompt, RuleEngine engine) {
		    for(;;) {
		      ClauseRuntime G=getGoal(IO.promptln(prompt), engine);
		      if(null==G) {
		        continue;
		      }
		      IO.peer=null;
		      timeGoal(G, engine);
		    }
		  }
		  
  /**
   Asks Prolog a query Answer, Goal and returns the
   first solution of the form "the(Answer)" or the constant
   "no" if no solution exists
  */
  public static Term askProlog(Term Answer,Term Body) {
    return LogicEngineImpl.firstSolution(Answer,Body);
  }
  
  /**
    Asks Prolog a query Goal and returns the
    first solution of the form "the(Answer)" , where
    Answer is an instance of Goal or the constant
    "no" if no solution exists
  */
  public static Term askProlog(Term Goal) {
    return askProlog(Goal,Goal);
  }
  
  /**
    Asks Prolog a String query and gets back a string Answer
    of the form "the('[]'(VarsOfQuery))" containing a binding
    of the variables or the first solution to the query or "no"
    if no such solution exists
  */
  public static String askProlog(String query, RuleEngine engine) {
    ClauseRuntime Goal=getGoal(query, engine);
    Term Body=Goal.getBody();
    return askProlog(Body).pprint();
  }
  
  public static boolean run(String[] args, RuleEngine engine) {
    if(null!=args) {
      for(int i=0;i<args.length;i++) {
        String result=askProlog(args[i], engine);
        IO.trace(result);
        if("no".equals(result.intern())) {
          IO.errmes("failing cmd line argument: "+args[i]);
          return false;
        }
      }
    }
    return true;
  }
  
  /**
     Initialises key data areas. Runs a first query, which,
     if suceeeds a true, otherwise false is returned
  */
  public static final boolean startProlog() {
    // should be final for expiration mechanism (it should avoid overriding!)
    IO.println(getInfo());
    DataBase.getInstance();
    return true;
  }
  
}
