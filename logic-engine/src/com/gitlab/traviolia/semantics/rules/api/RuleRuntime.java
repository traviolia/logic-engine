package com.gitlab.traviolia.semantics.rules.api;

import java.util.List;
import java.util.Map;

import javax.rules.RuleExecutionSetNotFoundException;
import javax.rules.RuleSessionCreateException;
import javax.rules.RuleSessionTypeUnsupportedException;

public interface RuleRuntime {

    int STATELESS_SESSION_TYPE = 1;
    int STATEFUL_SESSION_TYPE = 2;

    /**
     * Creates a <code>RuleSession</code> implementation using the supplied
     * Drools-specific rule execution set registration URI.
     * 
     * @param uri
     *            the URI for the <code>RuleExecutionSet</code>
     * @param properties
     *            additional properties used to create the
     *            <code>RuleSession</code> implementation.
     * @param ruleSessionType
     *            the type of rule session to create.
     * 
     * @throws RuleSessionTypeUnsupportedException
     *             if the ruleSessionType is not supported by Drools or the
     *             RuleExecutionSet
     * @throws RuleExecutionSetNotFoundException
     *             if the URI could not be resolved into a
     *             <code>RuleExecutionSet</code>
     * 
     * @return The created <code>RuleSession</code>.
     */
    public abstract RuleSession createRuleSession(String uri, Map properties, int ruleSessionType)
            throws RuleSessionTypeUnsupportedException, RuleSessionCreateException, RuleExecutionSetNotFoundException;

    /**
     * Retrieves a <code>List</code> of the URIs that currently have
     * <code>RuleExecutionSet</code>s associated with them. An empty list is
     * returned is there are no associations.
     * 
     * @return a <code>List</code> of <code>String</code>s (URIs)
     */
    public abstract List getRegistrations();

}