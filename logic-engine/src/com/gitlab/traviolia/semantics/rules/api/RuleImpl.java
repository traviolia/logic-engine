/*
 * Copyright 2005 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gitlab.traviolia.semantics.rules.api;


import java.util.HashMap;
import java.util.Map;

/**
 * The Drools implementation of the <code>Rule</code> interface which provides
 * access to simple metadata for a rule. Related <code>Rule</code>
 * instances are assembled into <code>RuleExecutionSet</code>s, which in
 * turn, can be executed by a rules engine via the <code>RuleSession</code>
 * interface.
 *
 * @see Rule
 */
public class RuleImpl implements Rule {
    private static final long    serialVersionUID = 510l;

    /** The name of this rule. */
    private String               name;

    /** A description of the rule or null if no description is specified. */
    private String               description;

    /** A <code>Map</code> of user-defined and Drools-defined properties. */
    private final Map            properties       = new HashMap();

    /**
     * Creates a <code>RuleImpl</code> object by wrapping an
     * <code>org.kie.rule.Rule</code> object.
     *
     * @param rule the <code>org.kie.rule.Rule</code> object to be wrapped.
     */
    RuleImpl() {
    }

    /**
     * Returns the <code>org.kie.rule.Rule</code> that lies at the core of
     * this <code>javax.rules.admin.Rule</code> object. This method is package
     * private.
     *
     * @return <code>org.kie.rule.Rule</code> at the core of this object.
     */
    Object getRule() {
        return null;
    }

    /* Rule interface methods */

    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.Rule#getName()
     */
    @Override
    public String getName() {
        return this.name;
    }

    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.Rule#getDescription()
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.Rule#getProperty(java.lang.Object)
     */
    @Override
    public Object getProperty(final Object key) {
        // TODO certain keys should reference internal rule accessor methods
        return this.properties.get( key );
    }

    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.Rule#setProperty(java.lang.Object, java.lang.Object)
     */
    @Override
    public void setProperty(final Object key,
                            final Object value) {
        // TODO certain keys should alter internal rule accessor methods
        this.properties.put( key,
                             value );
    }
}

