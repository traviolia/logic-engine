package com.gitlab.traviolia.semantics.rules.api;

import java.util.List;

import javax.rules.Handle;
import javax.rules.InvalidHandleException;
import javax.rules.InvalidRuleSessionException;
import javax.rules.ObjectFilter;

public interface StatefulRuleSession {

    /**
     * Returns <code>true</code> if the given object is contained within
     * rulesession state of this rule session.
     * 
     * @param objectHandle
     *            the handle to the target object.
     * 
     * @return <code>true</code> if the given object is contained within the
     *         rule session state of this rule session.
     */
    public abstract boolean containsObject(Handle objectHandle);

    /**
     * Adds a given object to the rule session state of this rule session. The
     * argument to this method is Object because in the non-managed env. not all
     * objects should have to implement Serializable. If the
     * <code>RuleSession</code> is <code>Serializable</code> and it contains
     * non-serializable fields a runtime exception will be thrown.
     * 
     * @param object
     *            the object to be added.
     * 
     * @return the Handle for the newly added Object
     * 
     * @throws InvalidRuleSessionException
     *             on illegal rule session state.
     */
    public abstract Handle addObject(Object object) throws InvalidRuleSessionException;

    /**
     * Adds a <code>List</code> of <code>Object</code>s to the rule session
     * state of this rule session.
     * 
     * @param objList
     *            the objects to be added.
     * 
     * @return a <code>List</code> of <code>Handle</code>s, one for each
     *         added <code>Object</code>. The <code>List</code> must be
     *         ordered in the same order as the input <code>objList</code>.
     * 
     * @throws InvalidRuleSessionException
     *             on illegal rule session state.
     */
    public abstract List addObjects(List objList) throws InvalidRuleSessionException;

    /**
     * Notifies the rules engine that a given object in the rule session state
     * has changed. <p/> The semantics of this call are equivalent to calling
     * <code>removeObject</code> followed by <code>addObject</code>. The
     * original <code>Handle</code> is rebound to the new value for the
     * <code>Object</code> however.
     * 
     * @param objectHandle
     *            the handle to the original object.
     * @param newObject
     *            the new object to bind to the handle.
     * 
     * @throws InvalidRuleSessionException
     *             on illegal rule session state.
     * @throws InvalidHandleException
     *             if the input <code>Handle</code> is no longer valid
     */
    public abstract void updateObject(Handle objectHandle, Object newObject) throws InvalidRuleSessionException,
            InvalidHandleException;

    /**
     * Removes a given object from the rule session state of this rule session.
     * 
     * @param handleObject
     *            the handle to the object to be removed from the rule session
     *            state.
     * 
     * @throws InvalidRuleSessionException
     *             on illegal rule session state.
     * @throws InvalidHandleException
     *             if the input <code>Handle</code> is no longer valid
     */
    public abstract void removeObject(Handle handleObject) throws InvalidRuleSessionException, InvalidHandleException;

    /**
     * Executes the rules in the bound rule execution set using the objects
     * present in the rule session state. This will typically modify the rule
     * session state - and may add, remove or update <code>Object</code>s
     * bound to <code>Handle</code>s.
     * 
     * @throws InvalidRuleSessionException
     *             on illegal rule session state.
     */
    public abstract void executeRules() throws InvalidRuleSessionException;

    /**
     * @see StatefulRuleSessionImpl
     */
    public abstract Object getObject(Handle handle) throws InvalidRuleSessionException, InvalidHandleException;

    /**
     * Returns a <code>List</code> of the <code>Handle</code>s being used
     * for object identity.
     * 
     * @return a <code>List</code> of <code>Handle</code>s present in the
     *         currect state of the rule session.
     */
    public abstract List getHandles();

    /**
     * Returns a List of all objects in the rule session state of this rule
     * session. The objects should pass the default filter test of the default
     * <code>RuleExecutionSet</code> filter (if present). <p/> This may not
     * neccessarily include all objects added by calls to <code>addObject</code>,
     * and may include <code>Object</code>s created by side-effects. The
     * execution of a <code>RuleExecutionSet</code> can add, remove and update
     * objects as part of the rule session state. Therefore the rule session
     * state is dependent on the rules that are part of the executed
     * <code>RuleExecutionSet</code> as well as the rule vendor's specific
     * rule engine behavior.
     * 
     * @return a <code>List</code> of all objects part of the rule session
     *         state.
     * 
     * @throws InvalidRuleSessionException
     *             on illegal rule session state.
     */
    public abstract List getObjects() throws InvalidRuleSessionException;

    /**
     * Resets this rule session. Calling this method will bring the rule session
     * state to its initial state for this rule session and will reset any other
     * state associated with this rule session.
     * <p/>
     * A reset will not reset the state on the default object filter for a
     * <code>RuleExecutionSet</code>.
     */
    public abstract void reset();

    public abstract int getType() throws InvalidRuleSessionException;

    /**
     * Releases all resources used by this rule session.
     * This method renders this rule session unusable until
     * it is reacquired through the <code>RuleRuntime</code>.
     */
    public abstract void release();

}