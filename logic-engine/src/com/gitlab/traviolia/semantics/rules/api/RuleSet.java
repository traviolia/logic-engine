package com.gitlab.traviolia.semantics.rules.api;

import java.util.List;

public interface RuleSet {

    /**
     * Get the name of this rule execution set.
     *
     * @return The name of this rule execution set.
     */
    public abstract String getName();

    /**
     * Get a description of this rule execution set.
     *
     * @return A description of this rule execution set or null of no
     *         description is specified.
     */
    public abstract String getDescription();

    /**
     * Return a list of all <code>Rule</code>s that are part of the
     * <code>RuleExecutionSet</code>.
     *
     * @return a list of all <code>Rule</code>s that are part of the
     *         <code>RuleExecutionSet</code>.
     */
    public abstract List getRules();

}