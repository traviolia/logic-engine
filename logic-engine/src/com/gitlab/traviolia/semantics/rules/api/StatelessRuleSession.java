package com.gitlab.traviolia.semantics.rules.api;

import java.util.List;

import javax.rules.InvalidRuleSessionException;

public interface StatelessRuleSession extends RuleSession {

    /**
     * Executes the rules in the bound rule execution set using the supplied
     * list of objects. A <code>List</code> is returned containing the objects
     * created by (or passed into the rule session) the executed rules that pass
     * the filter test of the default <code>RuleExecutionSet</code>
     * <code>ObjectFilter</code>
     * (if present). <p/> The returned list may not neccessarily include all
     * objects passed, and may include <code>Object</code>s created by
     * side-effects. The execution of a <code>RuleExecutionSet</code> can add,
     * remove and update objects. Therefore the returned object list is
     * dependent on the rules that are part of the executed
     * <code>RuleExecutionSet</code> as well as Drools specific rule engine
     * behavior.
     * 
     * @param objects
     *            the objects used to execute rules.
     * 
     * @return a <code>List</code> containing the objects as a result of
     *         executing the rules.
     * 
     * @throws InvalidRuleSessionException
     *             on illegal rule session state.
     */
    public abstract List executeRules(List objects) throws InvalidRuleSessionException;

    public abstract int getType() throws InvalidRuleSessionException;

}