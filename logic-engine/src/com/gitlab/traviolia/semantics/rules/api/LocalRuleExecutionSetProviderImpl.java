/*
 * Copyright 2010 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.traviolia.semantics.rules.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Map;

import javax.rules.admin.LocalRuleExecutionSetProvider;
import javax.rules.admin.RuleExecutionSet;
import javax.rules.admin.RuleExecutionSetCreateException;

/**
 * The Drools implementation of the <code>LocalRuleExecutionSetProvider</code>
 * interface which defines <code>RuleExecutionSet</code> creation methods for
 * defining <code>RuleExecutionSet</code>s from local (non-serializable)
 * resources.
 *
 * @see LocalRuleExecutionSetProvider
 */
public class LocalRuleExecutionSetProviderImpl
    implements
    LocalRuleExecutionSetProvider {
    /** Default constructor. */
    public LocalRuleExecutionSetProviderImpl() {
        super();
    }

    /**
     * Creates a <code>RuleExecutionSet</code> implementation using a supplied
     * input stream and additional Drools-specific properties. A Drools-specific
     * rule execution set is read from the supplied InputStream. The method
     * <code>createRuleExecutionSet</code> taking a Reader instance should be
     * used if the source is a character stream and encoding conversion should
     * be performed.
     *
     * @param ruleExecutionSetStream
     *            an input stream used to read the rule execution set.
     * @param properties
     *            additional properties used to create the
     *            <code>RuleExecutionSet</code> implementation. May be
     *            <code>null</code>.
     *
     * @throws RuleExecutionSetCreateException
     *             on rule execution set creation error.
     *
     * @return The created <code>RuleExecutionSet</code>.
     */
    public RuleExecutionSet createRuleExecutionSet(final InputStream ruleExecutionSetStream,
                                                   final Map properties) throws RuleExecutionSetCreateException {
        return null;
    }
    


    /**
     * Creates a <code>RuleExecutionSet</code> implementation using a supplied
     * character stream Reader and additional Drools-specific properties. A
     * Drools-specific rule execution set is read from the supplied Reader.
     *
     * @param ruleExecutionSetReader
     *            a Reader used to read the rule execution set.
     * @param properties
     *            additional properties used to create the
     *            <code>RuleExecutionSet</code> implementation. May be
     *            <code>null</code>.
     *
     * @throws RuleExecutionSetCreateException
     *             on rule execution set creation error.
     *
     * @return The created <code>RuleExecutionSet</code>.
     */
    public RuleExecutionSet createRuleExecutionSet(final Reader ruleExecutionSetReader,
                                                   final Map properties) throws RuleExecutionSetCreateException {
        return null;
    }
    
    
    /**
     * Creates a <code>RuleExecutionSet</code> implementation from a
     * Drools-specific AST representation and Drools-specific properties.
     *
     * @param ruleExecutionSetAst
     *            the vendor representation of a rule execution set
     * @param properties
     *            additional properties used to create the
     *            <code>RuleExecutionSet</code> implementation. May be
     *            <code>null</code>.
     *
     * @throws RuleExecutionSetCreateException
     *             on rule execution set creation error.
     *
     * @return The created <code>RuleExecutionSet</code>.
     */
    public RuleExecutionSet createRuleExecutionSet(final Object ruleExecutionSetAst,
                                                   final Map properties) throws RuleExecutionSetCreateException {
        return null;
    }
}

