package com.gitlab.traviolia.semantics.rules.api;

import javax.rules.RuleExecutionSetMetadata;

interface RuleSession {

    /**
     * Returns the meta data for the rule execution set bound to this rule
     * session.
     *
     * @return the RuleExecutionSetMetaData bound to this rule session.
     */
    public abstract RuleExecutionSetMetadata getRuleExecutionSetMetadata();

    /**
     * Releases all resources used by this rule session.
     * This method renders this rule session unusable until
     * it is reacquired through the <code>RuleRuntime</code>.
     */
    public abstract void release();

}