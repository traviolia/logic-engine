/*
 * Copyright 2005 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.traviolia.semantics.rules.api;


import java.util.ArrayList;
import java.util.List;

import javax.rules.admin.RuleExecutionSet;

/**
 * The Drools implementation of the <code>RuleExecutionSet</code> interface
 * which defines a named set of executable <code>Rule</code> instances. A
 * <code>RuleExecutionSet</code> can be executed by a rules engine via the
 * <code>RuleSession</code> interface.
 *
 * @see RuleExecutionSet
 */
public class RuleSetImpl implements RuleSet {

    /**
     * A description of this rule execution set or null if no
     * description is specified.
     */
    private String            description;

    /**
     * Instances of this class should be obtained from the
     * <code>LocalRuleExecutionSetProviderImpl</code>. Each
     * <code>RuleExecutionSetImpl</code> corresponds with an
     * <code>org.kie.Package</code> object.
     *
     * @param pkg The <code>Package</code> to associate with this
     *        <code>RuleExecutionSet</code>.
     * @param properties A <code>Map</code> of user-defined and
     *        Drools-defined properties. May be <code>null</code>.
     */
    RuleSetImpl() {
    }

    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.Rule#getName()
     */
    @Override
    public String getName() {
        return null;
    }

    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.Rule#getDescription()
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.Rule#getRules()
     */
    @Override
    public List getRules() {
        final List jsr94Rules = new ArrayList();

        return jsr94Rules;
    }
}

