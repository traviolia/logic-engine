package com.gitlab.traviolia.semantics.rules.api;

public interface Rule {

    /**
     * Get the name of this rule.
     *
     * @return The name of this rule.
     */
    public abstract String getName();

    /**
     * Get a description of the rule.
     *
     * @return A description of the rule or null of no description is specified.
     */
    public abstract String getDescription();

    /**
     * Get a user-defined or Drools-defined property.
     *
     * @param key the key to use to retrieve the property
     *
     * @return the value bound to the key or <code>null</code>
     */
    public abstract Object getProperty(Object key);

    /**
     * Set a user-defined or Drools-defined property.
     *
     * @param key the key for the property value
     * @param value the value to associate with the key
     */
    public abstract void setProperty(Object key, Object value);

}