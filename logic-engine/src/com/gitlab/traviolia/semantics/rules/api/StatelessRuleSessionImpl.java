/*
 * Copyright 2010 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gitlab.traviolia.semantics.rules.api;

import java.util.List;
import java.util.Map;

import javax.rules.InvalidRuleSessionException;
import javax.rules.RuleExecutionSetNotFoundException;
import javax.rules.RuleSessionCreateException;

/**
 * The Drools implementation of the <code>StatelessRuleSession</code>
 * interface which is a representation of a stateless rules engine session. A
 * stateless rules engine session exposes a stateless rule execution API to an
 * underlying rules engine.
 * 
 * @see StatelessRuleSession
 */
public class StatelessRuleSessionImpl extends AbstractRuleSessionImpl implements StatelessRuleSession {
    /**
     * Gets the <code>RuleExecutionSet</code> for this URI and associates it
     * with a RuleBase.
     * 
     * @param bindUri
     *            the URI the <code>RuleExecutionSet</code> has been bound to
     * @param properties
     *            additional properties used to create the
     *            <code>RuleSession</code> implementation.
     * 
     * @throws RuleExecutionSetNotFoundException
     *             if there is no rule set under the given URI
     * @throws RuleSessionCreateException 
     */
    StatelessRuleSessionImpl(final String bindUri,
                             final Map properties,
                             final RuleExecutionSetRepository repository)
    throws RuleExecutionSetNotFoundException, RuleSessionCreateException {

        super( repository );
        setProperties( properties );

        RuleSetImpl ruleSet = null;
        
        try {
            ruleSet = (RuleSetImpl)
            repository.getRuleExecutionSet(bindUri, properties);
        } catch (RuleExecutionSetRepositoryException e) {
            String s = "Error while retrieving rule execution set bound to: " + bindUri;
            throw new RuleSessionCreateException(s, e);
        }

        if ( ruleSet == null ) {
            throw new RuleExecutionSetNotFoundException( "RuleExecutionSet unbound: " + bindUri );
        }

        setRuleExecutionSet( ruleSet );
    }
    
    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.StatelessRuleSession#executeRules(java.util.List)
     */
    @Override
    public List executeRules(final List objects) throws InvalidRuleSessionException {
        return executeRules( objects );
    }
    
    /* (non-Javadoc)
     * @see com.github.lycastus.semantics.rules.api.StatelessRuleSession#getType()
     */
    @Override
    public int getType() throws InvalidRuleSessionException {
        return RuleRuntime.STATELESS_SESSION_TYPE;
    }
    
    /**
     * Ensures this <code>RuleSession</code> is not
     * in an illegal rule session state.
     *
     * @throws InvalidRuleSessionException on illegal rule session state.
     */
    protected void checkRuleSessionValidity() throws InvalidRuleSessionException {
        if ( getRuleExecutionSet() == null ) {
            throw new InvalidRuleSessionException( "invalid rule session" );
        }
    }

}

