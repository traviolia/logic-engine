package com.gitlab.traviolia.semantics.rules;


public class Version {

    private static class VersionHolder {
        private static final Version INSTANCE= new Version();
	}
	
	public static Version getInstance() {
        return VersionHolder.INSTANCE;
	}
	
	int number;

  public Version(){
    super();
  }
  
  public int getNumber() {
	  return number;
  }
  
  public void setNumber(int number) {
	  this.number = number;
  }
  
}
