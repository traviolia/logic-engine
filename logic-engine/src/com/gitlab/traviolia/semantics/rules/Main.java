package com.gitlab.traviolia.semantics.rules;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.gitlab.traviolia.semantics.rules.builtins.Builtins;
import com.gitlab.traviolia.semantics.rules.fluents.DataBase;
import com.gitlab.traviolia.semantics.rules.runtime.LogicEngineImpl;
import com.gitlab.traviolia.semantics.rules.runtime.RuleEngine;
import com.gitlab.traviolia.semantics.rules.runtime.Term;

/**
   Minimal command line only Prolog main entry point
*/
public class Main {
	static final private int VERSION = 2;
  public static int init(RuleEngine engine) {
    if(!Init.startProlog())
      return 0;
    Builtins.getInstance();
    Init.askProlog("reconsult('"+Init.default_lib+"')", engine);
    HashMap<String, LinkedList<Term>> map = DataBase.getInstance();
    for (Map.Entry<String,LinkedList<Term>> entry : map.entrySet())
    	System.out.println(entry);
    return 1;
  }
  
  public static void main(String args[]) {
	  Version.getInstance().setNumber(VERSION);
	    LogicEngineImpl engine = new LogicEngineImpl((Term) null);
    if(0==init(engine))
      return;
    Init.standardTop(engine); // interactive
  }
}
