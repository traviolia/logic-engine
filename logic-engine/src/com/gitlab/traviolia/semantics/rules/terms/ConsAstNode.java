/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.terms;




/**
  List Constructor. Cooperates with terminator Nil.

  @see NilAstNode
*/
public class ConsAstNode extends StructureAstNode {
  public ConsAstNode(String cons,TermAstNode x0,TermAstNode x1){
    super(ConsAstNode.class, cons,x0,x1);
  }
  
  public ConsAstNode(TermAstNode x0,TermAstNode x1){
    this(".",x0,x1);
  }
  
	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

  public TermAstNode getHead() {
    return (TermAstNode)getArg(0);
  }
  
  public TermAstNode getTail() {
    return (TermAstNode)getArg(1);
  }
  
  /**
    List printer.
  */
  public String toString() {
    TermAstNode h=(TermAstNode)getArg(0);
    TermAstNode t=(TermAstNode)getArg(1);
    StringBuffer s=new StringBuffer("["+watchNull(h));
    for(;;) {
      if(t instanceof NilAstNode) {
        s.append("]");
        break;
      } else if(t instanceof ConsAstNode) {
        h=(TermAstNode)((ConsAstNode)t).getArg(0);
        t=(TermAstNode)((ConsAstNode)t).getArg(1);
        s.append(","+watchNull(h));
      } else {
        s.append("|"+watchNull(t)+"]");
        break;
      }
    }
    return s.toString();
  }
}
