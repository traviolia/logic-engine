package com.gitlab.traviolia.semantics.rules.terms;

/**
  Special constant terminating a list
*/
public class NilAstNode extends AtomAstNode {
  public NilAstNode(String s){
    super(NilAstNode.class, s);
  }
  
  public NilAstNode(){
    this("[]");
  }
}
