/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.terms;



public class ConjAstNode extends ConsAstNode {
  public ConjAstNode(TermAstNode first,TermAstNode x1){
    super(",",first,x1);
  }
  
	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

  public String conjToString() {
    TermAstNode h= (TermAstNode) ((TermAstNode) args[0]);
    TermAstNode t= (TermAstNode) ((TermAstNode) args[1]);
    StringBuffer s=new StringBuffer(watchNull(h));
    for(;;) {
      if(!(t instanceof ConjAstNode)) {
        s.append(","+t);
        break;
      } else {
        h=(TermAstNode) ((TermAstNode) ((ConjAstNode)t).args[0]);
        t=(TermAstNode) ((TermAstNode) ((ConjAstNode)t).args[1]);
        s.append(","+watchNull(h));
      }
    }
    return s.toString();
  }
  
  public String toString() {
    return funToString();
  }
  
  static public final TermAstNode getHead(TermAstNode T) {
    return (T instanceof ConjAstNode)?(TermAstNode)((ConjAstNode)T).getArg(0):T;
  }
  
  static public final TermAstNode getTail(TermAstNode T) {
    return (T instanceof ConjAstNode)?(TermAstNode)((ConjAstNode)T).getArg(1):AtomAstNode.aTrue;
  }
}
