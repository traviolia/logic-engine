package com.gitlab.traviolia.semantics.rules.terms;



/**
  Always fails
*/
public class FailAstNode extends ConstBuiltinAstNode {
  FailAstNode() {super(FailAstNode.class, "fail");}

}