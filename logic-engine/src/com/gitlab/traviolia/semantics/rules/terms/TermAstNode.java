/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.terms;

import com.gitlab.traviolia.semantics.rules.runtime.TermType;

/**
  Top element of the Prolog term hierarchy.
  Describes a simple or compound term like:
  X,a,13,f(X,s(X)),[a,s(X),b,c], a:-b,c(X,X),d, etc.
*/
public abstract class TermAstNode extends Object {

protected final Class<?> myself;
  
   protected TermAstNode(Class<?> selfType) {
	    myself = selfType;
//	    myself = selfType.cast(this);
  }
  
   public TermType getType() {
	   return TermType.VAR;
   }

  /** 
    returns or fakes an arity for all subtypes 
  */
  public int getArity() {
	  return 0;
  };
  
	public abstract void inspect(AstNodeVisitor visitor);
	
	public abstract String getName();
  
  /**
     Prints out a term to a String with
     variables named in order V1, V2,....
  */
  public String pprint() {
    return this.toString();
  }
  
  /*
    Returns an unquoted version of toString()
  */
  public String toUnquoted() {
    return pprint();
  }
    
  static final TermAstNode stringToChars(String s) {
    if(0==s.length())
      return AtomAstNode.aNil;
    ConsAstNode l=new ConsAstNode(new IntAstNode((s.charAt(0))),AtomAstNode.aNil);
    ConsAstNode curr=l;
    for(int i=1;i<s.length();i++) {
      ConsAstNode tail=new ConsAstNode(new IntAstNode((s.charAt(i))),AtomAstNode.aNil);
      curr.args[1]=tail;
      curr=tail;
    }
    return l;
  }
  
  public TermAstNode toChars() {
    return stringToChars(toUnquoted());
  }
  
}
