package com.gitlab.traviolia.semantics.rules.terms;

import com.gitlab.traviolia.semantics.rules.runtime.StructureRuntime;


public class FunBuiltinWrapper extends StructureAstNode {

	public StructureRuntime runtime;
	
	public FunBuiltinWrapper(StructureRuntime runtime) {
		super(FunBuiltinWrapper.class, runtime.name(), new TermAstNode[runtime.args.length]);
		this.runtime = runtime;
	}
	
	@Override
	public int getArity() {
		return runtime.getArity();
	}
	
	@Override
	public Object getArg(int i) {
		return runtime.args[i];
	}
	
	@Override
	public void setArg(int i, TermAstNode T) {
	    runtime.args[i]=T;
	}

	@Override
	public Object[] getArgs() {
		return runtime.args;
	}
	
	@Override
	public void setArgs(TermAstNode[] args) {
		runtime.args = new Object[args.length];
		for (int i = 0; i < args.length; i += 1) {
			runtime.args[i] = args[i];
		}
	}

}
