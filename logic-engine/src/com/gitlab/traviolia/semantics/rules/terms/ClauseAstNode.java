/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.terms;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
* Datatype for a Prolog clause (H:-B) having a head H and a body b
*/
public class ClauseAstNode extends StructureAstNode {
	Logger logger = LogManager.getLogger();
/**
    Variable dictionary
  */
  public HashMap dict=null;
  
  /**
    Remembers if a clause is ground.
  */
  public boolean ground=false;
  
  /**
    File name and line where sources start and end (if applicable)
  */
  
  public String fname=null;
  
  public int begins_at=0;
  
  public int ends_at=0;
  
  /**
   *  Builds a clause given its head and its body
   */
  public ClauseAstNode(TermAstNode term, TermAstNode term2){
    super(ClauseAstNode.class, ":-",term,term2);
  }
  
	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.terms.Clause#setFile(java.lang.String, int, int)
 */
public void setFile(String fname,int begins_at,int ends_at) {
    this.fname=fname.intern();
    this.begins_at=begins_at;
    this.ends_at=ends_at;
  }
  
  /**
    Checks if a Clause has been proven ground after
    beeing read in or created.
  */
  final boolean provenGround() {
    return ground;
  }
  
  /**
  * Prints out a clause as Head:-Body
  */
  private String Clause2String(ClauseAstNode c) {
    TermAstNode h=c.getHead();
    TermAstNode t=c.getBody();
    if(t instanceof ConjAstNode)
      return h+":-"+((ConjAstNode)t).conjToString();
    return h+":-"+t;
  }
  
  // uncomment if you want this to be the default toString
  // procedure - it might create read-back problems, though
  // public String toString() {
  // return Clause2String(this);
  // }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.terms.Clause#pprint()
 */
  @Override
public String pprint() {
    return pprint(false);
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.terms.Clause#pprint(boolean)
 */
public String pprint(boolean replaceAnonymous) {
    String s=Clause2String(this);
    // if(fname!=null) s="%% "+fname+":"+begins_at+"-"+ends_at+"\n"+s;
    return s;
  }
  
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.terms.Clause#getHead()
 */
public final TermAstNode getHead() {
    return args[0];
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.terms.Clause#getBody()
 */
public final TermAstNode getBody() {
    return args[1];
  }
  
  /**
    Gets the leftmost (first) goal in the body of a clause,
    i.e. from H:-B1,B2,...,Bn it will extract B1.
  */
  final public TermAstNode getFirst() {
    TermAstNode body=getBody();
    if(body instanceof ConjAstNode)
      return (TermAstNode) ((TermAstNode) ((ConjAstNode)body).args[0]);
    else if(body instanceof TrueAstNode)
      return null;
    else
      return body;
  }
  
  /**
    Gets all but the leftmost goal in the body of a clause,
    i.e. from H:-B1,B2,...,Bn it will extract B2,...,Bn.
    Note that the returned Term is either Conj or True,
    the last one meaning an empty body.

    @see True
    @see ConjAstNode
  */
  final public TermAstNode getRest() {
    TermAstNode body=getBody();
    if(body instanceof ConjAstNode)
      return (TermAstNode) ((TermAstNode) ((ConjAstNode)body).args[1]);
    else
      return AtomAstNode.aTrue;
  }
  
  /**
    Concatenates 2 Conjunctions
    @see ClauseAstNode#unfold
  */
  static final TermAstNode appendConj(TermAstNode first,TermAstNode y) {
    y= (TermAstNode) y;
    if(first instanceof TrueAstNode)
      return y;
    if(y instanceof TrueAstNode)
      return first; // comment out if using getState
    if(first instanceof ConjAstNode) {
      TermAstNode curr= (TermAstNode) ((TermAstNode) ((ConjAstNode)first).args[0]);
      TermAstNode cont=appendConj((TermAstNode) ((ConjAstNode)first).args[1], y);
      // curr.getState(this,cont);
      return new ConjAstNode(curr,cont);
    } else
      return new ConjAstNode(first,y);
  }
  
}
