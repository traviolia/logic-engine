/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.terms;

/**
 * Abstract numeric class, part of the Term hierarchy
 * 
 * @see IntAstNode
 * @see RealAstNode
 * @see TermAstNode
 */
public abstract class NumberAstNode extends TermAstNode {

	protected NumberAstNode(Class<?> selfType) {
		super(selfType);
	}

	public String toString() {
		return Double.toString(getValue());
	}

	@Override
	public abstract void inspect(AstNodeVisitor visitor);
	
	abstract public double getValue();
}