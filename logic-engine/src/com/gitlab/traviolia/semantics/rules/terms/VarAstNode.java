/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.terms;

/**
 * Part of the Term hierarchy implmenting logical variables. They are subject to
 * reset by application of and undo action keep on the trail stack.
 */
public class VarAstNode extends TermAstNode {

	protected String name;

	protected VarAstNode(Class<?> selfType, String name) {
		super(selfType);
		this.name = name;
	}

	public VarAstNode() {
		this(VarAstNode.class, "_");
	}

	public VarAstNode(String name) {
		this(VarAstNode.class, name);
	}

	public String getName() {
		return name;
	}

	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

}