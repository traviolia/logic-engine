/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.terms;



/**
 * Symbolic constant, of arity 0.
 */

public class AtomAstNode extends TermAstNode {

	public final static AtomAstNode aNil = new NilAstNode();

	public final static AtomAstNode aTrue = new TrueAstNode();

	public final static AtomAstNode aFail = new FailAstNode();

	public final static AtomAstNode aYes = new AtomAstNode("yes");

	public final static AtomAstNode aNo = new AtomAstNode("no");

	public final static AtomAstNode anEof = new AtomAstNode("end_of_file");

	public final static TermAstNode the(TermAstNode X) {
		return (null == X) ? AtomAstNode.aNo : new StructureAstNode("the", X);
	}

	protected String sym;

	protected AtomAstNode(Class<?> selfType, String s) {
		super(selfType);
		sym = s.intern();
	}

	public AtomAstNode(String s) {
        this(AtomAstNode.class, s);
    }

	@Override
	public final String getName() {
		return sym;
	}

	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}
	
	/* (non-Javadoc)
	 * @see com.github.lycastus.semantics.rules.terms.Atom#qname()
	 */
	public String qname() {
		if (0 == sym.length())
			return "''";
		for (int i = 0; i < sym.length(); i++) {
			if (!Character.isLowerCase(sym.charAt(i)))
				return '\'' + sym + '\'';
		}
		return sym;
	}

	@Override
	public String toString() {
		return qname();
	}

	/**
	 * creates a ConstBuiltin from a Const known to be registered as being a
	 * builtin while returning its argument unchanged if it is just a plain
	 * Prolog constant with no builtin code attached to it
	 */

	@Override
	public String toUnquoted() {
		return getName();
	}
}
