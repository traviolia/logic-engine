package com.gitlab.traviolia.semantics.rules.terms;



/**
  Always succeeds
*/
public class TrueAstNode extends ConstBuiltinAstNode {
  TrueAstNode(){
    super(TrueAstNode.class, "true");
  }
  
}
