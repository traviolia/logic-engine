package com.gitlab.traviolia.semantics.rules.terms;

import com.gitlab.traviolia.semantics.rules.runtime.Term;


public interface AstNodeVisitor {
	
	public void visit(AtomAstNode node);
	
	public void visit(ClauseAstNode node);

	public void visit(ConjAstNode node);
	
	public void visit(ConsAstNode node);

	public void visit(IntAstNode node);
	
	public void visit(RealAstNode node);

	public void visit(StructureAstNode node);

	public void visit(VarAstNode node);
	
	public void visit(Term node); // do not use

}
