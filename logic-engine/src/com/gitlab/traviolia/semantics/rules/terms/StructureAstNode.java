package com.gitlab.traviolia.semantics.rules.terms;

/**
  Implements compound terms
  @see TermAstNode
*/
public class StructureAstNode extends TermAstNode {
	
  protected String symbol;
  protected TermAstNode args[];
  
  public int getArity() {
    return args.length;
  }
  
  protected StructureAstNode(Class<?> selfType, String s, TermAstNode ...args){
	    super(StructureAstNode.class);
	    this.symbol = s;
	    this.args = args;
	  }
	  
	public StructureAstNode(String s) {
		this(StructureAstNode.class, s, new TermAstNode[0]);
	}

	public StructureAstNode(String name, TermAstNode[] args) {
		this(StructureAstNode.class, name, args);
	}

	public StructureAstNode(String s, int arity) {
		this(StructureAstNode.class, s, new TermAstNode[arity]);
	}

	public StructureAstNode(String s, TermAstNode x0) {
		this(s, 1);
		args[0] = x0;
	}

	public StructureAstNode(String s, TermAstNode x0, TermAstNode x1) {
		this(s, 2);
		args[0] = x0;
		args[1] = x1;
	}

	public StructureAstNode(String s, TermAstNode x0, TermAstNode x1, TermAstNode x2) {
		this(s, 3);
		args[0] = x0;
		args[1] = x1;
		args[2] = x2;
	}

	public StructureAstNode(String s, TermAstNode x0, TermAstNode x1,
			TermAstNode x2, TermAstNode x3) {
		this(s, 4);
		args[0] = x0;
		args[1] = x1;
		args[2] = x2;
		args[3] = x3;
	}

	public final String getSymbol() {
		return symbol;
	}

	/* (non-Javadoc)
	 * @see com.github.lycastus.semantics.rules.terms.Structure#inspect(com.github.lycastus.semantics.rules.terms.AstNodeVisitor)
	 */
	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

	/* (non-Javadoc)
	 * @see com.github.lycastus.semantics.rules.terms.Structure#qname()
	 */
	protected String qname() {
		if (0 == symbol.length())
			return "''";
		for (int i = 0; i < symbol.length(); i++) {
			if (!Character.isLowerCase(symbol.charAt(i)))
				return '\'' + symbol + '\'';
		}
		return symbol;
	}

  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.terms.Structure#getArg(int)
 */
public Object getArg(int i) {
    return args[i];
  }
    
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.terms.Structure#setArg(int, com.github.lycastus.semantics.rules.terms.TermAstNode)
 */
public void setArg(int i, TermAstNode T) {
    args[i]=T;
  }
  
  protected final String funToString() {
    if(args==null)
      return qname()+"()";
    int l=args.length;
    return qname()+(l<=0?"":"("+show_args()+")");
  }
  
  public String toString() {
    return funToString();
  }
  
  protected static String watchNull(Object x) {
    return((null==x)?"null":x.toString());
  }
  
  private String show_args() {
    StringBuffer s=new StringBuffer(watchNull(args[0]));
    for(int i=1;i<args.length;i++) {
      s.append(","+watchNull(args[i]));
    }
    return s.toString();
  }
  
	public Object[] getArgs() {
		return args;
	}
	
	public void setArgs(TermAstNode[] args) {
		this.args = args;
	}

	@Override
	public String getName() {
		return symbol;
	}

}
