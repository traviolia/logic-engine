package com.gitlab.traviolia.semantics.rules.terms;

import com.gitlab.traviolia.semantics.rules.runtime.AtomRuntime;

public class ConstBuiltinWrapper extends AtomAstNode {

	public AtomRuntime builtin;

	public ConstBuiltinWrapper(AtomRuntime builtin) {
		super(ConstBuiltinWrapper.class, builtin.name());
		this.builtin = builtin;
		this.sym = builtin.name();
	}

}
