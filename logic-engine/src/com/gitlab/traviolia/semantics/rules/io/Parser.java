/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.io;

import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.runtime.AtomRuntime;
import com.gitlab.traviolia.semantics.rules.runtime.RuleEngine;
import com.gitlab.traviolia.semantics.rules.runtime.StructureRuntime;
import com.gitlab.traviolia.semantics.rules.terms.AtomAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ClauseAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ConjAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ConsAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ConstBuiltinWrapper;
import com.gitlab.traviolia.semantics.rules.terms.FunBuiltinWrapper;
import com.gitlab.traviolia.semantics.rules.terms.IntAstNode;
import com.gitlab.traviolia.semantics.rules.terms.RealAstNode;
import com.gitlab.traviolia.semantics.rules.terms.StructureAstNode;
import com.gitlab.traviolia.semantics.rules.terms.TermAstNode;
import com.gitlab.traviolia.semantics.rules.terms.VarAstNode;

enum TokenType {
	VARIABLE      (false, "$VAR$"),
	INTEGER       (false, "$INT$"),
	REAL          (false, "$REAL$"),
	CONST         (false, "$CONST$"),
	STRING        (false, "$STRING$"),
	FUN           (false, "$PRED$"),
	END_OF_CLAUSE (true, "$EOC$"),
	END_OF_FILE   (true, "$EOF$"),
	IF_LIGATURE   (true, ":-"),
	LEFT_PAREN    (true, "("),
	RIGHT_PAREN   (true, ")"),
	LEFT_BRACKET  (true, "["),
	RIGHT_BRACKET (true, "["),
	VERTICAL_BAR  (true, "|"),
	COMMA         (true, ",");

	private final boolean selfDescribing;
    private final String name;
    
    TokenType(Boolean selfDescribing, String name) {
        this.selfDescribing = selfDescribing;
        this.name = name;
    }
    
    final public boolean isSelfDescribing() {
    	return selfDescribing;
    }
    
    final public String getName() {
    	return name;
    }
}

final class Token {
	TokenType type;
	TermAstNode term;

	Token(TokenType type) {
		if (!type.isSelfDescribing())
			throw new IllegalArgumentException("only self describing tokens can be created without a term");
		this.type = type;
	}

	Token(TokenType type, TermAstNode term) {
		if (type.isSelfDescribing())
			throw new IllegalArgumentException("self describing tokens cannot be created with a term");
		this.type = type;
		this.term = term;
	}
	
	protected TokenType getType() {
		return type;
	}
	
	protected TermAstNode getTerm() {
		if (type.isSelfDescribing())
			throw new IllegalStateException("illegal attempt to get a self describing token's term");
		return  term;
	}
}

//class lparToken extends Token {
//	public lparToken() {
//		super("(");
//	}
//}
//
//class rparToken extends Token {
//	public rparToken() {
//		super(")");
//	}
//}
//
//class lbraToken extends Token {
//	public lbraToken() {
//		super("[");
//	}
//}
//
//class rbraToken extends Token {
//	public rbraToken() {
//		super("]");
//	}
//}
//
//class barToken extends Token {
//	public barToken() {
//		super("|");
//	}
//}
//
//class commaToken extends Token {
//	public commaToken() {
//		super(",");
//	}
//}

//class varToken extends AbstractStructure {
//	public varToken(AbstractVar X, AbstractAtom C, AbstractInt I) {
//		super(varToken.class, "varToken", new AbstractTerm[3]);
//		args[0] = X;
//		args[1] = C;
//		args[2] = I;
//	}
//}

//class intToken extends AbstractStructure {
//	public intToken(int i) {
//		super(intToken.class, "intToken", new Int(i));
//	}
//}

//class realToken extends AbstractStructure {
//	public realToken(double i) {
//		super(realToken.class, "realToken", new Real(i));
//	}
//}

//class constToken extends AbstractStructure {
//	public constToken(AbstractAtom c) {
//		super(constToken.class, "constToken", c);
//		args[0] = Builtins.toConstBuiltin(c);
//	}
//
//	public constToken(String s) {
//		this(new Atom(s));
//	}
//}

//class stringToken extends AbstractStructure {
//	public stringToken(constToken c) {
//		super(stringToken.class, "stringToken", (c.args[0]));
//	}
//}

//class funToken extends AbstractStructure {
//	public funToken(String s) {
//		super(funToken.class, "funToken", new Structure(s));
//	}
//}

//class eocToken extends AbstractStructure {
//	public eocToken() {
//		super(eocToken.class, "eocToken", new Atom("end_of_clause"));
//	}
//}

//class eofToken extends AbstractStructure {
//	public eofToken() {
//		super(eofToken.class, "eofToken", Atom.anEof);
//	}
//}

//class iffToken extends AbstractStructure {
//	public iffToken(String s) {
//		super(iffToken.class, "iffToken", new Atom(s));
//	}
//}

class ParserException extends IOException {
	private static final long serialVersionUID = 1L;
	public ParserException(String e, String f, Token token) {
		super("expected: " + e + ", found: " + f + "'" + token + "'");
	}
}

/**
 * Lexicographic analyzer reading from a stream
 */
class Lexer {
	private Reader input;
	private final StreamTokenizer tokenizer;
	private final static String anonymous = "_".intern();
	private boolean inClause = false;
	private final RuleEngine engine;


	protected Lexer(Reader I, RuleEngine engine) throws IOException {
		this.input = I;
		this.engine = engine;
		this.tokenizer = new StreamTokenizer(I);
		tokenizer.parseNumbers();
		tokenizer.eolIsSignificant(true);
		tokenizer.ordinaryChar('.');
		tokenizer.ordinaryChar('-'); // creates problems with -1 etc.
		tokenizer.ordinaryChar('/');
		tokenizer.quoteChar('\'');
		tokenizer.quoteChar('\"');
		tokenizer.slashStarComments(true);
		tokenizer.commentChar('%');
		wordChar('$');
		wordChar('_');
		dict = new HashMap();
	}

	/**
	 * String based constructor. Used in queries ended by \n + prolog2java.
	 */

	protected Lexer(String s, RuleEngine engine) throws Exception {
		this(IO.string_to_stream(s), engine);
	}

	protected Lexer(RuleEngine engine) throws IOException {
		this(IO.input, engine);
	}

	protected int lineno() {
		return tokenizer.lineno();
	}
	
	protected boolean atEOF() {
		boolean yes = (StreamTokenizer.TT_EOF == tokenizer.ttype);
		if (yes)
			try {
				input.close();
			} catch (IOException e) {
				IO.trace("unable to close atEOF");
			}
		return yes;
	}

	protected boolean atEOC() {
		return !inClause;
	}

	protected AtomAstNode toConstBuiltin(AtomAstNode c) {
		if (c.getName().equals(AtomAstNode.aNil.getName()))
			return AtomAstNode.aNil;
		if (c.getName().equals(AtomAstNode.aNo.getName()))
			return AtomAstNode.aNo;
		if (c.getName().equals(AtomAstNode.aYes.getName()))
			return AtomAstNode.aYes;

		AtomRuntime B = engine.newBuiltin(c);
		if (null == B) {
			// IO.mes("not a builtin:"+this);
			return c;
		}
		return new ConstBuiltinWrapper(B);
	}

	protected final Token make_const(String s) {
		return new Token(TokenType.CONST, toConstBuiltin(new AtomAstNode(s)));
	}

	private final Token make_fun(String s) {
		return new Token(TokenType.FUN, new StructureAstNode(s));
	}

	private final Token make_int(double n) {
		return new Token(TokenType.INTEGER, new IntAstNode((int) n));
	}

	private final Token make_real(double n) {
		return new Token(TokenType.REAL, new RealAstNode(n));
	}

	private final Token make_number(double nval) {
		Token token;
		if (Math.floor(nval) == nval)
			token = make_int(nval);
		else
			token = make_real(nval);
		return token;
	}

	private final Token make_var(String s) {
		s = s.intern();
		VarAstNode X;
		long occ;
		if (s == anonymous) {
			occ = 0;
			X = new VarAstNode();
			s = X.toString();
		} else {
			X = (VarAstNode) dict.get(s);
			if (X == null) {
				occ = 1;
				X = new VarAstNode(s);
			} else {
				occ = ((IntAstNode) dict.get(X)).longValue();
				occ++;
			}
		}
		IntAstNode I = new IntAstNode(occ);
		dict.put(X, I);
		dict.put(s, X);
//		return new varToken(X, new Atom(s), I);
		return new Token(TokenType.VARIABLE, X);
	}

	private final void wordChar(char c) {
		tokenizer.wordChars(c, c);
	}

	HashMap dict;

	private Token getWord(boolean quoted) throws IOException {
		Token T;
		if (quoted && 0 == tokenizer.sval.length())
			T = make_const("");
		/*
		 * DO NOT DO THIS: quoting is meant to prevent it!!! else
		 * if("()[]|".indexOf(sval.charAt(0))>=0) { switch(sval.charAt(0)) {
		 * case '(': T=new lparToken(); break; case ')': T=new rparToken();
		 * break; case '[': T=new lbraToken(); break; case ']': T=new
		 * rbraToken(); break; case '|': T=new barToken(); break; } }
		 */
		else {
			char c = tokenizer.sval.charAt(0);
			if (!quoted && (Character.isUpperCase(c) || '_' == c))
				T = make_var(tokenizer.sval);
			else { // nonvar
				String s = tokenizer.sval;
				int nt = tokenizer.nextToken();
				tokenizer.pushBack();
				T = ('(' == nt) ? make_fun(s) : make_const(s);
			}
		}
		return T;
	}

	protected Token next() throws IOException {
		inClause = true;
		Token token;
		int n;
		do {
	    	n = tokenizer.nextToken();
	    } while (n == StreamTokenizer.TT_EOL);
		switch (n) {
		case StreamTokenizer.TT_WORD:
			token = getWord(false);
			break;

		case '\'':
			token = getWord(true);
			break;

		case StreamTokenizer.TT_NUMBER:
			token = make_number(tokenizer.nval);
			break;

		case StreamTokenizer.TT_EOF:
			token = new Token(TokenType.END_OF_FILE);
			inClause = false;
			break;

		case StreamTokenizer.TT_EOL:
			throw new ParserException("unexpected end of line", "",  make_const(Character.toString((char) n)));

		case '-':
			if (StreamTokenizer.TT_NUMBER == tokenizer.nextToken()) {
				token = make_number(-tokenizer.nval);
			} else {
				tokenizer.pushBack();
				token = make_const(Character.toString((char) n));
			}

			break;

		case ':':
			if ('-' == tokenizer.nextToken()) {
				token = new Token(TokenType.IF_LIGATURE);
			} else {
				tokenizer.pushBack();
				token = make_const(Character.toString((char) n));
			}
			break;

		case '.':
			int c = tokenizer.nextToken();
			if (StreamTokenizer.TT_EOL == c || StreamTokenizer.TT_EOF == c) {
				inClause = false;
				// dict.clear(); ///!!!: this looses Var names
				token = new Token(TokenType.END_OF_CLAUSE);
			} else {
				tokenizer.pushBack();
				token = make_const(Character.toString((char) n)); // !!!: sval is gone
			}
			break;

		case '\"':
			token = new Token(TokenType.STRING, getWord(true).getTerm());
			break;

		case '(':
			token = new Token(TokenType.LEFT_PAREN);
			break;
		case ')':
			token = new Token(TokenType.RIGHT_PAREN);
			break;
		case '[':
			token = new Token(TokenType.LEFT_BRACKET);
			break;
		case ']':
			token = new Token(TokenType.RIGHT_BRACKET);
			break;
		case '|':
			token = new Token(TokenType.VERTICAL_BAR);
			break;
		case ',':
			token = new Token(TokenType.COMMA);
			break;
		default:
			token = make_const(Character.toString((char) n));
		}
		// IO.mes("TOKEN:"+T);
		return token;
	}
}

/**
 * Simplified Prolog parser: Synatax supported: a0:-a1,...,an. - no operators (
 * except toplevel :- and ,) - use quotes to create special symbol names, i.e.
 * compute('+',1,2, Result) and write(':-'(a,','(b,c)))
 */

public final class Parser {
	Logger logger = LogManager.getLogger();

	private final Lexer lexer;
	private final RuleEngine engine;
	
	public Parser(Reader I, RuleEngine engine) throws IOException {
		this.lexer = new Lexer(I, engine);
		this.engine = engine;
	}

	public Parser(String s, RuleEngine engine) throws Exception {
		this.lexer = new Lexer(s, engine);
		this.engine = engine;
	}

	static public final boolean isError(ClauseAstNode C) {
		TermAstNode H = C.getHead();
		if (H instanceof StructureAstNode
				&& "error".equals(((StructureAstNode) H).getSymbol())
				&& H.getArity() == 3
				&& !(((TermAstNode) ((StructureAstNode) H).getArg(0)) instanceof VarAstNode))
			return true;
		return false;
	}

	static public final void showError(ClauseAstNode c) {
		IO.errmes("*** " + c);
	}

	public boolean atEOF() {
		return lexer.atEOF();
	}

	public int lineno() {
		return lexer.lineno();
	}

	/**
	 * Main Parser interface: reads a clause together with variable name
	 * information
	 */
	public ClauseAstNode readClause() {
		ClauseAstNode t = null;
		boolean verbose = false;
		try {
			t = readClauseOrEOF();
			// IO.mes("GOT Clause:"+t);
		}
		/**
		 * catch built exception clauses which are defined in lib.pro - allowing
		 * to recover or be quiet about such errors.
		 */
		catch (ParserException e) {
			t = errorClause(e, "syntax_error", lexer.lineno(), verbose);
			try {
				while (!lexer.atEOC() && !lexer.atEOF())
					lexer.next();
			} catch (IOException toIgnore) {
			}
		} catch (IOException e) {
			t = errorClause(e, "io_exception", lexer.lineno(), verbose);
		} catch (Exception e) {
			t = errorClause(e, "unexpected_syntax_exception", lexer.lineno(), true);
		}
		return t;
	}

	private ClauseAstNode errorClause(Exception e, String type, int line, boolean verbose) {

		String mes = e.getMessage();
		if (null == mes)
			mes = "unknown_error";
		StructureAstNode f = new StructureAstNode("error", new AtomAstNode(type), new AtomAstNode(mes),
				new StructureAstNode("line", new IntAstNode(line)));
		ClauseAstNode C = new ClauseAstNode(f, AtomAstNode.aTrue);
		if (verbose) {
			IO.errmes(type + " error at line:" + line);
			IO.errmes(C.pprint(), e);
		}
		return C;
	}

	private ClauseAstNode readClauseOrEOF() throws IOException {

		lexer.dict = new HashMap();

		Token token = lexer.next();

		// IO.mes("readClauseOrEOF 0:"+n);

		if (token.getType() == TokenType.END_OF_FILE)
			return null; // $$toClause(n.token(),dict);

		if (token.getType() == TokenType.IF_LIGATURE) {
			token = lexer.next();
			TermAstNode t = getTerm(token);
			TermAstNode bs = getConjCont(t);
			ClauseAstNode C = new ClauseAstNode(new AtomAstNode("init"), bs);
			C.dict = lexer.dict;
			return C;
		}

		TermAstNode h = getTerm(token);

		// IO.mes("readClauseOrEOF 1:"+h);

		token = lexer.next();

		// IO.mes("readClauseOrEOF 2:"+n);

		if (token.getType() == TokenType.END_OF_CLAUSE || token.getType() == TokenType.END_OF_FILE)
			return toClause(h, lexer.dict);

		// IO.mes("readClauseOrEOF 3:"+b);

		ClauseAstNode C = null;
		if (token.getType() == TokenType.IF_LIGATURE) {
			TermAstNode t = getTerm();
			TermAstNode bs = getConjCont(t);
			C = new ClauseAstNode(h, bs);
			C.dict = lexer.dict;
		} else if (token.getType() == TokenType.COMMA) {
			TermAstNode b = getTerm();
			TermAstNode bs = getConjCont(b);
			C = toClause(new ConjAstNode(h, bs), lexer.dict);
		} else {
			throw new ParserException("':-' or '.' or ','", "bad body element", token);
		}
		return C;
	}

	private ClauseAstNode toClause(TermAstNode T, HashMap dict) {
		ClauseAstNode C = new ClauseAstNode(T,AtomAstNode.aTrue); // adds ...:-true if missing
		C.dict = dict;
		return C;
	}

	private TermAstNode getConjCont(TermAstNode curr)
			throws IOException {

		Token token = lexer.next();
		TermAstNode t = null;
		if (token.getType() == TokenType.END_OF_CLAUSE)
			t = curr;
		else if (token.getType() == TokenType.COMMA) {
			TermAstNode other = getTerm();
			t = new ConjAstNode(curr, getConjCont(other));
		}
		if (null == t) {
			throw new ParserException("'.'", "bad body element", token);
		}
		return t;
	}

	  private TermAstNode toFunBuiltin(StructureAstNode f) {
		    if(f.getSymbol().equals(":-")&&f.getArity()==2) {
		      return new ClauseAstNode((TermAstNode) f.getArg(0), (TermAstNode)f.getArg(1));
		    }
		    if(f.getSymbol().equals(",")&&f.getArity()==2) {
		      return new ConjAstNode((TermAstNode) f.getArg(0), (TermAstNode) f.getArg(1));
		    }
		    StructureRuntime B = engine.newBuiltin(f);
		    if(null==B)
		      return f;
		    Object args[] = new Object[f.getArity()];
		    B = B.funClone();
		    B.setArgs(args);
		    for (int i = 0; i < B.args.length; i += 1) {
		    	B.setArg(i, f.getArg(i));
		    }
		    return new FunBuiltinWrapper(B);
		  }
		  
	private TermAstNode getTerm(Token token) throws IOException {
		TermAstNode term;
		if (token.getType() == TokenType.VARIABLE || token.getType() == TokenType.INTEGER
				|| token.getType() == TokenType.REAL || token.getType() == TokenType.CONST) {
			term = token.getTerm();
		} else if (token.getType() == TokenType.STRING) {
			term = ((TermAstNode) token.getTerm()).toChars();
			// IO.mes("getTerm:stringToken-->"+t);

		} else if (token.getType() == TokenType.LEFT_BRACKET) {
			term = getList();
		} else if (token.getType() == TokenType.FUN) {
			StructureAstNode f = (StructureAstNode) token.getTerm();
			f.setArgs(getArgs());
			term = toFunBuiltin(f);
		} else
			throw new ParserException("var,int,real,constant,'[' or functor", "bad term", token);
		return term;
	}

	private TermAstNode getTerm() throws IOException {
		Token token = lexer.next();
		return getTerm(token);
	}

	private TermAstNode[] getArgs() throws IOException {
		Token token = lexer.next();
		if (token.getType() != TokenType.LEFT_PAREN)
			throw new ParserException("'('", "in getArgs", token);
		ArrayList<TermAstNode> v = new ArrayList<>();
		TermAstNode t = getTerm();
		v.add(t);
		for (;;) {
			token = lexer.next();
			if (token.getType() == TokenType.RIGHT_PAREN) {
				int l = v.size();
				TermAstNode args[] = new TermAstNode[l];
				// v.copyInto(args);
				TermAstNode[] as = v.toArray(new TermAstNode[0]);
				for (int i = 0; i < l; i++) {
					args[i] = (TermAstNode) as[i];
				}
				return args;
			} else if (token.getType() == TokenType.COMMA) {
				t = getTerm();
				v.add(t);
			} else {
				throw new ParserException("',' or ')'", "bad arg", token);
			}
		}
	}

	private TermAstNode getList() throws IOException {
		Token token = lexer.next();
		if (token.getType() == TokenType.RIGHT_BRACKET)
			return AtomAstNode.aNil;
		TermAstNode t = getTerm(token);
		return getListCont(t);
	}

	private TermAstNode getListCont(TermAstNode curr) throws IOException {
		// IO.trace("curr: "+curr);
		Token token = lexer.next();
		TermAstNode t = null;
		if (token.getType() == TokenType.RIGHT_BRACKET)
			t = new ConsAstNode(curr, AtomAstNode.aNil);
		else if (token.getType() == TokenType.VERTICAL_BAR) {
			t = new ConsAstNode(curr, getTerm());
			token = lexer.next();
			if (token.getType() != TokenType.RIGHT_BRACKET) {
				throw new ParserException("']'", "bad list end after '|'", token);
			}
		} else if (token.getType() == TokenType.COMMA) {
			TermAstNode other = getTerm();
			t = new ConsAstNode(curr, getListCont(other));
		}
		if (t == null)
			throw new ParserException("| or ]", "bad list continuation", token);
		return t;
	}

	private static final String patchEOFString(String s) {
		if (!(s.lastIndexOf(".") >= s.length() - 2))
			s = s + ".";
		return s;
	}

	public static final ClauseAstNode clsFromString(String s, RuleEngine engine) {
		if (null == s)
			return null;
		s = patchEOFString(s);
		ClauseAstNode t = null;
		try {
			Parser p;
			p = new Parser(s, engine);
			t = p.readClause();
		} catch (Exception e) { // nothing expected to catch
			IO.errmes("unexpected parsing error", e);
		}
		if (t.dict == null)
			t.ground = false;
		else
			t.ground = t.dict.isEmpty();
		return t;
	}

}


