package com.gitlab.traviolia.semantics.rules.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.io.IO;
import com.gitlab.traviolia.semantics.rules.io.Parser;
import com.gitlab.traviolia.semantics.rules.terms.TermAstNode;

//!depends
/**
 * Datatype for a Prolog clause (H:-B) having a head H and a body b
 */
public class ClauseRuntime extends StructureRuntime implements Clause {
	private Logger logger = LogManager.getLogger();

	/**
	 * Builds a clause given its head and its body
	 */
	public ClauseRuntime(Term head, Term body) {
		super(":-", head, body);
	}

	@Override
	public ClauseRuntime clone() {
		return (ClauseRuntime) super.clone();
	}

	@Override
	public void inspect(TermVisitor visitor) {
		visitor.visit(this);
	}

	/**
	 * Reads a goal as a clause containing a dummy header with all variables in
	 * it
	 */

	public ClauseRuntime toGoal() {
		ClauseRuntime G = new ClauseRuntime(varsOf(), getHead());
		G.dict = dict;
		G.checkIfGround();
		IO.trace("conversion from clause to goal ignores body of: " + pprint());
		return G;
	}

		public static ClauseRuntime goalFromString(String line, RuleEngine engine) {
			IO.trace("read string: <" + line + ">");

			if (null == line)
				line = AtomRuntime.anEof.name();
			else if (0 == line.length())
				return null;

			ClauseRuntime C = clauseFromString(line, engine);
			if (null == C) {
				IO.errmes("warning (null Clause):" + line);
				return null;
			}

			// IO.trace("got goal:\n"+C.toGoal()); //OK
			return C.toGoal();
		}

		public static ClauseRuntime goalFromString(String line, LogicEngineImpl engine) {
			IO.trace("read string: <" + line + ">");

			if (null == line)
				line = AtomRuntime.anEof.name();
			else if (0 == line.length())
				return null;

			ClauseRuntime C = clauseFromString(line, engine);
			if (null == C) {
				IO.errmes("warning (null Clause):" + line);
				return null;
			}

			// IO.trace("got goal:\n"+C.toGoal()); //OK
			return C.toGoal();
		}

		public static ClauseRuntime clauseFromString(String s, RuleEngine engine) {
			ClauseCompiler compiler = new ClauseCompiler();
			TermAstNode node = Parser.clsFromString(s, engine);
			node.inspect(compiler);
			return (ClauseRuntime) compiler.getReturnValue();
		}

		public static ClauseRuntime clauseFromString(String s, LogicEngineImpl engine) {
			ClauseCompiler compiler = new ClauseCompiler();
			TermAstNode node = Parser.clsFromString(s, engine);
			node.inspect(compiler);
			return (ClauseRuntime) compiler.getReturnValue();
		}

	/**
	 * Detects that a clause is ground (i.e. has no variables)
	 */
	final void checkIfGround() {
		ground = varsOf().getArity() == 0;
	}

	/**
	 * Variable dictionary
	 */
	public HashMap dict = null;

	/**
	 * Remembers if a clause is ground.
	 */
	public boolean ground = false;

	/**
	 * File name and line where sources start and end (if applicable)
	 */

	public String fname = null;

	public int begins_at = 0;

	public int ends_at = 0;

	public void setFile(String fname, int begins_at, int ends_at) {
		this.fname = fname.intern();
		this.begins_at = begins_at;
		this.ends_at = ends_at;
	}

	/**
	 * Checks if a Clause has been proven ground after beeing read in or
	 * created.
	 */
	final boolean provenGround() {
		return ground;
	}

	/**
	 * Prints out a clause as Head:-Body
	 */
	private String Clause2String(ClauseRuntime c) {
		Term h = c.getHead();
		Term t = c.getBody();
		if (t instanceof Conjunction)
			return h + ":-" + ((Conjunction) t).conjToString();
		return h + ":-" + t;
	}

	// uncomment if you want this to be the default toString
	// procedure - it might create read-back problems, though
	// public String toString() {
	// return Clause2String(this);
	// }

	/**
	 * Pretty prints a clause after replacing ugly variable names
	 */
	  @Override
	public String pprint() {
		return pprint(false);
	}

	/**
	 * Pretty prints a clause after replacing ugly variable names
	 */
	  @Override
	public String pprint(boolean replaceAnonymous) {
		String s = Clause2String(this.cnumbervars(replaceAnonymous));
		// if(fname!=null) s="%% "+fname+":"+begins_at+"-"+ends_at+"\n"+s;
		return s;
	}

	/**
	 * Clause to Term converter: the joy of strong typing:-)
	 */
	  @Override
	public ClauseRuntime toClause() { // overrides toClause in Term
		return this;
	}

	/**
	 * Replaces varibles with nice looking upper case constants for printing
	 * purposes
	 */
	// synchronized
	public ClauseRuntime cnumbervars(boolean replaceAnonymous) {
		if (dict == null)
			return (ClauseRuntime) numbervars();
		if (provenGround())
			return this;
		Trail trail = new Trail();
		Iterator e = dict.keySet().iterator();

		while (e.hasNext()) {
			Object X = e.next();
			if (X instanceof String) {
				VariableRuntime V = (VariableRuntime) dict.get(X);
				long occNb = ((Integer) dict.get(V)).longValue();
				String s = (occNb < 2 && replaceAnonymous) ? "_" : (String) X;
				// bug: occNb not accurate when adding artif. '[]' head
				V.unify(new PseudoVar(s), trail);
			}
		}
		ClauseRuntime NewC = (ClauseRuntime) numbervars();
		trail.unwind(0);
		return NewC;
	}

	/**
	 * Converts a clause to a term. Note that Head:-true will convert to the
	 * term Head.
	 */
	  @Override
	public final Term toTerm() {
		if (getBody() instanceof True)
			return getHead();
		return this;
	}

	/**
	 * Creates a copy of the clause with variables standardized apart, i.e.
	 * something like f(s(X),Y,X) becomes f(s(X1),Y1,X1)) with X1,Y1 variables
	 * garantted not to occurring in the the current resolvant.
	 */
	  @Override
	final public Clause ccopy() {
		if (ground)
			return this;
		ClauseRuntime C = (ClauseRuntime) copy();
		C.dict = null;
		C.ground = ground;
		return C;
	}

	/**
	 * Extracts the head of a clause (a Term).
	 */
	  @Override
	public final Term getHead() {
		Term t = (Term) args[0];
		return (Term) t.ref();
	}

	/**
	 * Extracts the body of a clause
	 */
	  @Override
	public final Term getBody() {
		return (Term) ((Term) args[1]).ref();
	}

	/**
	 * Gets the leftmost (first) goal in the body of a clause, i.e. from
	 * H:-B1,B2,...,Bn it will extract B1.
	 */
	  @Override
	final public Term getFirst() {
		Term body = getBody();
		if (body instanceof Conjunction)
			return (Term) ((Term) ((Conjunction) body).args[0]).ref();
		else if (body instanceof True)
			return null;
		else
			return body;
	}

	/**
	 * Gets all but the leftmost goal in the body of a clause, i.e. from
	 * H:-B1,B2,...,Bn it will extract B2,...,Bn. Note that the returned Term is
	 * either Conj or True, the last one meaning an empty body.
	 * 
	 * @see True
	 * @see Conjunction
	 */
	  @Override
	final public Term getRest() {
		Term body = getBody();
		if (body instanceof Conjunction)
			return (Term) ((Term) ((Conjunction) body).args[1]).ref();
		else
			return AtomRuntime.aTrue;
	}

	/**
	 * Concatenates 2 Conjunctions
	 * 
	 * @see ClauseRuntime#unfold
	 */
	static final Term appendConj(Term x, Term y) {
		y = (Term) y.ref();
		if (x instanceof True)
			return y;
		if (y instanceof True)
			return x; // comment out if using getState
		if (x instanceof Conjunction) {
			Term curr = (Term) ((Term) ((Conjunction) x).args[0]).ref();
			Term cont = appendConj((Term) ((Conjunction) x).args[1], y);
			// curr.getState(this,cont);
			return new Conjunction(curr, cont);
		} else
			return new Conjunction(x, y);
	}

//	/**
//	 * Algebraic composition operation of 2 Clauses, doing the basic resolution
//	 * step Prolog is based on. From A0:-A1,A2...An and B0:-B1...Bm it builds
//	 * (A0:-B1,..Bm,A2,...An) mgu(A1,B0). Note that it returns null if A1 and B0
//	 * do not unify.
//	 * 
//	 * @see AbstractTerm#unify()
//	 */
//	  @Override
//	public final Clause unfold(Clause that, Trail trail) {
//		Clause result = null;
//		Term first = getFirst();
//
//		logger.trace("attempting to unify '{}' and '{}'", that, first);
//		if (first != null && that.getHead().matches(first, trail)) {
//
//			that = that.ccopy();
//
//			that.getHead().unify(first, trail);
//
//			Term cont = appendConj(that.getBody(), getRest());
//			result = new ClauseRuntime(getHead(), cont);
//		}
//		return result;
//	}
//
//	// synchronized
//	  @Override
//	public final Clause unfold_with_goal(Clause goal, Trail trail) {
//		return goal.unfold(this, trail);
//	}

	/*
	 * // synchronized final Clause unfoldedCopy(Clause that,Trail trail) { int
	 * oldtop=trail.size(); Clause result=unfold(that,trail); if(result==null)
	 * return null; result=result.ccopy(); trail.unwind(oldtop); return result;
	 * }
	 */

	/**
	 * Returns a key based on the principal functor of the head of the clause
	 * and its arity.
	 */
	  @Override
	final public String getKey() {
		return getHead().getKey();
	}

	  @Override
	final boolean isClause() {
		return true;
	}

	@Override
	public Term[] toArray() {
		ArrayList<Term> list = new ArrayList<Term>();
		Term term = getHead();
		list.add(term);
		term = getBody();
	    while (term instanceof Conjunction) {
	        list.add((Term) ((Term) ((Conjunction)term).args[0]).ref());
	        term = (Term) ((Conjunction) term).args[1];
	    }
	    list.add(term);
		return list.toArray(new Term[0]);
	}

}
