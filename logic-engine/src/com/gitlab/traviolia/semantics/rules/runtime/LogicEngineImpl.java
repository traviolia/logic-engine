/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.runtime;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.builtins.Builtins;
import com.gitlab.traviolia.semantics.rules.fluents.DataBase;
import com.gitlab.traviolia.semantics.rules.terms.AtomAstNode;
import com.gitlab.traviolia.semantics.rules.terms.StructureAstNode;

/**
 * <p>This is a "from scratch" implementation of an abstract prolog machine;
 * starting with a simple textbook interpreter. For an example interpreter,
 * see Chapters 1 and 2 of "The Implementation of Prolog" by Patrice Boizumault.
 * </p>
 * <p>The interpreter consists of three primary functions - resolve,
 * prove, and unify - which are defined as follows:
 * </p>
 * <pre><code>
 * resolve(resolvant:List<term>, sub:List<Var,Term>) -> (bool, list<Var,Term>)
 *     sub = []
 *     while (resolvant != null) 
 *         (result, sub) =
 *             prove(resolvant.first(), rules(resolvant.first()), sub)
 *             if (result) then
 *                 resolvant = resolvant.rest();
 *             else
 *                 return (false, sub)
 *     return (true, sub)
 *         
 * prove(goal : term, clauses : list<clause>, resolvant : list<term>,
 *       sub :List<Var,Term>) -> (bool, list<Var,Term>)
 *     if (goal == null) then
 *         return (false, sub)
 *     else
 *         (result, sub') = unify(goal, clauses.first().head(), sub)
 *         if (result) then
 *             (result, sub') =
 *                 resolve(clauses.first.rest(), sub')
 *             if (result) then
 *                 return (result, sub')
 *         return prove(goal, clauses.rest(), resolvant, sub)
 * 
 * unify(goal:Term, term:Term, sub:List<name, value>) -> (bool, list<Var,Term>)
 *     if (goal == term) then
 *         return (true, sub)
 *     if (goal is Var) then
 *         return bind(goal, term, sub)
 *     if (term is Var) then
 *         return bind(term, goal, sub)
 *     if (goal isa Term and term isa Term and goal.first() == term.first()
 *         and goal.rest().length() == term.rest().length()) then
 *         do
 *            (result, sub) = unify(goal.rest().first(),
 *                                  term.rest().first(),
 *                                  sub)
 *             goal = goal.rest()
 *             next = term.rest()
 *         while (result and goal != null or next != null)
 *         if (result and goal == null and next == null)
 *             return (true, sub)
 *     return (false, sub)
 * 
 * bind(v:var, t:Term, sub:List<Var,Term>)
 *     if (v occurs in t) then
 *         return (false, sub)
 *     return (true, append(<v, t>, sub))   
 * </code></pre>   
 * <p>Note that the interpreter only returns the first result found; it is
 * improved to support returning all answer when it is converted to use
 * continuations.
 * </p>
 * <p>Using techniques from two papers - "From Interpreter to Logic Engine by
 * Defunctionalization" by Bernacki Dariusz and Danvy Olivier; and "A Functional
 * Derivation of the Warren Abstract Machine" by Maciej Pirog and Jeremy
 * Gibbons - the interpreter will be transformed into a state machine with
 * two stacks.
 * </p>
 * <p>The first step is to apply the CPS transformation. The boolean result
 * indicating success or failure is replaced by a success (sk) and a failure
 * (fk) continuation. The success continuation takes the substitutions and
 * a failure continuation as arguments.  The success continuation can use
 * the passed failure continuation to obtain additional answers by simulating
 * failure at the last choice point in the search; which resumes the search
 * for the next answer.
 * </p>
 * <pre><code>
 * resolve(resolvant:List<term>, sub:List<name, value>, 
 *         sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     sub = []
 *     if (resolvant != null) then
 *         let sk' =
 *             (lambda (sub', fk')
 *                 resolvant = resolvant.next()
 *                 if (resolvant != null ) then
 *                     prove(resolvant.first(), rules(resolvant().first()) sub', sk', fk'))
 *                 else
 *                     sk(sub', fk')
 *         prove(resolvant.first(), rules(resolvant().first()), sub, sk', fk)
 *     else
 *         sk(sub, fk)
 * 
 * prove(goal:Term, clauses:List<clause>, resolvant:List<term>, sub:List<Var,Term>,
 *       sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     if (goal == null) then
 *         fk()
 *     else
 *         unify(goal,
 *               clauses.first().head(),
 *               resolvant,
 *               sub,
 *               (lambda (sub' fk')
 *                   resolve(clauses.first().rest(), sub', sk, fk')),
 *               (lambda ()
 *                   prove(goal, clauses.rest(), resolvant, sub, sk, fk)))
 * 
 * unify(goal:Term, term:Term, sub:List<name, value>,
 *       sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     if (goal == term) then // assumes null == null
 *         sk(sub, fk)
 *     if (goal is Var) then
 *         bind(goal, term, sub, sk, fk)
 *     if (term is Var) then
 *         bind(term, goal, sub, sk, fk)
 *     if (goal isa Term and term isa Term and goal.first() == term.first()
 *         and goal.rest().length() == term.rest().length()) then
 *         let sk' =
 *             (lambda (sub', fk')
 *                 if (goal'.rest() == null and next'.rest() ==null) then
 *                     sk(sub', fk')
 *                 unify(goal.rest(), term.rest(), sub', sk', fk')
 *         unify(goal.first(),
 *               term.first(),
 *               sub,
 *               sk',
 *               fk)
 *     fk()
 * 
 * bind(v:var, t:Term, sub:List<Var,Term>
 *      sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     if (v occurs in t) then
 *         fk()
 *     sk(append(<v,t>, sub), fk)
 * </code></pre>
 * <p>While the interpreter is in continuation passing form, we add support
 * for "memoing" goals to support recursive queries.  Just as we assumed that
 * rules(goal) returns the list of clauses for a goal; we assume that
 * memo(goal) returns true if the goal is defined recursively.
 * </p>
 * <p>We add a list of memoed goals as an additional argument to resolve
 * and prove.  This list is used to avoid infinite loops for recursive queries
 * by checking to see if we are already working on an answer for the goal.
 * </p>
 * <p>The prove_memoed function used instead of prove for memoed goals.
 * Unlike prove, the proved_memoed function must evaluate all applicable
 * clauses for its goal. This ensures that
 * </p>
 * <pre><code>
 * resolve(resolvant:List<term>, sub:List<name, value>, memoed:List<goal, value>, 
 *         sk:(List<name, value>, ()->answer)->answer, fk:()->answer)
 *     sub = []
 *     if (resolvant == null) then
 *         sk(sub, fk)
 *     else 
 *         let sk' =
 *             (lambda (sub', fk')
 *                 resolvant = resolvant.next()
 *                 if (resolvant == null ) then
 *                     sk(sub', fk')
 *                 else
 *                     if (memo(resolvant.first()))
 *                         prove_memoed(resolvant.first(), rules(resolvant().first()), sub', memoed, sk', fk')
 *                     else
 *                         prove(resolvant.first(), rules(resolvant().first()), sub', memoed, sk', fk'))
 *         if (memo(resolvant.first()))
 *             prove_memoed(resolvant.first(), rules(resolvant().first()), sub, memoed, sk', fk)
 *         else
 *             prove(resolvant.first(), rules(resolvant().first()), sub, memoed, sk', fk)
 * 
 * prove(goal:Term, clauses:List<clause>, resolvant:List<term>, sub:List<Var,Term>,
 *       memoed:Map<Term, Struct<rklist:List<>, answers:List<Var,Term>>, 
 *       sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     if (goal == null) then
 *         fk()
 *     else 
 *         let fk' =
 *             (lambda ()
 *                 prove(goal, clauses.rest(), resolvant, sub, memoed, sk, fk)))
 *         let sk' =
 *            (lambda (sub'', fk'')
 *                resolve(clauses.first().rest(), sub'', memoed, sk, fk'')),
 *         unify(goal,
 *               clauses.first().head(),
 *               sub,
 *               sk'),
 *               fk'))
 * 
 * prove_memoed(goal:Term, clauses:List<clause>, resolvant:List<term>, sub:List<Var,Term>,
 *              memoed:Map<Term, Struct<rklist:List<>, answers:List<Var,Term>>, 
 *              sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     if (goal == null) then
 *         fk()
 *     else if (memoed.contains(goal))
 *         memoed.get(goal).sklist.add(sk)
 *         answer_loop(memoed.get(goal).alist, sk, fk)
 *     else
 *         let fk' =
 *             (lambda ()
 *                 prove(goal, clauses.rest(), resolvant, sub, memoed, sk, fk)))
 *         let sk' =
 *            (lambda (sub'', fk'')
 *                let sk'' =
 *                   (lambda (sub''', fk''')
 *                       alist = memoed.get(goal).alist.add(sub''')
 *                       sklist = memoed.get(goal).sklist
 *                       suspended_loop(sklist, sub''', (lambda () sk(sub''' fk'''))
 *                resolve(clauses.first().rest(), sub'', memoed, sk'', fk'')),
 *         unify(goal,
 *               clauses.first().head(),
 *               sub,
 *               sk',
 *               fk'))
 * 
 * answer_loop(lambda (alist, sk, fk)
 *     if (alist == null) then
 *         fk()
 *     else
 *         sk(alist.first(), (lambda() answer_loop(alist.rest(), sk, fk)))
 *         
 * suspended_loop(lambda (sklist, sub, fk)
 *     if (sklist == null) then
 *         fk()
 *     else
 *         sklist'.first()(sub, (lambda () suspended_loop(sklist.rest(), sub, fk))
 *         
 * unify(goal:Term, term:Term, sub:List<name, value>,
 *       sk:(list<name, value>, ()->answer)->answer,
 *       fk:()->answer)
 *     if (goal == term) then // assumes null == null
 *         sk(sub, fk)
 *     if (goal is Var) then
 *         bind(goal, term, sub, sk, fk)
 *     if (term is Var) then
 *         bind(term, goal, sub, sk, fk)
 *     if (goal isa Term and term isa Term and goal.first() == term.first()
 *         and goal.rest().length() == term.rest().length()) then
 *         term_loop(goal, term, sub, sk, fk)
 *     fk()
 * 
 * term_loop(goal:Term, term:Term, sub:List<name, value>,
 *           sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *           fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (goal.rest() == null and term.rest() ==null) then
 *         sk(sub, fk)
 *     else
 *         unify(goal.first(),
 *               term.first(),
 *               sub,
 *               (lambda () term_loop(goal.rest(), term.rest(), sub, sk, fk)),
 *               fk)
 *
 * bind(v:var, t:Term, sub:List<Var,Term>
 *       sk:(list<name, value>, ()->answer)->answer,
 *       fk:()->answer)
 *     if (v occurs in t) then
 *         fk()
 *     sk(append(<v,t>, sub), fk)
 * </code></pre>
 * <p>The next step is to defunctionalize the continuations; converting them into
 * explicit closures and defining an apply function for them.  The success,
 * failure, and call continuations each have their own apply function.
 * The sucess and failure continuations share a common data structure for their
 * closures.
 * </p>
 * <p>There are two failure continuations: the starting failure continuation,
 * and the failure continuation that tries alternative clauses for a goal.
 * The starting failure continuation can be identified as it does not
 * contain a previous failure continuation in its closure.
 * </p>
 * <p>There are three success continuations: the staring success continuation,
 * the success continuation which tries the next term in the resolvant, and
 * the success continuation which tries the next term in a clause for a goal.
 * The starting success continuation can be identified as it does not
 * contain a previous success continuation in its closure.
 * </p>
 * <p>There are also two call continuations: the inital call continuation,
 * which wraps the success continuation that tries the next term in the
 * resolvant, and the call continuation which attempts to unify the next pair
 * of terms in unify's inner loop. The starting call continuation can be
 * identified as it does not contain a previous call continuation in its closure.
 * </p>
 * <pre><code>
 * resolve(resolvant:List<term>, sub:List<name, value>, memoed:List<goal, value>,
 *         sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *         fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (resolvant == null) then
 *         apply(sclosure, sub, fclosure)
 *     else 
 *         let sclosure' =
 *             new sclosure(NEXT, clauses, resolvant, sub, sclosure, fclosure),
 *         if (memo(resolvant.first()))
 *             prove_memoed(resolvant.first(), rules(resolvant().first()),
 *                          sub, memoed, sclosure', fclosure)
 *         else
 *             prove(resolvant.first(), rules(resolvant().first()),
 *                   sub, memoed, sclosure', fclosure)
 * 
 * prove(goal:Term, clauses:List<clause>, resolvant:List<term>, sub:List<Var,Term>,
 *       memoed:Map<Term, Struct<rklist:List<>, answers:List<Var,Term>>, 
 *       sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *       fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (goal == null) then
 *         apply(fclosure)
 *     else
 *         let sclosure' =
 *             new sclosure(NEW, clauses, resolvant,
 *                          sub, memoed, sclosure, fclosure),
 *         let fclosure' =
 *             new fclosure(goal, clauses, resolvant,
 *                          sub, memoed, sclosure, fclosure)))
 *         unify(goal,
 *               clauses.first().head(),
 *               sub,
 *               sclosure',
 *               fclosure')
 * 
 * prove_memoed(goal:Term,
 *              clauses:List<clause>, resolvant:List<term>, sub:List<Var,Term>,
 *              memoed:Map<Term, Struct<rklist:List<>, answers:List<Var,Term>>, 
 *              sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     if (goal == null) then
 *         apply(fclosure)
 *     else if (memoed.contains(goal))
 *         memoed.get(goal).sklist.add(sk)
 *         answer_loop(memoed.get(goal).alist,
 *                     sclosure,
 *                     fclosure)
 *     else
 *         let sclosure' =
 *             new sclosure(NEW_ANSWER, clauses, resolvant,
 *                          sub, memoed, sclosure, fclosure),
 *         let sclosure'' =
 *             new sclosure(NEW_RESOLVANT, clauses, resolvant,
 *                          sub, memoed, sclosure', fclosure),
 *         let fclosure' =
 *             new fclosure(goal, clauses, resolvant,
 *                          sub, memoed, sclosure, fclosure)))
 *         unify(goal,
 *               clauses.first().head(),
 *               sub,
 *               sclosure'',
 *               fclosure'))
 * 
 * answer_loop(alist,
 *             sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *             fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (alist == null) then
 *         apply(fclosure)
 *     else
 *         apply(sclosure,
 *               alist.first(),
 *               new fclosure(ANSWER_LOOP, alist.rest(), sclosure, fclosure))
 *        
 * suspended_loop(sklist,
 *                sub:List<Var,Term>,
 *                fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (sklist == null) then
 *         apply(fclosure)
 *     else
 *         apply(sklist.first(),
 *               sub,
 *               new fclosure(SUSPENDED_LOOP, sklist.next, sub, fclosure))
 *               
 * unify(goal:Term, term:Term, sub:List<name, value>,
 *       sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     if (goal == term) then // assumes null == null
 *         sk(sub, fk)
 *     if (goal is Var) then
 *         bind(goal, term, sub, sk, fk)
 *     if (term is Var) then
 *         bind(term, goal, sub, sk, fk)
 *     if (goal isa Term and term isa Term and goal.first() == term.first()
 *         and goal.rest().length() == term.rest().length()) then
 *         term_loop(goal, term, sub, sk, fk)
 *     fk()
 * 
 * term_loop(goal:Term, term:Term, sub:List<name, value>,
 *           sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *           fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (goal.rest() == null and term.rest() ==null) then
 *         sk(sub, fk)
 *     else
 *         unify(goal.first(),
 *               term.first(),
 *               sub,
 *               new sclosure(TERM_LOOP, goal.rest(), term.rest(), sub, sk, fk)),
 *               fk)
 *        
 * bind(v:var, t:Term, sub:List<Var,Term>
 *       cclosure:{goal, term, cclosure, sclosure, fclosure},
 *       fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (v occurs in t) then
 *         return apply(fclosure)
 *     return apply(cclosure, append(<v,t>, sub))
 *         
 * apply(sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *       sub:List<Var,Term>,
 *       fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (sclosure#sclosure == null) then
 *        return (true, sub, fclosure)
 *     else if (sclosure#kind == NEXT)
 *        resolvant = sclosure#resolvant.next()
 *        if (resolvant == null ) then
 *            apply(sclosure#sclosure, sclosure#sub, sclosure#fclosure)
 *        else
 *            let sclosure' = 
 *                new sclosure(NEXT, sclosure#clauses, sclosure#resolvant,
 *                             sub, sclosure#memoed, sclosure#sclosure, fclosure),
 *            if (memo(resolvant.first()))
 *                prove_memoed(resolvant.first(),
 *                             rules(resolvant().first()),
 *                             sclosure#sub,
 *                             sclosure#memoed,
 *                             sclosure',
 *                             fclosure)
 *            else
 *                prove(resolvant.first(),
 *                      rules(resolvant().first()),
 *                      sclosure#sub,
 *                      sclosure#memoed,
 *                      sclosure'
 *                      fclosure))
 *     else if (sclosure#kind == TERM_LOOP)
 *         if (sclosure#goal.rest() == null and sclosure#term.rest() == null) then
 *             apply(sclosure#sclosure, sub, sclosure#fclosure)
 *         else
 *             unify(sclosure#goal.rest(),
 *                   sclosure#term.rest(),
 *                   sub,
 *                   new sclosure(TERM_LOOP, 
 *                                sclosure#goal.rest(),
 *                                sclosure#term.rest(),
 *                                sub
 *                                sclosure#sclosure,
 *                                sclosure#fclosure),                    
 *                   sclosure#fclosure)
 *     else if (sclosure#kind = NEW_RESOLVANT)
 *        resolve(sclosure#clauses.first().rest(),
 *                sclosure#sub,
 *                sclosure#memoed,
 *                sclosure#sclosure,
 *                sclosure#fclosure)
 *     else if (sclosure#kind = NEW_ANSWER)
 *        alist = sclosure#memoed.get(goal).alist.add(sub''')
 *        sklist = sclosure#memoed.get(goal).sklist
 *        suspended_loop(sklist,
 *                       sclosure#sub,
 *                       new fclosure(RETURN_ANSWER,
 *                                    sclosure#sub,
 *                                    sclosure#sclosure,
 *                                    sclosure#fclosure))
 *
 * apply(fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (fclosure#fclosure == null)
 *         return (false, [], null)
 *     else if (fclosure#kind = ANSWER_LOOP)
 *         answer_loop(fclosure#list, fclosure#sclosure, fclosure#fclosure)
 *     else if (fclosure#kind = SUSPENDED_LOOP)
 *         suspended_loop(fclosure#list, fclosure#sub, fclosure#fclosure)
 *     else if (fclosure#kind = RETURN_LOOP)
 *         apply(fclosure#sclosure, fclosure#sub, fclosure#fclosure)
 *     else if (fclosure#kind = UNIFY_LOOP)
 *         unify_loop(fclosure#list, fclosure#sub, fclosure#fclosure)
 *     else
 *         prove(fclosure#goal,
 *               fclosure#clauses.rest(),
 *               fclosure#resolvant,
 *               fclosure#sub,
 *               fclosure#sclosure,
 *               fclosure#fclosure) 
 * </code></pre>
 * <p>The final step is to realize that all the functionn calls are tail calls,
 * so they can be replaced by labels and gotos statements.
 * </p>
 * <pre><code>
 * RESOLVE:
 *     // (resolvant:List<term>, sub:List<name, value>, memoed:List<goal, value>,
 *     // sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     // fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (resolvant == null) then
 *         GOTO SUCCESS(sclosure, sub, fclosure)
 *     else 
 *         let sclosure' =
 *             new sclosure(NEXT, clauses, resolvant, sub, sclosure, fclosure),
 *         if (memo(resolvant.first()))
 *             GOTO PROVE_MEMOED(resolvant.first(), rules(resolvant().first()),
 *                               sub, memoed, sclosure', fclosure)
 *         else
 *             GOTO PROVE(resolvant.first(), rules(resolvant().first()),
 *                        sub, memoed, sclosure', fclosure)
 * 
 * 
 * PROVE:
 *     // goal:Term, clauses:List<clause>, resolvant:List<term>, sub:List<Var,Term>,
 *     // memoed:Map<Term, Struct<rklist:List<>, answers:List<Var,Term>>, 
 *     // sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     // fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (goal == null) then
 *         GOTO FAILURE(fclosure)
 *     else
 *         let sclosure' =
 *             new sclosure(NEW, clauses, resolvant,
 *                          sub, memoed, sclosure, fclosure),
 *         let fclosure' =
 *             new fclosure(goal, clauses, resolvant,
 *                          sub, memoed, sclosure, fclosure)))
 *         GOTO UNIFY(goal,
 *                    clauses.first().head(),
 *                    sub,
 *                    sclosure',
 *                    fclosure'))
 * 
 * PROVE_MEMOED:
 *     // goal:Term,
 *     // clauses:List<clause>, resolvant:List<term>, sub:List<Var,Term>,
 *     // memoed:Map<Term, Struct<rklist:List<>, answers:List<Var,Term>>, 
 *     // sk:(list<name, value>, ()->answer)->answer, fk:()->answer)
 *     if (goal == null) then
 *         GOTO FAILURE(fclosure)
 *     else if (memoed.contains(goal))
 *         memoed.get(goal).sklist.add(sk)
 *         GOTO ANSWER_LOOP(memoed.get(goal).alist,
 *                          sclosure,
 *                          fclosure)
 *     else
 *         let sclosure' =
 *             new sclosure(NEW_ANSWER, clauses, resolvant,
 *                          sub, memoed, sclosure, fclosure),
 *         let sclosure'' =
 *             new sclosure(NEW_RESOLVANT, clauses, resolvant,
 *                          sub, memoed, sclosure', fclosure),
 *         let fclosure' =
 *             new fclosure(goal, clauses, resolvant,
 *                          sub, memoed, sclosure, fclosure)))
 *         GOTO UNIFY(goal,
 *                    clauses.first().head(),
 *                    sub,
 *                    sclosure'',
 *                    fclosure'))
 * 
 * ANSWER_LOOP:
 *     // alist,
 *     // sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     // fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (alist == null) then
 *         GOTO FAILURE(fclosure)
 *     else
 *         GOTO SUCCESS(sclosure,
 *                      alist.first(),
 *                      new fclosure(ANSWER_LOOP, alist.rest(), sclosure, fclosure))
 * 
 * SUSPENDED_LOOP:
 *     // sklist,
 *     // sub:List<Var,Term>,
 *     // fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (sklist == null) then
 *         GOTO FAILURE(fclosure)
 *     else
 *         GOTO SUCCESS(sklist.first(),
 *                      sub,
 *                      new fclosure(SUSPENDED_LOOP, sklist.next, sub, fclosure))
 *               
 * UNIFY:
 *     // goal:Term, term:Term, sub:List<name, value>,
 *     // sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     // fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (goal == term) then // assumes null == null
 *         GOTO SUCCESS(sclosure, sub, fclosure)
 *     if (goal is Var) then
 *         bind(goal, term, sub, sk, fk)
 *     if (term is Var) then
 *         bind(term, goal, sub, sk, fk)
 *     if (goal isa Term and term isa Term and goal.first() == term.first()
 *         and goal.rest().length() == term.rest().length()) then
 *         GOTO TERM_LOOP(goal, term, sub, sclosure, fclosure)
 *     GOTO FAILURE(fclosure)
 * 
 * TERM_LOOP:
 *     // goal:Term, term:Term, sub:List<name, value>,
 *     // sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     // fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (goal.rest() == null and term.rest() ==null) then
 *         GOTO SUCCESS(sclosure, sub, fclosure)
 *     else
 *         GOTO UNIFY(goal.first(),
 *                    term.first(),
 *                    sub,
 *                    new sclosure(TERM_LOOP, goal.rest(), term.rest(), sub, sk, fk)),
 *                    fk)
 *        
 * bind(v:var, t:Term, sub:List<Var,Term>
 *       cclosure:{goal, term, cclosure, sclosure, fclosure},
 *       fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (v occurs in t) then
 *         GOTO FAILURE(fclosure)
 *     GOTO SUCCESS(sclosure, sub, fclosure)
 *         
 * SUCCESS:
 *     // sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     // sub:List<Var,Term>,
 *     // fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (sclosure#sclosure == null) then
 *        GOTO RETURN(true, sub, fclosure)
 *     else if (sclosure#kind == NEXT)
 *        resolvant = sclosure#resolvant.next()
 *        if (resolvant != null ) then
 *            GOTO PROVE(resolvant.first(), 
 *                  rules(resolvant.first()),
 *                  sclosure#sub,
 *                  sclosure#memoed,
 *                  new sclosure(NEXT, sclosure#clauses, sclosure#resolvant,
 *                               sub, sclosure#memoed, sclosure#sclosure, fclosure),
 *                  fclosure)
 *        else
 *            GOTO SUCCESS(sclosure#sclosure, sclosure#sub, sclosure#fclosure)
 *     else if (sclosure#kind = NEW_RESOLVANT)
 *        GOTO RESOLVE(sclosure#clauses.first().rest(),
 *                     sclosure#sub,
 *                     sclosure#memoed,
 *                     sclosure#sclosure,
 *                     sclosure#fclosure)
 *     else if (sclosure#kind == TERM_LOOP)
 *         if (sclosure#goal.rest() == null and sclosure#term.rest() == null) then
 *             GOTO SUCCESS(sclosure#sclosure, sub, sclosure#fclosure)
 *         else
 *             GOTO UNIFY(sclosure#goal.rest(),
 *                        sclosure#term.rest(),
 *                        sub,
 *                        new sclosure(TERM_LOOP, 
 *                                     sclosure#goal.rest(),
 *                                     sclosure#term.rest(),
 *                                     sub
 *                                     sclosure#sclosure,
 *                                     sclosure#fclosure),                    
 *                        sclosure#fclosure)
 *     else if (sclosure#kind = NEW_ANSWER)
 *        alist = sclosure#memoed.get(goal).alist.add(sub''')
 *        sklist = sclosure#memoed.get(goal).sklist
 *        GOTO SUSPEND_LOOP(sklist,
 *                          sclosure#sub,
 *                          new fclosure(RETURN_ANSWER,
 *                                       sclosure#sub,
 *                                       sclosure#sclosure,
 *                                       sclosure#fclosure))
 *
 * FAILURE:
 *   // (fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (fclosure#fclosure == null)
 *         GOTO HALT(false, [], null)
 *     else if (fclosure#kind = ANSWER_LOOP)
 *         GOTO ANSWER_LOOP(fclosure#list, fclosure#sclosure, fclosure#fclosure)
 *     else if (fclosure#kind = SUSPENDED_LOOP)
 *         GOTO SUSPEND_LOOP(fclosure#list, fclosure#sub, fclosure#fclosure)
 *     else if (fclosure#kind = RETURN_LOOP)
 *         GOTO SUCCESS(fclosure#sclosure, fclosure#sub, fclosure#fclosure)
 *     else if (fclosure#kind = UNIFY_LOOP)
 *         GOTO UNIFY_LOOP(fclosure#list, fclosure#sub, fclosure#fclosure)
 *     else
 *         GOTO PROVE(fclosure#goal,
 *                    fclosure#clauses.rest(),
 *                    fclosure#resolvant,
 *                    fclosure#sub,
 *                    fclosure#sclosure,
 *                    fclosure#fclosure)
 *    
 * </code></pre>
 * <p>
 * TODO
 * </p>
 * <pre><code>
 * RESOLVE:
 *     // (resolvant:List<term>, sub:List<name, value>, 
 *     //  sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     //  fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (resolvant != null) then
 *         GOTO PROVE(resolvant.first(),
 *                    copy(rules(resolvant.first())),
 *                    sub,
 *                    new sclosure(NEXT, clauses, resolvant, sub, sclosure, fclosure),
 *                    fclosure)
 *     else
 *         GOTO SUCCESS(sclosure, sub, fclosure)
 * 
 * PROVE:
 *     // (goal:Term, clauses:List<clause>, resolvant:List<term>, sub:List<Var,Term>,
 *     //  sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     //  fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (goal == null) then
 *         GOTO FAILURE(fclosure)
 *     else
 *         let sclosure' =
 *             new sclosure(clauses, resolvant,
 *                          sub, sclosure, fclosure),
 *         let fclosure' =
 *             new fclosure(goal, clauses, resolvant,
 *                          sub, sclosure, fclosure)))
 *         GOTO UNIFY(goal,
 *                    clauses.first().head(),
 *                    sub,
 *                    new cclosure(null, null, null,
 *                                 sclosure',
 *                                 fclosure'),
 *                    fclosure'))
 * 
 * UNIFY:
 *     // (goal:Term, term:Term, sub:List<name, value>,
 *     //  cclosure:{goal, term, cclosure, sclosure, fclosure},
 *     //  fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (goal == term) then // assumes null == null
 *         GOTO UNIFY_RETURN(cclosure, sub)
 *     if (goal is Var) then
 *         if (goal occurs in term) then
 *             GOTO FAILURE(fclosure)
 *         GOTO UNIFY_RETURN(cclosure, append(<v,t>, sub))
 *     if (term is Var) then
 *         if (term occurs in goal) then
 *             GOTO FAILURE(fclosure)
 *         GOTO UNIFY_RETURN(cclosure, append(<v,t>, sub))
 *     if (goal isa Term and term isa Term and goal.first() == term.first()
 *         and goal.rest().length() == term.rest().length()) then
 *         GOTO UNIFY(goal.first(),
 *                    term.first(),
 *                    sub,
 *                    new cclosure(goal, term, cclosure, null, null),
 *                    fclosure)
 *     GOTO FAILURE(fclosure)
 * 
 * SUCCESS:
 *     // (sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *     //  sub:List<Var,Term>,
 *     //  fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (sclosure#sclosure == null) then
 *         GOTO RETURN(true, sub, fclosure)
 *     else
 *         GOTO sclosure#kind(sclosure)
 * 
 * PROVE_NEXT:
 *     // (sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *        resolvant = sclosure#resolvant.next()
 *        if (resolvant != null ) then
 *            return prove(resolvant.first(), 
 *                         rules(resolvant.first()),
 *                         sclosure#sub,
 *                         new sclosure(NEXT, sclosure#clauses, sclosure#resolvant, sub, sclosure#sclosure, fclosure),
 *                         fclosure)
 *            else
 *                return apply(sclosure#sclosure, sclosure#sub, sclosure#fclosure)
 * 
 * RESOLVE_NEW:
 *     // (sclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure},
 *        GOTO RESOLVE(sclosure#clauses.first().rest(),
 *                     sclosure#sub,
 *                     sclosure#sclosure,
 *                     sclosure#fclosure)
 *
 * FAILURE:
 *     // (fclosure:(kind, goal, clauses, resolvant, sub, sclosure, fclosure})
 *     if (fclosure#fclosure == null)
 *         GOTO HALT(false, [], null)
 *     else
 *         GOTO PROVE(fclosure#goal,
 *                    fclosure#clauses.rest(),
 *                    fclosure#resolvant,
 *                    fclosure#sub,
 *                    fclosure#sclosure,
 *                    fclosure#fclosure)
 *    
 * UNIFY_RETURN:
 *     // (cclosure:{goal, term, sclosure, fclosure}, sub:List<Var,Term>)
 *     if (cclosure#cclosure == null)
 *         GOTO SUCCESS(cclosure#sclosure, sub, cclosure#fclosure)
 *     else
 *         if (cclosure#goal.rest() == null and cclosure#term.rest() == null) then
 *             GOTO UNIFY_RETURN:(cclosure#cclosure, sub)
 *         GOTO UNIFY(cclosure#goal.rest(),
 *                    cclosure#term.rest(),
 *                    sub,
 *                    new cclosure(cclosure#goal.rest(),
 *                                 cclosure#term.rest(),
 *                                 cclosure
 *                                 null,
 *                                 null),                    
 *                      cclosure#fclosure)
 * </code></pre>
 */
public class LogicEngineImpl implements RuleEngine, AutoCloseable, Supplier<Map<String,Object>> {
	
	Logger logger = LogManager.getLogger();

	static public enum State {
		INIT,
		RESOLVE,
		PROVE,
		UNIFY,
	    UNIFY_RETURN,
		SUCCESS,
		PROVE_NEXT,
		RESOLVE_NEW,
		FAILURE,
		RETURN,
		HALT;		
	}

	/**
	 *
	 */
	static private class ResolveStackFrame {
		
		State resumeState;
		Object savedGoal;
		Term[] savedClause;
		Clause[] savedClauseList;
		int savedNextClauseIndex;
		Term[] savedResolvant;
		int savedNextTermIndex;
		int savedActiveChoiceTrailPos;
		ResolveStackFrame savedResolveStackTop;
		ResolveStackFrame savedActivedChoiceFrame;

		ResolveStackFrame(State state,
				          Object goal,
				          Term[] clause,
						  Clause[] clauseList,
						  int nextClauseIndex,
				          Term[] resolvant,
				          int nextTermIndex,
				          int activeChoiceTrailPos,
				          ResolveStackFrame resolveStackTop,
				          ResolveStackFrame activeChoiceFrame) {
			this.resumeState = state;
			this.savedGoal = goal;
			this.savedClause = clause;
			this.savedClauseList = clauseList;
			this.savedNextClauseIndex = nextClauseIndex;
			this.savedResolvant = resolvant;
			this.savedNextTermIndex = nextTermIndex;
			this.savedActiveChoiceTrailPos = activeChoiceTrailPos;
			this.savedResolveStackTop = resolveStackTop;
			this.savedActivedChoiceFrame = activeChoiceFrame;
		}
	}

	/**
	 * 
	 */
	static private class UnifyStackFrame {

		Object savedGoal;
		Object savedTerm;
		LinkedList<Object> goals;
		LinkedList<Object> terms;
		UnifyStackFrame oldUnifyStackTop;

		UnifyStackFrame(Object savedGoal,
		                Object savedTerm,
		                LinkedList<Object> goals,
				        LinkedList<Object> terms,
				        UnifyStackFrame unifyStackTop) {
			this.savedGoal = savedGoal;
			this.savedTerm = savedTerm;
			this.goals = goals;
			this.terms = terms;
			this.oldUnifyStackTop = unifyStackTop;
		}
	}
	
	static private class VariableMapper implements TermVisitor {
		
		public HashMap<String,Object> variableBindings;

		/**
		 * creates a new Copier together with its related HashDict for variables
		 */
		VariableMapper() {
			variableBindings = new HashMap<>();
		}

		@Override
		public void visit(AtomRuntime node) {
		}
	
		@Override
		public void visit(ClauseRuntime node) {
			visit((StructureRuntime) node);
		}
	
		@Override
		public void visit(Conjunction node) {
			visit((StructureRuntime) node);
		}
	
		@Override
		public void visit(Cons node) {
			visit((StructureRuntime) node);
		}
	
		@Override
		public void visit(StructureRuntime node) {
			StructureRuntime structure = node.clone();
		    structure.args = new Object[node.args.length];
		    for (int i = 0; i < node.args.length; i += 1) {
		    	if (node.args[i] instanceof AbstractTerm) {
			    	((AbstractTerm) node.args[i]).inspect(this);
		    	}
		    }
		}
	
		@Override
		public void visit(AbstractSystemObject node) {
		}
	
		@Override
		public void visit(VariableRuntime node) {
			variableBindings.put(node.getName(), node.ref());
		}
	}
	
	/**
	 * The engine's state.
	 */
	State state;
	
	/**
	 * The start goals which the engine is attempting to statisfy.
	 */
	LinkedList<Term> startingGoals;
	
	/**
	 * Remaining terms which must be resolved to satisfy the starting
	 * goals.
	 */
	Term[] resolvant;
	int nextTermIndex;
	
	/**
	 * Current goal the engine is attempting to prove.
	 */
	Object goal;
	
	/**
	 * Remaining clauses which might prove the current goal.
	 */
	Clause[] clauseList;
	int nextClauseIndex;
	
	/**
	 * Remaining clauses which might prove the current goal.
	 */
	Term[] clause;
	
	/**
	 * A term to unify with the goal.
	 */
	Object term;
	
	ResolveStackFrame resolveStackTop;
	ResolveStackFrame activeChoiceFrame;
	UnifyStackFrame unifyStackTop;

	/**
	 * 
	 */
	Trail trail;

	public LogicEngineImpl(Term goal) {
		super();
		LinkedList<Term> goals = new LinkedList<Term>();
		goals.add(goal);
		startingGoals = goals;
		state = State.INIT;
	}
	
	public LogicEngineImpl(List<Term> goals) {
		super();
		startingGoals = new LinkedList<Term>(goals);
		state = State.INIT;
	}
	
	public LogicEngineImpl(Clause clause) {
		super();
		LinkedList<Term> goals = conjunctionToList(clause.getBody());
		startingGoals = goals;
		state = State.INIT;
	}
	
	
	/**
	 * 
	 * @param term
	 * @return
	 */
	private LinkedList<Term> conjunctionToList(Term term) {
		LinkedList<Term> list = new LinkedList<Term>();
	    while (term instanceof Conjunction) {
	        list.add((Term) ((Term) ((Conjunction)term).args[0]).ref());
	        term = (Term) ((Conjunction) term).args[1];
	    }
	    list.add(term);
	    return list;

	}
	
	/**
	 * Implements the state transition function for the INIT state.
	 * 
	 */
	private void init() {
		goal = null;
		term = null;
		clause = null;
		clauseList = null;
		nextClauseIndex = 0;
		unifyStackTop = null;
		resolveStackTop = null;
		activeChoiceFrame = null;
		trail = new Trail();
		resolvant = startingGoals.toArray(new Term[0]);
		nextTermIndex = 0;
		if (logger.isTraceEnabled()) {
			StringBuilder sb = new StringBuilder();
			for (int i = nextTermIndex; i < resolvant.length; i += 1) {
				sb.append(" ").append(resolvant[i].toString());
			}
			logger.trace("INIT: resolvant =\"{}\"", sb.toString());
		}
		reduceBuiltins();
		state = State.RESOLVE;
	}
	
	/**
	 * 
	 */
    private void reduceBuiltins() {
        logger.trace("entering reduceBuiltins {}", goal);

        while (nextTermIndex < resolvant.length) {

        	term = resolvant[nextTermIndex];
            if (!(term instanceof Term))
            	return;

            int ok = 0;
            try {
            	ok = ((Term) term).exec(this); // (possibly) executes builtin
            } catch (RuntimeException e) {
            	logger.trace("builtin threw exception {}", goal, e);
            	break;
            }

            switch (ok) {

            case -1: // nothing to do, this is not a builtin
            	logger.trace("not a builtin {}", goal);
                break;

            case 1: // builtin suceeds
            	logger.trace("builtin succeeded {}", goal);
            	nextTermIndex += 1;
                continue; // advance

            case 0: // builtin fails
            	logger.trace("builtin failed {}", goal);
                break; 

            default: // unexpected code: programming error
                logger.error("Bad return code: ({}) in builtin: {}", ok, term);
                break;
            }
            
            logger.trace("leaving reduceBuiltins: {}", goal);
            break;
        }
    }
		
	/**
	 * Implements the state transition function for the RESOLVE state.
	 * 
	 */
	private void resolve() {
		if (nextTermIndex >= resolvant.length) {
	    	logger.trace("RESOLVE: resolvant is empty");
			state = State.SUCCESS;
		} else {
			goal = resolvant[nextTermIndex++];
	    	logger.trace("RESOLVE: goal = \"{}\"", goal);
            LinkedList<Term> terms = DataBase.getInstance().getClauses(((Term) goal).getKey());
            clauseList = (terms == null) ? new Clause[0] : (Clause[]) terms.toArray(new Clause[0]);
            if (logger.isTraceEnabled()) {
                for (int i = 0; i < clauseList.length; i += 1) {
                	logger.trace("    matching clause = (key:{}) '{}'", clauseList[i].getKey(), clauseList[i]);
                }
            }
            nextClauseIndex = 0;
    		resolveStackTop =
    				new ResolveStackFrame(State.PROVE_NEXT, null, null, clauseList, nextClauseIndex,
    						resolvant, nextTermIndex, trail.size(), resolveStackTop, activeChoiceFrame);
			state = State.PROVE;					
		}				
	}
	
	/**
	 * Implements the state transition function for the PROVE state.
	 * 
	 */
	private void prove() {
		if (goal == null) {
	        logger.trace("PROVE: no goal");
			state = State.FAILURE;
			return;
		}
		if (nextClauseIndex >= clauseList.length) {
	        logger.trace("PROVE: no more clauses");
			state = State.FAILURE;
			return;
		}
		clause = clauseList[nextClauseIndex++].ccopy().toArray();
		resolveStackTop =
				new ResolveStackFrame(State.RESOLVE_NEW, goal, clause, clauseList, nextClauseIndex,
						resolvant, nextTermIndex, trail.size(), resolveStackTop, activeChoiceFrame);
		activeChoiceFrame = resolveStackTop;
		term = clause[0];
        logger.trace("PROVE: try to unify \"{}\" with \"{}\"", goal, clause[0]);
		unifyStackTop = new UnifyStackFrame(goal, term, null, null, null);		
		state = State.UNIFY;
	}
	
	/**
	 * Implements the state transition function for the UNIFY state.
	 * 
	 */
	private final void unify() {
		
		// do they unify as something other than structures?
		state = unifyIfNotStructure(goal, term);
		if (logger.isTraceEnabled()) {
			if (state == State.FAILURE)
				logger.trace("UNIFY: failed to unify {} with {}", goal, term);
			
			if (state == State.UNIFY_RETURN)
				logger.trace("UNIFY: unified {} with {}", goal, term);
		}
		if (state != State.UNIFY)
			return;
			
		Structure goalStructure = (Structure) ((Term) goal).ref();
		Structure termStructure = (Structure) ((Term) term).ref();
		if (!goalStructure.name().equals(termStructure.name())) {
			logger.trace("UNIFY: failed to unify {} with {)", goal, term);
			state = State.FAILURE;
			return;
		}
		
		LinkedList<Object> goals = new LinkedList<Object>();
		Object args[] = goalStructure.getArgs();
		for (int i = 0; i < args.length; i += 1)
			goals.add(args[i]);
		LinkedList<Object> terms = new LinkedList<Object>();
		args = termStructure.getArgs();
		for (int i = 0; i < args.length; i += 1)
			terms.add(args[i]);
		if (goals.isEmpty() && terms.isEmpty()) {
			state = State.SUCCESS;
			return;
		}
		if (goals.isEmpty() || terms.isEmpty()) {
			logger.trace("UNIFY: failed to unify {} with {)", goal, term);
			state = State.FAILURE;
			return;
		}
		Object savedGoal = goal;
		Object savedTerm = term;
		goal = goals.removeFirst();
		term = terms.removeFirst();
		unifyStackTop = new UnifyStackFrame(savedGoal, savedTerm, goals, terms, unifyStackTop);		
		state = State.UNIFY;
	}
	
	/**
	 * Helps the UNIFY state transition function by implementing unify tests
	 * for everything but structures.
	 * 
	 * @param g
	 * @param t
	 * @return
	 */
	private State unifyIfNotStructure(Object g, Object t) {
		// do they unify as Java objects?
		State returnState = unifyAsJavaObjects(goal, term);
		if (returnState != State.UNIFY)
			return returnState;
			
		// failed as Java objects, so both must be Terms to unify
		if (!(goal instanceof Term) || !(term instanceof Term))
		    return State.FAILURE;
				
		// dereference the terms, moving down any variable chains
		Object goalDeref = ((Term) goal).ref();
		Object termDeref = ((Term) term).ref();
				
		// a variable could be bound to a Java Object so check again
		returnState = unifyAsJavaObjects(goalDeref, termDeref);
		if (returnState != State.UNIFY)
			return returnState;

		// derefs failed as Java objects, so both must be Terms to unify
		if (!(goalDeref instanceof Term) || !(termDeref instanceof Term))
			return State.FAILURE;
						
		// is either of them an unbound variable can they unify?
		returnState = bindIfEitherIsaVariable((Term) goalDeref, (Term) termDeref);
		if (returnState != State.UNIFY)
			return returnState;
							
		// only option left is unifying two structures
		if (!(goalDeref instanceof Structure) || !(termDeref instanceof Structure))
			return State.FAILURE;
		
		return returnState;
	}

	/**
	 * Helps the UNIFY state transition function by implementing unify tests
	 * for Java objects.
	 * 
	 * @param g
	 * @param t
	 * @return
	 */
	private State unifyAsJavaObjects(Object g, Object t) {
		if (g == t)
			return State.UNIFY_RETURN;
		if (g == null || t == null)
			return State.FAILURE;
		if (g.equals(t))
			return State.UNIFY_RETURN;
		logger.trace("equals is {} for {} '{}' {} '{}'", g.equals(t), g.getClass().getTypeName(), g.toString(),
				                                                      t.getClass().getTypeName(), t.toString());
		return State.UNIFY;
	}

	/**
	 * 
	 * @param g
	 * @param t
	 * @return
	 */
	private State bindIfEitherIsaVariable(Term g, Term t) {
		if (g.isVar()) {
			Variable var = (Variable) g;
			if (!var.isUnbound() || varOccursInTerm(var, t))
				return State.FAILURE;
			var.bind_to(t, trail);
			return State.UNIFY_RETURN;
		}
		if (t.isVar()) {
			Variable var = (Variable) t;
			if (!var.isUnbound() || varOccursInTerm(var, g))
				return State.FAILURE;
			var.bind_to(g, trail);
			return State.UNIFY_RETURN;
		}
		return State.UNIFY;
	}

	/**
	 * 
	 * @param var
	 * @param t
	 * @return
	 */
	private boolean varOccursInTerm(Variable var, Term t) {
		if (var == t)
			return true;
		if (!(t instanceof Structure))
			return false;
		Object args[] = ((Structure) t).getArgs();
		for (int i = 0; i < args.length; i += 1) {
			if (args[i] instanceof Term) {
				boolean found = varOccursInTerm(var, (Term) args[i]);
				if (found)
					return true;
			}
		}
		return false;
	}

	/**
	 * 
	 */
	private void unify_return() {
		if (unifyStackTop == null) {
			logger.trace("UNIFY_RETURN: unified \"{}\" with \"{}\"", goal, term);
			state = State.SUCCESS;
			return;
		}
		goal = unifyStackTop.savedGoal;
		term = unifyStackTop.savedTerm;
		LinkedList<Object> goals = unifyStackTop.goals;
		LinkedList<Object> terms = unifyStackTop.terms;
		unifyStackTop = unifyStackTop.oldUnifyStackTop;	
		if (goals == null && terms == null) {
			state = State.UNIFY_RETURN;
			return;
		}
		if (goals == null || term == null) {
			logger.trace("UNIFY_RETURN: failed to unify {} with {)", goal, term);
			state = State.FAILURE;
			return;
		}
		if (goals.isEmpty() && terms.isEmpty()) {
			state = State.UNIFY_RETURN;
			return;
		}
		if (goals.isEmpty() || terms.isEmpty()) {
			logger.trace("UNIFY_RETURN: failed to unify {} with {)", goal, term);
			state = State.FAILURE;
			return;
		}
		goal = goals.removeFirst();
		term = terms.removeFirst();
		unifyStackTop = new UnifyStackFrame(goal, term, goals, terms, unifyStackTop);		
        logger.trace("UNIFY_RETURN: try to unify \"{}\" with \"{}\"", goal, term);
		state = State.UNIFY;
	}

	/**
	 * 
	 */
	private void success() {
		if (resolveStackTop == null) {
			logger.trace("SUCCESS: returning");
			state = State.RETURN;
			return;
		}

		state = resolveStackTop.resumeState;
		goal = resolveStackTop.savedGoal;
		term = null;
		clause = resolveStackTop.savedClause;
		clauseList = resolveStackTop.savedClauseList;
		nextClauseIndex = resolveStackTop.savedNextClauseIndex;
		resolvant = resolveStackTop.savedResolvant;
		nextTermIndex = resolveStackTop.savedNextTermIndex;
		unifyStackTop = null;
		// use the faiure contiuation passed, not the one saved on the stack
		// activeChoiceFrame = resolveStackTop.savedActivedChoiceFrame;
		// trail.unwind(activeChoiceFrame.savedActiveChoiceTrailPos);
		resolveStackTop = resolveStackTop.savedResolveStackTop;
		if (logger.isTraceEnabled()) {
			StringBuilder sb = new StringBuilder();
			for (int i = nextTermIndex; i < resolvant.length; i += 1) {
				sb.append(" ").append(resolvant[i].toString());
			}
			logger.trace("SUCCESS: restoring goal=\"{}\" resolvant=\"{}\"", goal, sb.toString());
		}
	}
	
	/**
	 * Implements the state transition function for the PROVE_NEXT state.
	 * 
	 */
	private final void prove_next() {
		if (nextTermIndex >= resolvant.length) {
	        logger.trace("PROVE_NEXT: resolvant is empty");
			state = State.SUCCESS;
		} else {
			goal = resolvant[nextTermIndex++];
            clauseList = (Clause[]) DataBase.getInstance().getClauses(((Term) goal).getKey()).toArray(new Clause[0]);
            nextClauseIndex = 0;
    		resolveStackTop =
    				new ResolveStackFrame(State.PROVE_NEXT, goal, clause, clauseList, nextClauseIndex,
    						resolvant, nextTermIndex, trail.size(), resolveStackTop, activeChoiceFrame);
			state = State.PROVE;					
		}				
	}
	
	/**
	 * Implements the state transition function for the RESOLVE_NEW state.
	 * 
	 */
	private final void resolve_new() {
		resolvant = clause;
		nextTermIndex = 1; // Index of the first term in the clause body
		if (logger.isTraceEnabled()) {
			StringBuilder sb = new StringBuilder();
			for (int i = nextTermIndex; i < resolvant.length; i += 1) {
				sb.append(" ").append(resolvant[i].toString());
			}
			logger.trace("RESOLVE_NEW: resolvant =\"{}\"", sb.toString());
		}
		reduceBuiltins();
		state = State.RESOLVE;	
	}
	
	/**
	 * Implements the state transition function for the FAILURE state.
	 */
	private void failure() {
		if (activeChoiceFrame == null) {
	        logger.trace("FAILURE: halting");
			state = State.HALT;
			return;
		}
		// restore the choice point
		goal = activeChoiceFrame.savedGoal;
        logger.trace("FAILURE: restoring choice point for goal = \"{}\"", goal);
		term = null;
		clause = activeChoiceFrame.savedClause;
		clauseList = activeChoiceFrame.savedClauseList;
		nextClauseIndex = activeChoiceFrame.savedNextClauseIndex;
		resolvant = activeChoiceFrame.savedResolvant;
		nextTermIndex = activeChoiceFrame.savedNextTermIndex;
		unifyStackTop = null;
		resolveStackTop = activeChoiceFrame.savedResolveStackTop;
		trail.unwind(activeChoiceFrame.savedActiveChoiceTrailPos);
		activeChoiceFrame = activeChoiceFrame.savedActivedChoiceFrame;
		state = State.PROVE_NEXT;									
	}
	
	/**
	 * 
	 * @return
	 */
	@Override
	public Map<String,Object> get() {
		
		for (;;) {
			switch (state) {			
			case INIT:         init(); break;			    
			case RESOLVE:      resolve(); break;			    
			case PROVE:        prove(); break;		    
			case UNIFY:        unify(); break;
			case UNIFY_RETURN: unify_return(); break;
			case SUCCESS:      success(); break;	
			case PROVE_NEXT:   prove_next(); break;
			case RESOLVE_NEW:  resolve_new(); break;
			case FAILURE:      failure(); break;
			case HALT:         close(); return null;			

			// This is where we extract the variable bindings
			// and build the resulting map.
			case RETURN: {
				VariableMapper mapper = new VariableMapper();
				for (Term t : startingGoals) {
					t.inspect(mapper);				
				}
			    // Before returning, simulate a failure.
				// The next get will resume at the last choice point
				// and generate another answer, if there is one.
				failure();
				return mapper.variableBindings;
			}
			default:
				throw new IllegalStateException();
			}			
		}
	}

	public boolean isClosed() {
		return state == State.HALT;
	}

	@Override
	public void close() {
		logger.trace("HALT: for goal = \"{}\"", goal);
		state = State.HALT;		
		// allow memory to be garbage collected
		startingGoals = null;
		resolvant = null;
		goal = null;
		clause = null;
		clauseList = null;
		term = null;
		trail = null;
		resolveStackTop = null;
		activeChoiceFrame = null;
		unifyStackTop = null;	
	}

	@Override
    public final Trail getTrail() {
        return trail;
    }


	@Override
	public RuleEngine getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public StructureRuntime newBuiltin(StructureAstNode S) {
		return Builtins.getInstance().newBuiltin(S);
	}

	@Override
	public AtomRuntime newBuiltin(AtomAstNode S) {
		return Builtins.getInstance().newBuiltin(S);
	}
	
	
    static public Term firstSolution(Term X, Term G) {
        LogicEngineImpl p = new_engine(X, G);
        Map<String, Object> map = ask_engine(p);
        Term retVal;
        if (map != null) {
            StructureRuntime structure = new StructureRuntime("the", map.size());
            retVal = structure;
            int i = 0;
            for (Map.Entry<String,Object> entry : map.entrySet()) {
            	structure.setArg(i++, new VariableRuntime(entry.getKey(), entry.getValue()));
            }
            p.stop();
        } else
            retVal = AtomRuntime.aNo;
        return retVal;
    }

    static public LogicEngineImpl new_engine(Term X, Term G) {
        ClauseRuntime C = new ClauseRuntime(X, G);
        LogicEngineImpl  p = new LogicEngineImpl(C);
        return p;
    }

    static public Map<String, Object> ask_engine(LogicEngineImpl engine) {
        return engine.get();
    }

}
