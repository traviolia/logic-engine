package com.gitlab.traviolia.semantics.rules.runtime;



public interface Structure extends Term {

	public abstract String name();

	public abstract String qname();

	public abstract String getKey();

	public abstract void init(int arity);

	public abstract Object getArg(int i);

	public abstract int getIntArg(int i);

	public abstract void setArg(int i, Object T);

	public abstract int putArg(int i, Object T, RuleEngine p);

//	public abstract TermAstNode token();

//	public abstract Structure funClone();

	public abstract Term listify();
	
	public Object[] getArgs();
	public void setArgs(Object[] args);

}