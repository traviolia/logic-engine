package com.gitlab.traviolia.semantics.rules.runtime;



public interface Variable extends Term {

	public abstract boolean isUnbound();

}