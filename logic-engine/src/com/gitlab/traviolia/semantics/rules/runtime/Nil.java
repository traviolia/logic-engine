package com.gitlab.traviolia.semantics.rules.runtime;

/**
  Special constant terminating a list
*/
public class Nil extends AtomRuntime {
  public Nil(String s){
    super(s);
  }
  
  public Nil(){
    this("[]");
  }
}
