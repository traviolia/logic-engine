/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.runtime;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;

/**
 * Symbolic constant, of arity 0.
 */

public class AtomRuntime extends AbstractNonVar implements Atom {

	public final static AtomRuntime aNil = new Nil();

	public final static AtomRuntime aTrue = new True();

	public final static AtomRuntime aFail = new Fail();

	public final static AtomRuntime aYes = new AtomRuntime("yes");

	public final static AtomRuntime aNo = new AtomRuntime("no");

	public final static AtomRuntime anEof = new AtomRuntime("end_of_file");

	public final static Term the(Term X) {
		return (null == X) ? AtomRuntime.aNo : new StructureRuntime("the", X);
	}

	private String sym;

	public AtomRuntime(String s) {
		sym = s.intern();
	}

	   @Override
	   public AtomRuntime clone() {
			return (AtomRuntime) super.clone();
		}

	public final String name() {
		return sym;
	}

	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public void inspect(TermVisitor visitor) {
		visitor.visit(this);
	}

	public String qname() {
		if (0 == sym.length())
			return "''";
		for (int i = 0; i < sym.length(); i++) {
			if (!Character.isLowerCase(sym.charAt(i)))
				return '\'' + sym + '\'';
		}
		return sym;
	}

	public String toString() {
		return qname();
	}

	@Override
	public boolean bind_to(Term that, Trail trail) {
		return super.bind_to(that, trail) && ((AtomRuntime) that).sym == sym;
	}

	public String getKey() {
		return sym;
	}

	/**
	 * returns an arity normally defined as 0
	 * 
	 * @see AbstractTerm#CONST
	 */
	public int getArity() {
		return Term.CONST;
	}

	/**
	 * creates a ConstBuiltin from a Const known to be registered as being a
	 * builtin while returning its argument unchanged if it is just a plain
	 * Prolog constant with no builtin code attached to it
	 */

	public String toUnquoted() {
		return name();
	}

	@Override
	public int exec(RuleEngine prog) {
		// TODO Auto-generated method stub
		return -11;
	}

	@Override
	public int hashCode() {
		return sym.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AtomRuntime)) {
			return false;
		}
		AtomRuntime other = (AtomRuntime) obj;
		if (sym == null || other.sym == null) {
			return false;
		} else if (sym != other.sym) {
			return false;
		}
		return true;
	}

}
