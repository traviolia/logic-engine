package com.gitlab.traviolia.semantics.rules.runtime;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class GoalTrie implements Collection<Object> {
	
	public static class TrieResult {
		TrieNode trie;
		TrieNode leafNode;
		
		TrieResult(TrieNode trie, TrieNode leafNode) {
			this.trie = trie;
			this.leafNode = leafNode;
		}
	}

	private TrieNode root;
	private int count;
	
	public GoalTrie() {
		clear();
	}

	public TrieResult addGoal(Object goal) {
		Object[] terms = new Object[] {goal};
		TrieResult trieResult = root.findOrAddTerm(new VarMap(), terms, 0);
		if (trieResult.trie == root)
			return trieResult;
		count += 1;
		return trieResult;
	}	

	@Override
	public boolean add(Object arg0) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public boolean addAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void clear() {
		root = new TrieNode(null, TrieNode.Type.ROOT, null, new TrieNode[0]);
		count = 0;
	}

	@Override
	public boolean contains(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		return (root.children.length == 0);
	}

	@Override
	public Iterator<Object> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int size() {
		return count;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray(Object[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}

class VarMap {
	HashMap<Variable,String> map = new HashMap<>();
	int count = 0;
}

class TrieNode {

	enum Type {
		ATOM,
		PREDICATE,
		VARIABLE,
		OTHER,
		ROOT;
	}
	
	Object value;
	Type type;
	TrieNode parent;
	TrieNode[] children;
	
	TrieNode(Object value,	Type type, TrieNode parent, TrieNode[] children) {
		this.value = value;
		this.type = type;
		this.parent = parent;
		this.children = children;
	}
	
	private TrieNode(Object value,	Type type, TrieNode parent) {
		this(value, type, parent, null);
	}
	
	int matchAtom(String key) {
		for (int i = 0; i < children.length; i += 1) {
			if (children[i].type == Type.ATOM && key.equals(children[i].value) )
				return i;
		}
		return -1;
	}
	
	int matchPredicate(String key) {
		for (int i = 0; i < children.length; i += 1) {
			if (children[i].type == Type.PREDICATE && key.equals(children[i].value))
				return i;
		}
		return -1;
	}
	
	int matchVariable(String name) {
		for (int i = 0; i < children.length; i += 1) {
			if (children[i].type == Type.VARIABLE && name.equals(children[i].value))
				return i;
		}
		return -1;
	}
	
	int matchOther(Object other) {
		for (int i = 0; i < children.length; i += 1) {
			if (children[i].type == Type.OTHER && other.equals(children[i].value))
				return i;
		}
		return -1;
	}
	
	GoalTrie.TrieResult findOrAddTerm(VarMap varMap, Object[] terms, int termPos) {
			
		int childPos;
		if (terms[termPos] instanceof Atom) {
			
			Atom atom = (Atom) terms[termPos];
			if ((childPos = matchAtom(atom.getKey())) < 0) {			
				return copyAndAddChild(atom.getKey(), Type.ATOM, varMap, terms, termPos);
			} else {
				return findOrAddForChild(childPos, varMap, terms, termPos);
			}
			
		} else if (terms[termPos] instanceof Structure) {
			
			Structure structure = (Structure) terms[termPos];
			Object[] newTerms = prependStrucutreTerms(structure, terms, termPos);
			if ((childPos = matchPredicate(structure.getKey())) < 0) {
				return copyAndAddChild(structure.getKey(), Type.PREDICATE, varMap, newTerms, 0);
			} else {
				return findOrAddForChild(childPos, varMap, newTerms, 0);
			}
			
		} else if (terms[termPos] instanceof Variable) {
			
			Variable var = (Variable) terms[termPos];
			String name = getVarName(var, varMap);
			childPos = matchVariable(name);			
			if (childPos < 0) {
			    return copyAndAddChild(name, Type.VARIABLE, varMap, terms, termPos);
			} else {
				return findOrAddForChild(childPos, varMap, terms, termPos);
			}
			
		} else {
			
			childPos = matchOther(terms[termPos]);
			if (childPos < 0) {
			    return copyAndAddChild(terms[termPos], Type.OTHER, varMap, terms, termPos);
			} else {
				return findOrAddForChild(childPos, varMap, terms, termPos);
			}
		}		
	}
	
	private Object[] prependStrucutreTerms(Structure structure, Object [] terms, int termPos) {
		Object[] args = structure.getArgs();
		int remaining = (terms.length - termPos) - 1;
		Object[] newTerms;
		if (remaining == 0) {
			newTerms = args;
		} else {
			newTerms = Arrays.copyOf(args, args.length + remaining);
			for (int i = args.length, j = termPos+1; j < terms.length; i += 1, j += 1) {
				newTerms[i] = terms[j];
			}
		}
		return newTerms;
	}
	
	private String getVarName(Variable var, VarMap varMap ) {
		String name = varMap.map.get(var);
		if (name == null) {
			varMap.count += 1;
			name = String.format("var$%02d", varMap.count).intern();
			varMap.map.put(var, name);
		}
		return name;
	}
	
	private GoalTrie.TrieResult findOrAddForChild(int childPos, VarMap varMap, Object[] terms, int termPos) {
		if (++termPos >= terms.length)
			return new GoalTrie.TrieResult(this, this);
		GoalTrie.TrieResult trieResult = children[childPos].findOrAddTerm(varMap, terms, termPos);
		if (trieResult.trie == children[childPos])
			return new GoalTrie.TrieResult(this, trieResult.leafNode);
		TrieNode newNode = new TrieNode(this.value, this.type, this.parent);
		newNode.children = Arrays.copyOf(this.children, this.children.length);
		newNode.children[childPos] = trieResult.trie;
		return new GoalTrie.TrieResult(newNode, trieResult.leafNode);
	}
		
	private GoalTrie.TrieResult copyAndAddChild(Object value, Type type,
			VarMap varMap, Object[] terms, int termPos) {
	    TrieNode newNode = new TrieNode(this.value, this.type, this.parent);
		newNode.children = Arrays.copyOf(this.children, this.children.length+1);
		TrieNode leafNode = newNode.addChild(this.children.length, value, type, varMap, terms, termPos);
		return new GoalTrie.TrieResult(newNode, leafNode);
	}
		
	private TrieNode addChild(int childPos, Object value, Type type, VarMap varMap, Object[] terms, int termPos) {
		TrieNode newNode = new TrieNode(value, type, this);
		this.children[childPos] = newNode;
		if (++termPos >= terms.length) {
			newNode.children = new TrieNode[0];
			return newNode;			
		}		
		newNode.children = new TrieNode[1];
		return newNode.addRestOfTerm(varMap, terms, termPos);
	}

	private TrieNode addRestOfTerm(VarMap varMap, Object[] terms, int termPos) {
		
		if (terms[termPos] instanceof Atom) {
			
			Atom atom = (Atom) terms[termPos];
			return addChild(0, atom.getKey(), Type.ATOM, varMap, terms, termPos);
			
		} else if (terms[termPos] instanceof Structure) {
			
			Structure structure = (Structure) terms[termPos];
			Object[] newTerms = prependStrucutreTerms(structure, terms, termPos);
			return addChild(0, structure.getKey(), Type.PREDICATE, varMap, newTerms, 0);

		} else if (terms[termPos] instanceof Variable) {
			
			Variable var = (Variable) terms[termPos];
			String name = getVarName(var, varMap);
			return addChild(0, name, Type.VARIABLE, varMap, terms, termPos);
			
		} else {
			
			return addChild(0, terms[termPos], Type.OTHER, varMap, terms, termPos);
		}
	}
	
}
