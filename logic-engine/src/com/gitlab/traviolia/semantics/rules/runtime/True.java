package com.gitlab.traviolia.semantics.rules.runtime;


/**
  Always succeeds
*/
class True extends ConstBuiltinRuntime {
  True(){
    super("true");
  }
  
  public int exec(RuleEngine p) {
    return 1;
  }
}
