package com.gitlab.traviolia.semantics.rules.runtime;

import java.util.ArrayList;

public class Stack<E> extends ArrayList<E> {
    private static final long serialVersionUID = 1L;
    
    public Stack() {
        super();
    }
    
    public Stack(int size) {
        super(size);
    }

    public void push(E x) {
        add(x);
    }

    public E pop() {
        return remove(size() - 1);
    }
}
