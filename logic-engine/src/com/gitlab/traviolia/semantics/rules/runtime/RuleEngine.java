package com.gitlab.traviolia.semantics.rules.runtime;

import com.gitlab.traviolia.semantics.rules.terms.AtomAstNode;
import com.gitlab.traviolia.semantics.rules.terms.StructureAstNode;


public interface RuleEngine {

    public abstract Trail getTrail();

    public abstract RuleEngine getParent();

    public abstract void stop();

    public abstract void run();

    public StructureRuntime newBuiltin(StructureAstNode f);
	  
    public AtomRuntime newBuiltin(AtomAstNode S);
    
}