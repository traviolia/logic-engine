package com.gitlab.traviolia.semantics.rules.runtime;

import java.util.HashMap;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;


/**
  Used in implementing uniform replacement of
  variables with new constants. useful for printing
  out with nicer variable names.

  @see VariableRuntime
  @see ClauseRuntime
*/
class VarNumbererRuntime extends AbstractSystemObject {
  HashMap dict;
  
  int ctr;
  
  VarNumbererRuntime(){
      super(VarNumbererRuntime.class);
    dict=new HashMap();
    ctr=0;
  }
  
  AbstractTerm action(AbstractTerm place) {
    place= ((AbstractTerm) place.ref());
    // IO.trace(">>action: "+place);
    if(place instanceof VariableRuntime) {
      AtomRuntime root=(AtomRuntime)dict.get(place);
      if(null==root) {
        root=new PseudoVar(ctr++);
        dict.put(place,root);
      }
      place=root;
    }
    // IO.trace("<<action: "+place);
    return place;
  }

@Override
public void inspect(AstNodeVisitor visitor) {
	throw new UnsupportedOperationException();
}

@Override
public int exec(RuleEngine prog) {
	// TODO Auto-generated method stub
	return -1;
}

@Override
public void inspect(TermVisitor visitor) {
	throw new UnsupportedOperationException();	
}

}
