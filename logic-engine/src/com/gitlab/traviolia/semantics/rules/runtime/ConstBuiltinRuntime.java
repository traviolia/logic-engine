package com.gitlab.traviolia.semantics.rules.runtime;



/**
* Template for builtins of arity 0
*/

abstract public class ConstBuiltinRuntime extends AtomRuntime implements ConstBuiltin {
  
  public ConstBuiltinRuntime(String s){
    super(s);
  }
  
  abstract public int exec(RuleEngine p);
  
  public boolean isBuiltin() {
    return true;
  }
}
