package com.gitlab.traviolia.semantics.rules.runtime;

public enum TermType {
    ATOM,
    STRUCT,
    VAR;
}
