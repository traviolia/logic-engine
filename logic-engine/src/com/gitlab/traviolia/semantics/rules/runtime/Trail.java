package com.gitlab.traviolia.semantics.rules.runtime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.terms.TermAstNode;
import com.gitlab.traviolia.semantics.rules.terms.VarAstNode;


/**
   Implements a stack of undo actions
   for backtracking, and in particular,
   resetting a Var's val fiels to unbound (i.e. this).

  
   @see TermAstNode
   @see VarAstNode
*/
public class Trail extends Stack<Object> {

    private static final long serialVersionUID = 1L;
    Logger logger = LogManager.getLogger();

public Trail(){
    super();
  };
  
  public String name() {
    return "trail"+hashCode()%64;
  }
  
  public String pprint() {
    return name()+"\n"+super.toString()+"\n";
  }
  
  /**
    Used to undo bindings after unification,
    if we intend to leave no side effects.
  */

  @Override
  public final void push (Object o) {
	  if (o instanceof VariableRuntime)
		  logger.trace("added {} to trail", ((VariableRuntime) o).getName());
	  super.push(o);
  }
  
  public final Object pop() {
	  Object o = super.pop();
	  if (o instanceof VariableRuntime)
		  logger.trace("removed {} from trail", ((VariableRuntime) o).getName());
	  return o;
  }
  
  // synchronized
  final public void unwind(int to) {
    // IO.mes("unwind TRAIL: "+name()+": "+size()+"=>"+to);
    // if(to>size())
    // IO.assertion("unwind attempted from smaller to larger top");
    for(int i=size()-to;i>0;i--) {
        Object object = pop();
    	if (object instanceof TermAstNode) {
	        throw new IllegalStateException();
    	} else if (object instanceof Term) {
	        ((Term) object).undo();
    	} else {
    		throw new RuntimeException();
    	}
    }
  }
  
  public String stat() {
    return "Trail="+size();
  }
}
