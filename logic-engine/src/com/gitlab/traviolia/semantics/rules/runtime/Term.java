package com.gitlab.traviolia.semantics.rules.runtime;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;

public interface Term {

    public final static int JAVA = -4;
    public final static int REAL = -3;
    public final static int INT = -2;
    public final static int VAR = -1;
    public final static int CONST = 0;

    public TermType getType();

    /** 
        returns or fakes an arity for all subtypes 
     */
    public int getArity();

    /** Dereferences if necessary.
          If multi-threaded, this should be synchronized
          otherwise vicious non-reentrancy problems
          may occur in the presence of GC and heavy
          multi-threading!!!
     */
    public Object ref();

    public void undo();
    
    public boolean bind_to(Term that,Trail trail);
    public boolean unify_to(Term that,Trail trail);
    public boolean unify(Term that,Trail trail);
    
    public int exec(RuleEngine prog);

//    public abstract Term token();

//    public Clause toClause();

    /**
        Tests if this term unifies with that.
        Bindings are trailed and undone after the test.
        This should be used with the shared term as this
        and the new term as that. Synchronization makes
        sure that side effects on the shared term are
        not interfering, i.e as in:
           SHARED.matches(NONSHARED,trail).
    
     */
    // synchronized
    public boolean matches(Term that);

    public boolean matches(Term that, Trail trail);

    /**
        Returns a copy of the result if the unification
        of this and that. Side effects on this and that
        are undone using trailing of bindings..
        Synchronization happens over this, not over that.
        Make sure it is used as SHARED.matching_copy(NONSHARED,trail).
     */
    // synchronized

    public Term matching_copy(Term that);

    /**
        Defines the reaction to an agent recursing
        over the structure of a term. <b>This</b> is passed
        to the agent and the result of the action is returned.
        Through overriding, for instance, a Fun term will provide
        the recursion over its arguments, by applying the action
        to each of them.
        
        @see StructureRuntime
     */
    public void inspect(AstNodeVisitor visitor);
    public void inspect(TermVisitor visitor);

    public Term action(Term that);

    /**
         Returns a copy of a term with variables
         standardized apart (`fresh variables').
     */
    // synchronized
    public Term copy();

//    /**
//        Returns '[]'(V1,V2,..Vn) where Vi is a variable
//        occuring in this Term
//     */
//    public Term varsOf();

//    /**
//        Replaces variables with uppercase constants named 
//        `V1', 'V2', etc. to be read back as variables.
//     */
//    public Term numbervars();
//
    /**
         Prints out a term to a String with
         variables named in order V1, V2,....
     */
    public String pprint();

    /*
        Returns an unquoted version of toString()
     */
    public String toUnquoted();

    /**
         Returns a string key used based on the string name
         of the term. Note that the key for a clause AL-B,C. 
         is the key insted of ':-'.
     */
    public String getKey();

    /** 
        Java Object wrapper. In particular, it is
        used to wrap a Thread to hide it inside a Prolog
        data object.
     */
    public Object toObject();

    public boolean isBuiltin();

	public Term reaction(Term term);
	
	public boolean isVar();

}