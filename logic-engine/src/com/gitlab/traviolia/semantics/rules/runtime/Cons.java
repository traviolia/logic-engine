package com.gitlab.traviolia.semantics.rules.runtime;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;


/**
  List Constructor. Cooperates with terminator Nil.

  @see Nil
*/
public class Cons extends StructureRuntime {
	  public Cons(String cons,Term x0,Term x1){
		    super(cons,x0,x1);
		  }
	  public Cons(String cons,Object x0,Term x1){
		    super(cons,x0,x1);
		  }
  
  public Cons(Term x0,Term x1){
	    this(".",x0,x1);
	  }
	  
  public Cons(Object x0,Term x1){
	    this(".",x0,x1);
	  }
	  
  @Override
  public Cons clone() {
		return (Cons) super.clone();
	}

	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void inspect(TermVisitor visitor) {
		visitor.visit(this);
	}

  public Term getHead() {
    return ((Term) getArg(0));
  }
  
  public Term getTail() {
    return ((Term) getArg(1));
  }
  
  /**
    List printer.
  */
  public String toString() {
    Object h= getArg(0);
    Object t= getArg(1);
    StringBuffer s=new StringBuffer("["+watchNull(h));
    for(;;) {
      if(t instanceof Nil) {
        s.append("]");
        break;
      } else if(t instanceof Cons) {
        h= ((Cons)t).getArg(0);
        t= ((Cons)t).getArg(1);
        s.append(","+watchNull(h));
      } else {
        s.append("|"+watchNull(t)+"]");
        break;
      }
    }
    return s.toString();
  }
}
