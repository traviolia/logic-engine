package com.gitlab.traviolia.semantics.rules.runtime;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;

/**
 * A SystemObject is a  Nonvar with system assigned name
 * 
 */
public abstract class AbstractSystemObject extends AbstractNonVar {
  
  static long ctr=0;
  
  private long ordinal;
  
  public AbstractSystemObject(Class<?> selfType){
    ordinal=++ctr;
  }
  
  @Override
  public AbstractSystemObject clone() {
		return (AbstractSystemObject) super.clone();
	}

  public String name() {
    return "{"+getClass().getName()+"."+ordinal+"}";
  }
  
	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void inspect(TermVisitor visitor) {
		visitor.visit(this);
	}

  @Override
  public boolean bind_to(Term that,Trail trail) {
    return super.bind_to(that,trail)&&ordinal==((AbstractSystemObject)that).ordinal;
  }
  
  public String toString() {
    return name();
  }
  
  public final int getArity() {
    return Term.JAVA;
  }
}
