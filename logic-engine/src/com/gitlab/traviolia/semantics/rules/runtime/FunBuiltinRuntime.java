package com.gitlab.traviolia.semantics.rules.runtime;


/**
* Template for builtins of arity >0
*/

abstract public class FunBuiltinRuntime extends StructureRuntime implements FunBuiltin {
  public FunBuiltinRuntime(String f,int i){
    super(f, new Object[i]);
  }
    
  @Override
  public boolean isBuiltin() {
    return true;
  }
}
