/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.runtime;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;

/**
  Top element of the Prolog term hierarchy.
  Describes a simple or compound term like:
  X,a,13,f(X,s(X)),[a,s(X),b,c], a:-b,c(X,X),d, etc.
*/
public abstract class AbstractTerm implements Cloneable, Term {

  @Override
   public AbstractTerm clone() {
		try {
			return (AbstractTerm) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

   /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#getType()
 */
@Override
public TermType getType() {
       return TermType.ATOM;
   }
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#getArity()
 */
  @Override
abstract public int getArity();
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#ref()
 */
  @Override
public Object ref() { // synchronized !!!
    return this;
  }
  
  public abstract boolean bind_to(Term that,Trail trail);
  
  /** Unify dereferenced */
  public abstract boolean unify_to(Term that,Trail trail);
  
  /** Dereference and unify_to */
  public final boolean unify(Term that,Trail trail) {
    return ((Term) ref()).unify_to(((Term) that.ref()),trail);
  }
  
  public void undo() { // does nothing
  }
  
  // public abstract boolean eq(Term that);
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#token()
 */
public Term token() {
    return this;
  }
  
  Term toTerm() {
    return this;
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#toClause()
 */
public Clause toClause() {
    return new ClauseRuntime(this,AtomRuntime.aTrue);
  }
  
  boolean isClause() {
    return false;
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#matches(com.github.lycastus.semantics.rules.runtime.Term)
 */
  // synchronized
  @Override
public boolean matches(Term that) {
    return matches(that,new Trail());
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#matches(com.github.lycastus.semantics.rules.runtime.Term, com.github.lycastus.semantics.rules.terms.Trail)
 */
@Override
public boolean matches(Term that,Trail trail) {
    int oldtop=trail.size();
    boolean ok=unify(that,trail);
    trail.unwind(oldtop);
    return ok;
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#matching_copy(com.github.lycastus.semantics.rules.runtime.TermInterface)
 */
  // synchronized
  
  @Override
public Term matching_copy(Term that) {
    Trail trail=new Trail();
    boolean ok=unify(that,trail);
    // if(ok) that=that.copy();
    if(ok)
      that= (Term) copy();
    trail.unwind(0);
    return (ok)?that:null;
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#inspect(com.github.lycastus.semantics.rules.runtime.AstNodeVisitor)
 */
	@Override
    public abstract void inspect(AstNodeVisitor visitor);

	@Override
	public abstract void inspect(TermVisitor visitor);

  public Term reaction(Term agent) {
	    Term T= (Term) agent.action(this);
	    return T;
	  }
	  
  /**
     Identity action.
  */
  public Term action(Term that) {
    return that;
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#copy()
 */
  // synchronized
  @Override
public Term copy() {
    return reaction(new CopierRuntime());
//	  Copier copier = new Copier();
//	  inspect(copier);
//	  return copier.getReturnValue();
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#varsOf()
 */
public Term varsOf() {
    return (new CopierRuntime()).getMyVars(this);
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#numbervars()
 */
public Term numbervars() {
    return copy().reaction(new VarNumbererRuntime());
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#pprint()
 */
  @Override
public String pprint() {
    return numbervars().toString();
  }
  
  /*
    Returns an unquoted version of toString()
  */
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#toUnquoted()
 */
@Override
public String toUnquoted() {
    return pprint();
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#getKey()
 */
  @Override
public String getKey() {
    return toString();
  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#toObject()
 */
  @Override
public Object toObject() {
    return ref();
  }
  
  /*
    Just to catch the frequent error when the arg is forgotten while
    definig a builtin. Being final, it will generate a compile time 
    error if this happens
  */
  final int exec() {
    
    return -1;
  }
  
  /**
    Executed when a builtin is called.
    Needs to be overriden. Returns a run-time
    warning if this is forgotten.
  */
  
//  @Override
//  public int exec(RuleEngine p) {
//    // IO.println("this should be overriden, prog="+p);
//    return -1;
//  }
  
  /* (non-Javadoc)
 * @see com.github.lycastus.semantics.rules.runtime.Term#isBuiltin()
 */
@Override
public boolean isBuiltin() {
    return false;
  }

@Override
public boolean isVar() {
	return false;
}
public AbstractNonVar toChars() {
    return stringToChars(toUnquoted());
  }

static final AbstractNonVar stringToChars(String s) {
    if(0==s.length())
      return AtomRuntime.aNil;
    Cons l=new Cons(s.charAt(0),AtomRuntime.aNil);
    Cons curr=l;
    for(int i=1;i<s.length();i++) {
      Cons tail=new Cons(s.charAt(i),AtomRuntime.aNil);
      curr.args[1]=tail;
      curr=tail;
    }
    return l;
  }
  
}
