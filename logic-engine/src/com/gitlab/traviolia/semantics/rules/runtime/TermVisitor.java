package com.gitlab.traviolia.semantics.rules.runtime;


public interface TermVisitor {
	
	public void visit(AtomRuntime node);
	
	public void visit(ClauseRuntime node);

	public void visit(Conjunction node);
	
	public void visit(Cons node);

	public void visit(StructureRuntime node);

	public void visit(AbstractSystemObject node);

	public void visit(VariableRuntime node);
	
}
