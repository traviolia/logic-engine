package com.gitlab.traviolia.semantics.rules.runtime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;

/**
  Implements compound terms
  @see AbstractTerm
*/
public class StructureRuntime extends AbstractNonVar implements Structure {
	
	Logger logger = LogManager.getLogger();
	
  public String sym;
  public Object args[];
  
  public final int getArity() {
    return args.length;
  }
  
  public StructureRuntime(String s, Object ...args){
	    this.sym = s;
	    this.args = args;
	  }
	  
	public StructureRuntime(String s) {
		this(s, new Object[0]);
	}

	public StructureRuntime(String s, int arity) {
		this(s, new Object[arity]);
	}

	public StructureRuntime(String s, Term x0) {
		this(s, 1);
		args[0] = x0;
	}

	public StructureRuntime(String s, Term x0, Term x1) {
		this(s, 2);
		args[0] = x0;
		args[1] = x1;
	}

	public StructureRuntime(String s, Term x0, Term x1, Term x2) {
		this(s, 3);
		args[0] = x0;
		args[1] = x1;
		args[2] = x2;
	}

	public StructureRuntime(String s, Term x0, Term x1,
			Term x2, Term x3) {
		this(s, 4);
		args[0] = x0;
		args[1] = x1;
		args[2] = x2;
		args[3] = x3;
	}

	   @Override
	   public StructureRuntime clone() {
			return (StructureRuntime) super.clone();
		}

	   @Override
	public final String name() {
		return sym;
	}

	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void inspect(TermVisitor visitor) {
		visitor.visit(this);
	}

	  @Override
	public String qname() {
		if (0 == sym.length())
			return "''";
		for (int i = 0; i < sym.length(); i++) {
			if (!Character.isLowerCase(sym.charAt(i)))
				return '\'' + sym + '\'';
		}
		return sym;
	}

	public String getKey() {
		return sym;
	}


  /*
  public Fun(int arity) {
     //setDefaultName();
     args=new Term[arity];
  }
  */
  
	  @Override
  public void init(int arity) {
    args=new Term[arity];
  }
  
  @Override
  public final Object getArg(int i) {
	  Object object = args[i];
	  if (object == null) {
		  return null;//FIXME should never be null
	  } else if (object instanceof Term) {
		  return ((Term) object).ref();
	  } else {
		  return object;
	  }
  }
  
  @Override
  public final int getIntArg(int i) {
      return (Integer) getArg(i);
  }
  
  @Override
  public final void setArg(int i, Object T) {
    args[i]=T;
  }
  
  @Override
  public final int putArg(int i, Object T, RuleEngine p) {
    // return getArg(i).unify(T,p.getTrail())?1:0;
    return ((AbstractTerm) args[i]).unify((Term) T,p.getTrail())?1:0;
  }
  
  protected final String funToString() {
    if(args==null)
      return qname()+"()";
    int l=args.length;
    return qname()+(l<=0?"":"("+show_args()+")");
  }
  
  @Override
  public String toString() {
    return funToString();
  }
  
  protected static String watchNull(Object x) {
    return((null==x)?"null":x.toString());
  }
  
  private String show_args() {
    StringBuffer s=new StringBuffer(watchNull(args[0]));
    for(int i=1;i<args.length;i++) {
      s.append(","+watchNull(args[i]));
    }
    return s.toString();
  }
  
  @Override
  public boolean bind_to(Term that,Trail trail) {
    return super.bind_to(that,trail)&&args.length==((StructureRuntime)that).args.length;
  }
  
  @Override
  public boolean unify_to(Term that,Trail trail) {
    if(bind_to(that,trail)) {
      StructureRuntime other=(StructureRuntime)that;
      for(int i=0;i<args.length;i++) {
        if(!((AbstractTerm)args[i]).unify((Term) other.args[i], trail))
          return false;
      }
      return true;
    } else
      return that.bind_to(this,trail);
  }
  
  @Override
  public Term token() {
    return (Term) args[0];
  }
  
  // stuff allowing polymorphic cloning of Fun subclasses
  // without using reflection - should be probaly faster than
  // reflection classes - to check
  
  final public StructureRuntime funClone() {
    StructureRuntime f=null;
       f=(StructureRuntime)clone();   
    return f;
  }
  
  protected Structure unInitializedClone() {
    Structure f=funClone();
    f.setArgs(new Term[args.length]);
    return f;
  }
  
//  protected Structure initializedClone() {
//    Structure f=funClone();
//    f.init(args.length);
//    return f;
//  }
  
  @Override
  public Term reaction(Term that) {
    //IO.mes("TRACE>> "+name());
    Structure f=funClone();
    f.setArgs(new Object[args.length]);
    for(int i=0;i<args.length;i++) {
    	if (args[i] instanceof Term) {
    		f.setArg(i, ((Term) args[i]).reaction(that));
    	} else {
    		f.setArg(i, args[i]);
    	}
    }
    return f;
  }
  
  @Override
public AbstractNonVar listify() {
   Cons l=new Cons(new AtomRuntime(name()), AtomRuntime.aNil);
    Cons curr=l;
    for(int i=0;i<args.length;i++) {
      Cons tail=new Cons((Term) args[i], AtomRuntime.aNil);
      curr.args[1]=tail;
      curr=tail;
    }
    return l;
  }
  
  @Override
  boolean isClause() {
    return getArity()==2&&name().equals(":-");
  }

	@Override
	public Object[] getArgs() {
		return args;
	}
	
	@Override
	public void setArgs(Object[] args) {
		this.args = args;
	}

	@Override
	public int exec(RuleEngine prog) {
		return -1;
	}
	
}
