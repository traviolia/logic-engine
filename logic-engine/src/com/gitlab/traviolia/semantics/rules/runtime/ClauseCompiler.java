package com.gitlab.traviolia.semantics.rules.runtime;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;
import com.gitlab.traviolia.semantics.rules.terms.AtomAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ClauseAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ConjAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ConsAstNode;
import com.gitlab.traviolia.semantics.rules.terms.ConstBuiltinWrapper;
import com.gitlab.traviolia.semantics.rules.terms.FailAstNode;
import com.gitlab.traviolia.semantics.rules.terms.FunBuiltinWrapper;
import com.gitlab.traviolia.semantics.rules.terms.IntAstNode;
import com.gitlab.traviolia.semantics.rules.terms.NilAstNode;
import com.gitlab.traviolia.semantics.rules.terms.RealAstNode;
import com.gitlab.traviolia.semantics.rules.terms.StructureAstNode;
import com.gitlab.traviolia.semantics.rules.terms.TermAstNode;
import com.gitlab.traviolia.semantics.rules.terms.TrueAstNode;
import com.gitlab.traviolia.semantics.rules.terms.VarAstNode;

/**
 * Term Copier agent. Has its own Variable dictionnary. Uses a generic action
 * propagator which recurses over Terms.
 */
public class ClauseCompiler implements AstNodeVisitor {
    
    private Logger logger = LogManager.getLogger();
    
    private HashMap<VarAstNode,VariableRuntime> oldVarNewVarMap;
    
    public ClauseCompiler() {
    	oldVarNewVarMap = new HashMap<VarAstNode,VariableRuntime>();
    }

    /**
     * Holds the value returned from the visit method.
     */
    private Object returnValue;

    public Object getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(Object retVal) {
        this.returnValue = retVal;
    }

    @Override
    public void visit(final AtomAstNode node) {
        if (node instanceof NilAstNode) {
            setReturnValue(AtomRuntime.aNil);
        } else if (node instanceof TrueAstNode) {
            setReturnValue(AtomRuntime.aTrue);
        } else if (node instanceof FailAstNode) {
            setReturnValue(AtomRuntime.aFail);
        } else if (node.getName().equals("yes")) {
            setReturnValue(AtomRuntime.aYes);
        } else if (node.getName().equals("no")) {
            setReturnValue(AtomRuntime.aNo);
        } else if (node.getName().equals("end_of_file")) {
            setReturnValue(AtomRuntime.anEof);
        } else if (node instanceof ConstBuiltinWrapper) {
        	setReturnValue(((ConstBuiltinWrapper)node).builtin);
//        } else if (node instanceof ConstBuiltinAstNode) {
//            AtomRuntime atom = new com.github.lycastus.semantics.rules.runtime.ConstBuiltinRuntime(node.name()) {
//                @Override
//                public int exec(ProgInterface p) {
//                    return ((ConstBuiltinAstNode) node).exec( p);
//                }
//            };
//            setReturnValue(atom);
        } else {
            setReturnValue(new AtomRuntime(node.getName()));
        }
    }

    @Override
    public void visit(ClauseAstNode node) {
        visit((StructureAstNode) node);
    }

    @Override
    public void visit(ConjAstNode node) {
        visit((StructureAstNode) node);
    }

    @Override
    public void visit(ConsAstNode node) {
        visit((StructureAstNode) node);
    }

    @Override
    public void visit(IntAstNode node) {
        setReturnValue(new Integer(node.intValue()));
    }

    @Override
    public void visit(RealAstNode node) {
        setReturnValue(new Double(node.getValue()));
    }

    @Override
    public void visit(final StructureAstNode node) {
        StructureRuntime structure;
        if (node instanceof FunBuiltinWrapper) {
        	structure = ((FunBuiltinWrapper) node).runtime;
        } else if (node instanceof ClauseAstNode) {
            structure = new ClauseRuntime(null, null);
            ((ClauseRuntime) structure).sym = ((ClauseAstNode) node).getSymbol();
            ((ClauseRuntime) structure).dict = null; //FIXME ((ClauseAstNode) node).dict;
            ((ClauseRuntime) structure).ground = ((ClauseAstNode) node).ground;
        } else if (node instanceof ConsAstNode) {
       	    if (node instanceof ConjAstNode) {
        		structure = new Conjunction(null, null);
        	} else {
        		structure = new Cons(null, null);
        	}
        } else {
            structure = new StructureRuntime(node.getSymbol(), node.getArity());
            structure.args = new Object[node.getArgs().length];
        }
        for (int i = 0; i < node.getArgs().length; i += 1) {
            ((TermAstNode) node.getArg(i)).inspect(this);
            structure.args[i] = getReturnValue();
            if (structure.args[i] == null) throw new RuntimeException();
        }
        if (node.getArgs().length < 1) throw new RuntimeException();
        setReturnValue(structure);
    }

    @Override
    public void visit(VarAstNode node) {
        VariableRuntime var = oldVarNewVarMap.get(node);
        if (null == var) {
            var = new VariableRuntime(node);
            oldVarNewVarMap.put(node, var);
        }
        setReturnValue(var);
    }

	@Override
	public void visit(Term node) {
		throw new UnsupportedOperationException();		
	}
}
