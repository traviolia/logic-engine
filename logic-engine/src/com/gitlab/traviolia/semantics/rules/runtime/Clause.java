package com.gitlab.traviolia.semantics.rules.runtime;



public interface Clause extends Structure {

	/**
	    Reads a goal as a clause containing a dummy header with
	    all variables in it
	 */

	public abstract void setFile(String fname, int begins_at, int ends_at);

	/**
	 * Pretty prints a clause after replacing ugly variable names
	 */
	public abstract String pprint();

	/**
	 * Pretty prints a clause after replacing ugly variable names
	 */
	public abstract String pprint(boolean replaceAnonymous);

	/**
	    Clause to Term converter: the joy of strong typing:-)
	 */
	public abstract Clause toClause();

//	/**
//	 * Replaces varibles with nice looking upper case
//	 * constants for printing purposes
//	 */
//	// synchronized
//	public abstract Clause cnumbervars(boolean replaceAnonymous);

	/**
	    Converts a clause to a term.
	    Note that Head:-true will convert to the term Head.
	 */
	public abstract Term toTerm();

	/**
	    Extracts the head of a clause (a Term).
	 */
	public Term getHead();

	/**
	    Extracts the body of a clause
	 */
	public abstract Term getBody();
	
	public Term getFirst();
	public Term getRest();
	public Clause ccopy();
//	public Clause unfold(Clause that,Trail trail);
//
//	public Clause unfold_with_goal(Clause goal,Trail trail);

	public abstract Term[] toArray();

}