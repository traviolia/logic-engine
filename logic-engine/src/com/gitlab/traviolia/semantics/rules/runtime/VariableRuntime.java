/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.runtime;

import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;
import com.gitlab.traviolia.semantics.rules.terms.VarAstNode;

/**
 * Part of the Term hierarchy implmenting logical variables. They are subject to
 * reset by application of and undo action keep on the trail stack.
 */
public class VariableRuntime extends AbstractTerm implements Variable {

	protected String name;
	protected Object val;

	public VariableRuntime() {
		this.val = this;
		this.name = "_";
	}

	public VariableRuntime(String name, Object val) {
		this.val = val;
		this.name = name;
	}

	public VariableRuntime(VarAstNode node) {
		this.val = this;
		this.name = node.getName();
	}

	public VariableRuntime(VariableRuntime variable) {
		this.val = this;
		this.name = variable.name;
	}

	@Override
	public VariableRuntime clone() {
		return (VariableRuntime) super.clone();
	}

	@Override
	public int getArity() {
		return Term.VAR;
	}

	final public boolean isUnbound() {
		return val == this;
	}

	@Override
	public Term ref() {
		return isUnbound() ? this : ((Term) ((Term) val).ref());
	}

	@Override
	public void inspect(AstNodeVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void inspect(TermVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public boolean bind_to(Term that, Trail trail) {
		val = that;
		trail.push(this);
		return true;
	}

	public boolean bind_to(Object x, Trail trail) {
		val = x;
		trail.push(this);
		return true;
	}

	public void undo() {
		val = this;
	}

	@Override
	public boolean unify_to(Term that, Trail trail) {
		// expects: this, that are dereferenced
		// return (this==that)?true:val.bind_to(that,trail);
		return ((Term) val).bind_to(that, trail);
	}

	public boolean eq(Term x) { // not a term compare!
		return ref() == x.ref();
	}

	@Override
	public String getKey() {
		Term t = ref();
		if (t instanceof VariableRuntime)
			return null;
		else
			return t.getKey();
	}

	@Override
	public Term reaction(Term agent) {

		Term R = agent.action(ref());

		if (!(R instanceof VariableRuntime)) {
			R = R.reaction(agent);
		}

		return R;
	}

	protected String getName() {
		return name+"$"+Integer.toHexString(hashCode());
	}

	@Override
	public String toString() {
		return isUnbound() ? getName() : ref().toString();
	}

	@Override
	public boolean isVar() {
		return true;
	}

	@Override
	public int exec(RuleEngine prog) {
		return -1;
	}
}