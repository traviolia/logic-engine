package com.gitlab.traviolia.semantics.rules.runtime;



/**
  Always fails
*/
class Fail extends ConstBuiltinRuntime {
  Fail() {super("fail");}

  public int exec(RuleEngine p) {
    return 0;
  }
}