package com.gitlab.traviolia.semantics.rules.runtime;


public interface Atom extends Term {

	public abstract String name();

	public abstract String qname();

}