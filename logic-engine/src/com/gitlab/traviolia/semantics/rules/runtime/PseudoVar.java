package com.gitlab.traviolia.semantics.rules.runtime;


/**
  Special constants, used to Name variables
  @see AbstractTerm
  @see VariableRuntime
*/
class PseudoVar extends AtomRuntime {
  PseudoVar(int i){
    super("V_"+i);
  }
  
  PseudoVar(String s){
    super(s);
  }
  
  public String toString() {
    return name();
  }
}
