package com.gitlab.traviolia.semantics.rules.runtime;


public class Conjunction extends Cons {
  public Conjunction(Term x0,Term x1){
    super(",",x0,x1);
  }
  
  @Override
  public Conjunction clone() {
		return (Conjunction) super.clone();
	}

	@Override
	public void inspect(TermVisitor visitor) {
		visitor.visit(this);
	}

  public String conjToString() {
    AbstractTerm h= ((AbstractTerm) ((Term) args[0]).ref());
    Term t= ((Term) ((Term) args[1]).ref());
    StringBuffer s=new StringBuffer(watchNull(h));
    for(;;) {
      if(!(t instanceof Conjunction)) {
        s.append(","+t);
        break;
      } else {
        h=(AbstractTerm) ((Term) ((Conjunction)t).args[0]).ref();
        t=(Term) ((Term) ((Conjunction)t).args[1]).ref();
        s.append(","+watchNull(h));
      }
    }
    return s.toString();
  }
  
  public String toString() {
    return funToString();
  }
  
  static public final Term getHead(Term T) {
    T= (Term) T.ref();
    return (T instanceof Conjunction) ? (Term) ((Conjunction)T).getArg(0) : T;
  }
  
  static public final Term getTail(Term T) {
    T= (Term) T.ref();
    return (T instanceof Conjunction) ? (AbstractTerm) ((Conjunction)T).getArg(1) : AtomRuntime.aTrue;
  }
}
