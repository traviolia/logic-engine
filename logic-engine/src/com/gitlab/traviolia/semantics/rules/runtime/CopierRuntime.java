/*
 * Copyright 2015 Lycastus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Derived from Paul Tarau's Kernel Prolog; which is available
 * in Open Source form under Apache License, Version 2.0
 * at https://code.google.com/p/kernel-prolog/
 */

package com.gitlab.traviolia.semantics.rules.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gitlab.traviolia.semantics.rules.io.IO;
import com.gitlab.traviolia.semantics.rules.terms.AstNodeVisitor;

/**
 * Term Copier agent. Has its own Variable dictionnary. Uses a generic action
 * propagator which recurses over Terms.
 */
public class CopierRuntime extends AbstractSystemObject {
	
	private Logger logger = LogManager.getLogger();
	
	private HashMap<VariableRuntime,VariableRuntime> oldVarNewVarMap;

	/**
	 * Holds the value returned from the visit method.
	 */
	private AbstractTerm returnValue;

	/**
	 * creates a new Copier together with its related HashDict for variables
	 */
	CopierRuntime() {
		super(CopierRuntime.class);
		oldVarNewVarMap = new HashMap<VariableRuntime,VariableRuntime>();
	}

	public AbstractTerm getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(AbstractTerm retVal) {
		this.returnValue = retVal;
	}

	/**
	 * This action only defines what happens here (at this <b> place </b>).
	 * Ageneric mechanism will be used to recurse over Terms in a (truly:-)) OO
	 * style (well, looks more like some Haskell stuff, but who cares).
	 */
	AbstractTerm action(AbstractTerm place) {

		if (place instanceof VariableRuntime) {
			VariableRuntime root = (VariableRuntime) oldVarNewVarMap.get(place);
			if (null == root) {
				root = new VariableRuntime((VariableRuntime) place);
				oldVarNewVarMap.put((VariableRuntime)place, root);
			}
			place = root;
		}

		return place;
	}

	// Term copyMe(Term that) {
	// return that.reaction(this);
	// }

	/**
	 * Reifies an Iterator as a ArrayList. ArrayList.iterator() can give back
	 * the iterator if needed.
	 * 
	 * @see CopierRuntime
	 */
	static ArrayList EnumerationToVector(Iterator e) {
		ArrayList V = new ArrayList();
		while (e.hasNext()) {
			V.add(e.next());
		}
		return V;
	}

	public static ArrayList ConsToVector(AbstractNonVar xs) {
		ArrayList V = new ArrayList();
		AbstractTerm t = xs;
		for (;;) {
			if (t instanceof Nil) {
				break;
			} else if (t instanceof Cons) {
				Cons c = (Cons) t;
				V.add(c.getArg(0));
				t = ((AbstractTerm) c.getArg(1));
			} else if (t instanceof AtomRuntime) {
				V.add(t);
				break;
			} else {
				V = null;
				IO.errmes("bad Cons in ConsToVector: " + t);
				break;
			}
		}
		// IO.mes("V="+V);
		return V;
	}

	/**
	 * Converts a reified Iterator to functor based on name of Const c and args
	 * being the elements of the Iterator.
	 */

	static Term toFun(AtomRuntime c, Iterator e) {
		ArrayList V = EnumerationToVector(e);
		int arity = V.size();
		if (arity == 0)
			return c;
		StructureRuntime f = new StructureRuntime(c.name(), arity);
		for (int i = 0; i < arity; i++) {
			f.args[i] = (Term) V.get(i);
		}
		return f;
	}

	/**
	 * Represents a list [f,a1...,an] as f(a1,...,an)
	 */

	static StructureRuntime VectorToFun(ArrayList V) {
		AtomRuntime f = (AtomRuntime) V.get(0);
		int arity = V.size() - 1;
		StructureRuntime T = new StructureRuntime(f.name(), arity);
		for (int i = 0; i < arity; i++) {
			T.args[i] = (Term) V.get(i + 1);
		}
		return T;
	}

	/**
	 * Extracts the free variables of a Term, using a generic action/reaction
	 * mechanism which takes care of recursing over its structure. It can be
	 * speeded up through specialization.
	 */
	final static AtomRuntime anAnswer = new AtomRuntime("answer");

//	TermAstNode getMyVars(TermAstNode that) {
//		/* Term */that.reaction(this);
//		return toFun(anAnswer, oldVarNewVarMap.keySet().iterator());
//	}

	Term getMyVars(Term that) {
		/*Term*/that.reaction(this);
		return toFun(anAnswer, oldVarNewVarMap.keySet().iterator());
	}

//	@Override
//	public void visit(Atom node) {
//		setReturnValue(node);
//	}
//
//	@Override
//	public void visit(ClauseRuntime node) {
//		visit((StructureRuntime) node);
//	}
//
//	@Override
//	public void visit(Conjunction node) {
//		visit((StructureRuntime) node);
//	}
//
//	@Override
//	public void visit(Cons node) {
//		visit((StructureRuntime) node);
//	}
//
//	@Override
//	public void visit(StructureRuntime node) {
//		StructureRuntime structure = node.clone();
//	    structure.args = new Object[node.args.length];
//	    for (int i = 0; i < node.args.length; i += 1) {
//	    	if (node.args[i] instanceof Term) {
//		    	((Term) node.args[i]).inspect(this);
//		    	structure.args[i] = getReturnValue();
//	    	} else {
//		    	structure.args[i] = node.args[i];
//	    	}
//	    }
//	    setReturnValue(structure);
//	}
//
//	@Override
//	public void visit(AbstractSystemObject node) {
//		setReturnValue(node);
//	}
//
//	@Override
//	public void visit(Variable node) {
//		if (node.isUnbound()) {
//			Variable var = oldVarNewVarMap.get(node);
//			if (null == var) {
//				var = new Variable();
//				oldVarNewVarMap.put(node, var);
//			}
//			setReturnValue(var);
//		} else {
//			node.ref().inspect(this);
//			// returnValue is set while visiting the referenced node
//		}
//	}

	@Override
	public void inspect(AstNodeVisitor visitor) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int exec(RuleEngine prog) {
		// TODO Auto-generated method stub
		return -1;
	}
	
	@Override
	public void inspect(TermVisitor visitor) {
		throw new UnsupportedOperationException();	
	}
}
